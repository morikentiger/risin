/* mymusic.h */



/*スピーカー関係*/

#define MTU_BZ_ON()		(MTU2.TSTR.BIT.CST2 = 1)	//スピーカーのMTUのカウントスタート
#define MTU_BZ_OFF()	(MTU2.TSTR.BIT.CST2 = 0)	//スピーカーのMTUのカウントやめー

#define MTU_BZ_CLOCK	25000000	//ブザー用MTUの発振数(24MHz)
#define MTU_BZ_CYCLE        MTU22.TGRA            //ブザー周期を設定
#define MTU_BZ_PULSWIDTH    MTU22.TGRB            //ブザーパルス幅を設定

//音階の周波数
#define BZ_FREQ_REST    0                      //休符
#define BZ_FREQ_LA0     440                    //ラ（A）の周波数
#define BZ_FREQ_SI0     494                    //シ（B）の周波数
#define BZ_FREQ_DO      523                    //ド（C）の周波数
#define BZ_FREQ_RE      587                    //レ（D）の周波数
#define BZ_FREQ_MI      659                    //ミ（E）の周波数
#define BZ_FREQ_FA      698                    //ファ（F）の周波数
#define BZ_FREQ_SO      784                    //ソ（G）の周波数
#define BZ_FREQ_LA      880                    //ラ（A）の周波数
#define BZ_FREQ_SI      988                    //シ（B）の周波数
#define BZ_FREQ_DO2     1047                   //ド（C）の周波数
#define BZ_FREQ_RE2     1174                   //レ（D）の周波数
#define BZ_FREQ_MI2     1318                   //ミ（E）の周波数
#define BZ_FREQ_FA2     1396                   //ファ（F）の周波数
#define BZ_FREQ_SO2     1568                   //ソ（G）の周波数
#define BZ_FREQ_LA2     1760                   //ラ（A）の周波数
#define BZ_FREQ_SI2     1976                   //シ（B）の周波数
#define BZ_FREQ_DO3     2094                   //ド（C）の周波数

//ブザー ON/OFF
#define BZ_ON           1                      //ブザー ON
#define BZ_OFF          0                      //ブザー OFF
//音階の周波数
#define BZ_FREQ_REST    0                      //休符

#define BZ_FREQ_DO4     262                    //ド（C）の周波数
#define BZ_FREQ_REb4    277                    //レb（Db）の周波数
#define BZ_FREQ_RE4     294                    //レ（D）の周波数
#define BZ_FREQ_MIb4    311                    //ミb（Eb）の周波数
#define BZ_FREQ_MI4     329                    //ミ（E）の周波数
#define BZ_FREQ_FA4     349                    //ファ（F）の周波数
#define BZ_FREQ_SOb4    370                    //ソb（Gb）の周波数
#define BZ_FREQ_SO4     392                    //ソ（G）の周波数
#define BZ_FREQ_LAb4    415                    //ラb（Ab）の周波数
#define BZ_FREQ_LA4     440                    //ラ（A4）の周波数
#define BZ_FREQ_SIb4    466                    //シb（Bb）の周波数
#define BZ_FREQ_SI4     494                    //シ（B）の周波数

#define BZ_FREQ_DO5     523                    //ド（C）の周波数
#define BZ_FREQ_REb5    554                    //レb（Db）の周波数
#define BZ_FREQ_RE5     587                    //レ（D）の周波数
#define BZ_FREQ_MIb5    622                    //ミb（Eb）の周波数
#define BZ_FREQ_MI5     659                    //ミ（E）の周波数
#define BZ_FREQ_FA5     698                    //ファ（F）の周波数
#define BZ_FREQ_SOb5    740                    //ソb（Gb）の周波数
#define BZ_FREQ_SO5     784                    //ソ（G）の周波数
#define BZ_FREQ_LAb5    831                    //ラb（Ab）の周波数
#define BZ_FREQ_LA5     880                    //ラ（A）の周波数
#define BZ_FREQ_SIb5    932                    //シb（Bb）の周波数
#define BZ_FREQ_SI5     988                    //シ（B）の周波数

#define BZ_FREQ_DO6    1047                    //ド（C）の周波数
#define BZ_FREQ_REb6   1109                    //レb（Db）の周波数
#define BZ_FREQ_RE6    1174                    //レ（D）の周波数
#define BZ_FREQ_MIb6   1245                    //ミb（Eb）の周波数
#define BZ_FREQ_MI6    1318                    //ミ（E）の周波数
#define BZ_FREQ_FA6    1396                    //ファ（F）の周波数
#define BZ_FREQ_SOb6   1480                    //ソb（Gb）の周波数
#define BZ_FREQ_SO6    1568                    //ソ（G）の周波数
#define BZ_FREQ_LAb6   1660                    //ラb（Ab）の周波数
#define BZ_FREQ_LA6    1760                    //ラ（A）の周波数
#define BZ_FREQ_SIb6   1865                    //シb（Bb）の周波数
#define BZ_FREQ_SI6    1976                    //シ（B）の周波数

#define BZ_FREQ_DO7    2094                    //ド（C）の周波数
#define BZ_FREQ_REb7   2218                    //レb（Db）の周波数
#define BZ_FREQ_RE7    2348                    //レ（D）の周波数
#define BZ_FREQ_MIb7   2490                    //ミb（Eb）の周波数
#define BZ_FREQ_MI7    2636                    //ミ（E）の周波数
#define BZ_FREQ_FA7    2792                    //ファ（F）の周波数
#define BZ_FREQ_SOb7   2960                    //ソb（Gb）の周波数
#define BZ_FREQ_SO7    3136                    //ソ（G）の周波数
#define BZ_FREQ_LAb7   3320                    //ラb（Ab）の周波数
#define BZ_FREQ_LA7    3520                    //ラ（A）の周波数
#define BZ_FREQ_SIb7   3730                    //シb（Bb）の周波数
#define BZ_FREQ_SI7    3952                    //シ（B）の周波数

#define BZ_FREQ_DO8    4188                    //ド（C）の周波数
#define BZ_FREQ_REb8   4436                    //レb（Db）の周波数
#define BZ_FREQ_RE8    4696                    //レ（D）の周波数
#define BZ_FREQ_MIb8   4980                    //ミb（Eb）の周波数
#define BZ_FREQ_MI8    5272                    //ミ（E）の周波数
#define BZ_FREQ_FA8    5584                    //ファ（F）の周波数
#define BZ_FREQ_SOb8   5920                    //ソb（Gb）の周波数
#define BZ_FREQ_SO8    6272                    //ソ（G）の周波数
#define BZ_FREQ_LAb8   6640                    //ラb（Ab）の周波数
#define BZ_FREQ_LA8    7040                    //ラ（A）の周波数
#define BZ_FREQ_SIb8   7460                    //シb（Bb）の周波数
#define BZ_FREQ_SI8    7904                    //シ（B）の周波数

#define BZ_FREQ_DO9    8376                    //ド（C）の周波数
#define BZ_FREQ_REb9   4436*2                    //レb（Db）の周波数
#define BZ_FREQ_RE9    4696*2                    //レ（D）の周波数
#define BZ_FREQ_MIb9   4980*2                    //ミb（Eb）の周波数
#define BZ_FREQ_MI9    5272*2                    //ミ（E）の周波数
#define BZ_FREQ_FA9    5584*2                    //ファ（F）の周波数
#define BZ_FREQ_SOb9   5920*2                    //ソb（Gb）の周波数
#define BZ_FREQ_SO9    6272*2                    //ソ（G）の周波数
#define BZ_FREQ_LAb9   6640*2                    //ラb（Ab）の周波数
#define BZ_FREQ_LA9    7040*2                    //ラ（A）の周波数
#define BZ_FREQ_SIb9   7460*2                    //シb（Bb）の周波数
#define BZ_FREQ_SI9    7904*2                    //シ（B）の周波数

#define BZ_FREQ_DO10    8376*2                    //ド（C）の周波数
#define BZ_FREQ_REb10   4436*4                    //レb（Db）の周波数
#define BZ_FREQ_RE10    4696*4                    //レ（D）の周波数
#define BZ_FREQ_MIb10   4980*4                    //ミb（Eb）の周波数
#define BZ_FREQ_MI10    5272*4                    //ミ（E）の周波数
#define BZ_FREQ_FA10    5584*4                    //ファ（F）の周波数
#define BZ_FREQ_SOb10   5920*4                    //ソb（Gb）の周波数
#define BZ_FREQ_SO10    6272*4                    //ソ（G）の周波数
#define BZ_FREQ_LAb10   6640*4                    //ラb（Ab）の周波数
#define BZ_FREQ_LA10    7040*4                    //ラ（A）の周波数
#define BZ_FREQ_SIb10   7460*4                    //シb（Bb）の周波数
#define BZ_FREQ_SI10    7904*4                    //シ（B）の周波数


#define BZ_FREQ_DO11    8376*2*2                    //ド（C）の周波数
#define BZ_FREQ_REb11   4436*4*2                    //レb（Db）の周波数
#define BZ_FREQ_RE11    4696*4*2                    //レ（D）の周波数
#define BZ_FREQ_MIb11   4980*4*2                    //ミb（Eb）の周波数
#define BZ_FREQ_MI11    5272*4*2                    //ミ（E）の周波数
#define BZ_FREQ_FA11    5584*4*2                    //ファ（F）の周波数
#define BZ_FREQ_SOb11   5920*4*2                    //ソb（Gb）の周波数
#define BZ_FREQ_SO11    6272*4*2                    //ソ（G）の周波数
#define BZ_FREQ_LAb11   6640*4*2                    //ラb（Ab）の周波数
#define BZ_FREQ_LA11    7040*4*2                    //ラ（A）の周波数
#define BZ_FREQ_SIb11   7460*4*2                    //シb（Bb）の周波数
#define BZ_FREQ_SI11    7904*4*2                    //シ（B）の周波数



/* ブザー関係 */
//ブザードライバ
volatile float cycle;
volatile float pulsWidth;
volatile long cnt_bz;
int Flag_bz_on=0;
int Flag_bz_start=0;
long i_ms;
int num_notes=1;
unsigned char num_music=0;

//const int 

//;//150*2;
#define tempo 360//308


typedef struct{
	float freq;
	float beat;
	int Tempo_music;
}melody;




const melody FFfanfale[]={
	
	{BZ_FREQ_DO9,3, tempo},
/*	{BZ_FREQ_REST,3, tempo},

	{BZ_FREQ_DO9,2, tempo},
	{BZ_FREQ_DO9,2, tempo},
	{BZ_FREQ_DO9,2, tempo},

	{BZ_FREQ_DO9,6, tempo},

	{BZ_FREQ_LAb8,6, tempo},

	{BZ_FREQ_SIb8,6, tempo},

	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_SIb8,2, tempo},

	{BZ_FREQ_DO9,6, tempo},

	{BZ_FREQ_REST,24, tempo},

	{0,0,tempo}
*/
};


const melody HanayamataOP[306]={
/*	
	//ぱーっとぱーっと
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//はれやかにー
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_REST,4, tempo},
		//さかせましょー
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		//は)なのように
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_DO8,8, tempo},
	
		{BZ_FREQ_FA9,16, tempo},
	
		{BZ_FREQ_MIb9,8, tempo},
		{BZ_FREQ_SIb9,8, tempo},
	
		{BZ_FREQ_DO10,16, tempo},
	
		{BZ_FREQ_REb10,14, tempo},
		{BZ_FREQ_SIb7,2, tempo},
		//Aメロ　ほしーのはー
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_SIb7,2, tempo},
		//すこーしのー
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MIb8,1, tempo},
		{BZ_FREQ_FA8,1, tempo},
		//
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_MIb8,16, tempo},
		{BZ_FREQ_SIb7,2, tempo},
		//Aメロ2
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_SIb7,2, tempo},
	
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MIb8,1, tempo},
		{BZ_FREQ_FA8,1, tempo},
		//
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_REST,4, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		//ひ、ふ、
		{BZ_FREQ_SIb8,8, tempo},
		{BZ_FREQ_RE8,8, tempo},
		//み！いきおい
		{BZ_FREQ_MIb8,8, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb7,2, tempo},
		//よーくはーねーてー
		{BZ_FREQ_SIb8,4, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		{BZ_FREQ_LAb8,4, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_FA8,6, tempo},
	
		{BZ_FREQ_REST,4, tempo},
		//ふふふふ
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		//ゆーめはー
		{BZ_FREQ_SIb7,6, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO8,8, tempo},
		//ほーんきのー
		{BZ_FREQ_SI8,6, tempo},
		{BZ_FREQ_SI8,2, tempo},
		{BZ_FREQ_SI8,8, tempo},
		//ゆーめはー
		{BZ_FREQ_DO8,6, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO8,12, tempo},
		//なーんとかー
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_DO9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ちゃーちゃ、ちゃ、ちゃー↑ちゃー↓ちゃ→
		{BZ_FREQ_DO9,3, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_REST,1, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MI8,4, tempo},
	
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_SIb7,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
	
		{BZ_FREQ_SO8,3, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_REST,1, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_FA8,4, tempo},
		//散らさないでー
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		//もーともーと
		{BZ_FREQ_REb8,6, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		{BZ_FREQ_LAb8,6, tempo},
		{BZ_FREQ_REb8,2, tempo},
	
		{BZ_FREQ_MIb8,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_REST,4, tempo},
		{BZ_FREQ_MIb8,2, tempo},
	
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_DO9,2, tempo},
		{BZ_FREQ_DO9,12, tempo},
	
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_LAb8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
	
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,8, tempo},
	
		{BZ_FREQ_REST,4, tempo},
		{BZ_FREQ_MIb9,4, tempo},//チャーn/い
		{BZ_FREQ_MIb8,2, tempo},//ろ
		{BZ_FREQ_FA8,2, tempo}, //は
		{BZ_FREQ_SO8,2, tempo}, //に
		{BZ_FREQ_SIb8,2, tempo},//ほ
	
		//ぱーっとぱーっと
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//はれやかにー
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_SO9,4, tempo},//ハイ(びたーん
		//さかせましょー
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		//は)なのように
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_DO8,4, tempo},
		{BZ_FREQ_DO9,2, tempo},//ハイハイ
		{BZ_FREQ_DO9,2, tempo},
	
		{BZ_FREQ_DO8,4, tempo},
		{BZ_FREQ_RE8,4, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_FA8,6, tempo},
	
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_RE8,4, tempo},
		{BZ_FREQ_MIb8,4, tempo},
	
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SO8,8, tempo},
	
		{BZ_FREQ_FA8,6, tempo},
		{BZ_FREQ_MIb8,2, tempo},//い
		{BZ_FREQ_MIb8,2, tempo},//ろ
		{BZ_FREQ_FA8,2, tempo},//は
		{BZ_FREQ_SO8,2, tempo},//に
		{BZ_FREQ_SIb8,2, tempo},//ほ
		//ぱーっとぱーっと
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_DO9,6, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//はれやかにー
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,4, tempo},
		{BZ_FREQ_SO9,4, tempo},//ハイ(びたーん
		//さかせるおもいは
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_FA8,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_DO9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_DO9,2, tempo},
		//つねならむー
		{BZ_FREQ_REb9,2, tempo},
		{BZ_FREQ_REb9,2, tempo},
		{BZ_FREQ_REb9,2, tempo},
		{BZ_FREQ_DO9,2, tempo},
		{BZ_FREQ_SO8,2, tempo},
		{BZ_FREQ_SO9,2, tempo},//ハイハイ
		{BZ_FREQ_SO9,2, tempo},
		//ウン)だってー
		{BZ_FREQ_REST,4, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
	
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_RE8,4, tempo},
		{BZ_FREQ_MIb8,4, tempo},
		{BZ_FREQ_SIb8,1, tempo},
		{BZ_FREQ_SI8,1, tempo},
		//かぜまーかせー
		{BZ_FREQ_REb9,4, tempo},
		{BZ_FREQ_SI8,2, tempo},
		{BZ_FREQ_SIb8,4, tempo},
		{BZ_FREQ_LAb8,4, tempo},
		{BZ_FREQ_SIb8,18, tempo},
		//つぶやいてみた
		{BZ_FREQ_REST,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		//チャララ1回め
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb9,2, tempo},
		//チャララ2回め
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb9,2, tempo},
		//チャララ3回め
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb9,2, tempo},
		//チャララ4回め
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb9,2, tempo},
		//チャララ5回め
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_MIb10,2, tempo},
	
		{BZ_FREQ_RE10,6, tempo},
		{BZ_FREQ_SIb9,6, tempo},
		{BZ_FREQ_SO9,4, tempo},
	
		//おとめごころさ
		{BZ_FREQ_SIb9,2, tempo},//{BZ_FREQ_FA10,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
		{BZ_FREQ_MIb8,2, tempo},
	
		//ラチャチャ1回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ2回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ3回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ4回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ5回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ6回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ7回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ8回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ9回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_SIb8,2, tempo},
		//ラチャチャ10回め
		{BZ_FREQ_MIb9,2, tempo},
		{BZ_FREQ_SIb9,2, tempo},
		{BZ_FREQ_LAb9,2, tempo},
		{BZ_FREQ_SO9,2, tempo},
*/		{BZ_FREQ_MIb9,2, tempo}
		
		
		
};

const melody Legendary_Air_Raid_Machine[]={
/*	
	//チャンチャ
	{BZ_FREQ_DO8,3, tempo},
	{BZ_FREQ_DO8,1, tempo},
	//チャーン
	{BZ_FREQ_FA8,6, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_MI8,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_SO8,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	
	{BZ_FREQ_LAb8,6, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_DO9,3, tempo},
	{BZ_FREQ_DO9,1, tempo},
		
	{BZ_FREQ_RE9,4, tempo},
	{BZ_FREQ_SIb8,3, tempo},
	{BZ_FREQ_RE9,1, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_LAb8,3, tempo},
	{BZ_FREQ_DO9,1, tempo},
	
	{BZ_FREQ_SIb8,8, tempo},
	//{BZ_FREQ_RE9,2, tempo},
	{BZ_FREQ_REST,4, tempo},
	{BZ_FREQ_SIb8,3, tempo},
	{BZ_FREQ_SIb8,1, tempo},
	
	{BZ_FREQ_REb9,8, tempo},
	{BZ_FREQ_MIb9,2, tempo},
	{BZ_FREQ_REb9,2, tempo},
	{BZ_FREQ_DO9,2, tempo},
	{BZ_FREQ_REb9,2, tempo},
	
	{BZ_FREQ_DO9,6, tempo},
	{BZ_FREQ_LAb8,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_DO9,3, tempo},
	{BZ_FREQ_DO9,1, tempo},
	
	{BZ_FREQ_SIb8,4, tempo},
	{BZ_FREQ_SO8,3, tempo},
	{BZ_FREQ_SIb8,1, tempo},
	{BZ_FREQ_LAb8,4, tempo},
	{BZ_FREQ_FA8,3, tempo},
	{BZ_FREQ_LAb8,1, tempo},
	
	{BZ_FREQ_SO8,8, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_REST,4, tempo},
	
	{BZ_FREQ_FA9,6, tempo},
	{BZ_FREQ_FA9,2, tempo},
	{BZ_FREQ_MI9,2, tempo},
	{BZ_FREQ_FA9,2, tempo},
	{BZ_FREQ_SO9,2, tempo},
	{BZ_FREQ_FA9,2, tempo},
	
	{BZ_FREQ_LAb9,6, tempo},
	{BZ_FREQ_FA9,2, tempo},
	{BZ_FREQ_DO10,4, tempo},
	{BZ_FREQ_DO10,3, tempo},
	{BZ_FREQ_DO10,1, tempo},
	
	{BZ_FREQ_RE10,4, tempo},
	{BZ_FREQ_SIb9,3, tempo},
	{BZ_FREQ_RE10,1, tempo},
	{BZ_FREQ_DO10,4, tempo},
	{BZ_FREQ_LAb9,3, tempo},
	{BZ_FREQ_DO10,1, tempo},
	{BZ_FREQ_SIb9,12, tempo},
	{BZ_FREQ_SIb8,3, tempo},
	{BZ_FREQ_SIb8,1, tempo},
	
	{BZ_FREQ_REb9,6, tempo},
	{BZ_FREQ_REb9,2, tempo},
	{BZ_FREQ_MIb9,6, tempo},
	{BZ_FREQ_REb9,2, tempo},
	
	{BZ_FREQ_DO9,6, tempo},
	{BZ_FREQ_LAb8,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_DO9,3, tempo},
	{BZ_FREQ_DO9,1, tempo},
	
	{BZ_FREQ_SIb8,6, tempo},
	{BZ_FREQ_LAb8,2, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_DO8,4, tempo},
	
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_FA9,3, tempo},
	{BZ_FREQ_MI9,1, tempo},
	
	{BZ_FREQ_MIb9,4, tempo},
	{BZ_FREQ_MIb9,3, tempo},
	{BZ_FREQ_MIb9,1, tempo},
	{BZ_FREQ_MIb9,2, tempo},
	{BZ_FREQ_SIb8,4, tempo},
	{BZ_FREQ_REb9,2, tempo},
	
	{BZ_FREQ_REb9,4, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_SI8,4, tempo},
	{BZ_FREQ_DO9,4, tempo},
	
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_DO9,3, tempo},
	{BZ_FREQ_DO9,1, tempo},
	{BZ_FREQ_REb9,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_SO8,2, tempo},
	
	{BZ_FREQ_SIb8,4, tempo},
	{BZ_FREQ_LAb8,4, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_LAb8,4, tempo},
	
	{BZ_FREQ_LAb8,8, tempo},
	{BZ_FREQ_SI8,4, tempo},
	{BZ_FREQ_SIb8,3, tempo},
	{BZ_FREQ_LAb8,1, tempo},
	
	{BZ_FREQ_DO9,8, tempo},
	{BZ_FREQ_FA9,4, tempo},
	{BZ_FREQ_REST,4, tempo},
	
	{BZ_FREQ_FA9,4, tempo},
	{BZ_FREQ_FA9,3, tempo},
	{BZ_FREQ_FA9,1, tempo},
	{BZ_FREQ_FA9,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_FA9,2, tempo},
	
	{BZ_FREQ_LAb9,8, tempo},
*/	{BZ_FREQ_SO9,4, tempo}
	
	
	
	
};


const melody Soreha_bokutachi_no_kiseki_LOVELIVE[]={
/*	
	//さぁー、ゆめをー
	{BZ_FREQ_FA8,10, tempo},
	{BZ_FREQ_MI8,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_DO9,10, tempo},
	
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_DO9,2, tempo},
	
	{BZ_FREQ_DO9,4, tempo},
	{BZ_FREQ_SIb8,4, tempo},
	{BZ_FREQ_LA8,2, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_SIb8,4, tempo},
	
	{BZ_FREQ_SIb8,2, tempo},
	{BZ_FREQ_LA8,4, tempo},
	{BZ_FREQ_SO8,2, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_FA8,10, tempo},
	
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_SO8,2, tempo},
	{BZ_FREQ_LA8,2, tempo},
	
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_SO8,2, tempo},
	{BZ_FREQ_LA8,2, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	
	{BZ_FREQ_SIb8,6, tempo},
	{BZ_FREQ_DO9,6, tempo},
	{BZ_FREQ_SIb8,4, tempo},
	
	{BZ_FREQ_LA8,4, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_MI8,4, tempo},
	{BZ_FREQ_FA8,6, tempo},
	
	{BZ_FREQ_DO8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_MI8,4, tempo},
	
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_LA8,4, tempo},
	{BZ_FREQ_SIb8,4, tempo},
	
	{BZ_FREQ_LA8,4, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_RE8,2, tempo},
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_DO8,18, tempo},
	
	{BZ_FREQ_REST,4, tempo},
	{BZ_FREQ_DO8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_MI8,4, tempo},
	
	{BZ_FREQ_FA8,4, tempo},
	{BZ_FREQ_SO8,4, tempo},
	{BZ_FREQ_LA8,4, tempo},
	{BZ_FREQ_FA8,4, tempo},
	
	{BZ_FREQ_SIb8,4, tempo},
	{BZ_FREQ_LA8,2, tempo},
	{BZ_FREQ_SO8,2, tempo},
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_FA8,2, tempo},
	{BZ_FREQ_REST,2, tempo},
	{BZ_FREQ_DO9,14, tempo},
	//Aメロ つ
	{BZ_FREQ_DO8,4, tempo},
	//よーい つ
	{BZ_FREQ_SO8,6, tempo},
	{BZ_FREQ_FA8,6, tempo},
	{BZ_FREQ_DO8,4, tempo},
	//よーい ね
	{BZ_FREQ_SO8,6, tempo},
	{BZ_FREQ_FA8,6, tempo},
*/	{BZ_FREQ_DO8,4, tempo},
	//がーい
	
	
};
	

int size_FFfanfale = sizeof(FFfanfale)/( sizeof(FFfanfale->freq) + sizeof(FFfanfale->beat) + sizeof(FFfanfale->Tempo_music) );
int size_HanayamataOP = sizeof(HanayamataOP)/( sizeof(HanayamataOP->freq) + sizeof(HanayamataOP->beat) + sizeof(HanayamataOP->Tempo_music) );
int size_Legendary_Air_Raid_Machine = sizeof(Legendary_Air_Raid_Machine)/( sizeof(Legendary_Air_Raid_Machine->freq) + sizeof(Legendary_Air_Raid_Machine->beat) + sizeof(Legendary_Air_Raid_Machine->Tempo_music) );
int size_Soreha_bokutachi_no_kiseki_LOVELIVE = sizeof(Soreha_bokutachi_no_kiseki_LOVELIVE)/( sizeof(Soreha_bokutachi_no_kiseki_LOVELIVE->freq) + sizeof(Soreha_bokutachi_no_kiseki_LOVELIVE->beat) + sizeof(Soreha_bokutachi_no_kiseki_LOVELIVE->Tempo_music) );




int PlayMusic(melody Tunes, int Size_Tunes, int TEMPO);

	


void init_mtu_speaker(void){
	//動作選択
	PFC.PECRL2.BIT.PE7MD = 1;		//TIOC2をMTU用端子に設定
	PFC.PEIORL.BIT.B7 = 1;			//TIOC2を出力端子に設定
	STB.CR4.BIT._MTU2 = 0;			//MTUスタンバイ解除(クロック供給開始)
	MTU2.TSTR.BYTE = 0; 			//カウンタ動作停止
	MTU22.TIER.BIT.TGIEB = 1;		//TGRBコンペアマッチによる割り込みを許可
	INTC.IPRD.BIT._MTU20G = 14;		//割り込み優先度を高めに設定　14


	//カウンタクロックの選択
	MTU22.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us):0.64usec
	
	//カウンタクリア要因の選択
	MTU22.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	
	//アウトプットコンペアレジスタの選択(コンペアマッチ前後での波形出力レベルの設定0/1)
	MTU22.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU22.TIOR.BIT.IOB = 5; 		// 初期状態:1 コンペアマッチ:0
	
	//周期・デューティー設定
	MTU22.TGRA = 3551;				//PWM周期 10msec=15625cnt 1msec=1563cnt 
			//今は適当な値(ドだったか？)
	MTU22.TGRB = MTU22.TGRA/4;		//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？ 
			//現在はTGRAの半分、つまり、TGRAが周期である矩形波を与えていることになっている→メインで値を変更し、音階が変わるようになっている
	
	//PWMモードの設定
	MTU22.TMDR.BIT.MD = 3; 			// PWM2モード
	
}





int Switch_music(mode){
	
	if(SW1==0&SW2!=0){//白スイッチのみON
			mode += 1;
			if(mode >= 5){mode=0;}
			wait(200);
			switch(mode){
				case 0://
					LED0=0;
					LED1=0;
					LED2=0;
					LED3=0;
					LED4=0;
													
					break;
				case 1:
					LED0=1;
					LED1=0;
					LED2=0;
					LED3=0;
					LED4=0;
					break;
					
				case 2:
					LED0=0;
					LED1=1;
					LED2=0;
					LED3=0;
					LED4=0;
					break;
				case 3:
					LED0=1;
					LED1=1;
					LED2=0;
					LED3=0;
					LED4=0;					
					break;
				case 4:
					LED0=0;
					LED1=0;
					LED2=1;
					LED3=0;
					LED4=0;
				
					break;
				case 5:
					LED0=1;
					LED1=0;
					LED2=1;
					LED3=0;
					LED4=0;
				
					break;
				case 6:
					LED0=0;
					LED1=1;
					LED2=1;
					LED3=0;
					LED4=0;
				
					break;
				case 7:
					LED0=1;
					LED1=1;
					LED2=1;
					LED3=0;
					LED4=0;
				
					break;
				case 8:
					LED0=0;
					LED1=0;
					LED2=0;
					LED3=1;
					LED4=0;
				
					break;
				case 9:
					LED0=1;
					LED1=0;
					LED2=0;
					LED3=1;
					LED4=0;
				
					break;
				case 10:
					LED0=0;
					LED1=1;
					LED2=0;
					LED3=1;
					LED4=0;
				
					break;
				case 11:
					LED0=1;
					LED1=1;
					LED2=0;
					LED3=1;
					LED4=0;
				
					break;
				case 12:
					LED0=0;
					LED1=0;
					LED2=1;
					LED3=1;
					LED4=0;
				
					break;
				case 13:
					LED0=1;
					LED1=0;
					LED2=1;
					LED3=1;
					LED4=0;
				
					break;
				case 14:
					LED0=0;
					LED1=1;
					LED2=1;
					LED3=1;
					LED4=0;
				
					break;
				case 15:
					LED0=1;
					LED1=1;
					LED2=1;
					LED3=1;
					LED4=0;
				
					break;
				
			}
								
		}
	
	return mode;
}


int PlayMusic(melody Tunes, int Size_Tunes, int TEMPO)
{
	//LED3=1;
	
	if(Flag_bz_start==0)//ブザーが鳴り始める前(0)なら入る
	{
		//LED2=1;
		//freq,beat,tempoからどれだけ鳴らすかi_msを算出する
		//ブザー処理(メロディ)
	    i_ms = (Tunes.beat * 60 * 1000 / 2) / TEMPO;
	    
		if (Tunes.freq == 0)//休符のとき(ゼロ割を防ぐため分ける)
	    {
	        
	    }
	    else
	    {
			//LED1=1;
	        //周波数を設定
	        cycle = (float)MTU_BZ_CLOCK / (float)Tunes.freq;
	        pulsWidth = cycle /2.0;
	        MTU_BZ_CYCLE = (short)cycle;
	        MTU_BZ_PULSWIDTH = (short)pulsWidth;
	        //発振
	        MTU_BZ_ON();            //ブザーの発振を開始
	    }
		
		cnt_bz = 0;
		Flag_bz_start=1;
		
	}//Flag_bz_start
	
	//カウントがi_ms増えるまでCMT回す
	cnt_bz++;
	
	if(cnt_bz>=i_ms*0.95)
	{
		MTU_BZ_OFF();           //ブザーの発振を終了
	}
	
	if(cnt_bz>=i_ms)//残りの0.1ms分だけ待ってフラグクリア
	{
		num_notes++;
		if(num_notes>Size_Tunes)//306)//sizeof(HanayamataOP))
		{
			num_notes=1;
		}
		Flag_bz_start = 0;
		cnt_bz = 0;
	}
	
		
	//return;
}



//MTU2割り込み関数(この関数で、左右のモータのデューティをセットする
void interrupt_speaker(void){
	
	int cntmtu = 0;
	
//	LED0 = 0;
//	LED1 = 0;
	
	MTU22.TSR.BIT.TGFB = 0;	//コンペアマッチ発生フラグクリア
	//MTU20.TGRB = speed_r;	//右モータPWMのデューティー

}


