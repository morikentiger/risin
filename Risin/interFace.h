/* interFace.h */
/*
#define isGreenSwitch()	(SW1!=0&SW2==0)
#define isWhiteSwitch()	(SW1==0&SW2!=0)
*/

void LED_all(char LED_Combined)
{
	
	LED0 = (LED_Combined & 0x01);
	LED1 = (LED_Combined & 0x02) >> 1;
	LED2 = (LED_Combined & 0x04) >> 2;
	LED3 = (LED_Combined & 0x08) >> 3;
	LED4 = (LED_Combined & 0x10) >> 4;
	
}


/* LEDFlash.c */

int LEDFlash(char LED_Combined, int cycle_ms, int freq_flash)
{
	int i;
	
	for(i=0;i<=freq_flash;i++)
	{
		LED_all(LED_Combined);
		wait(cycle_ms);
		LED_all(0);				//すべて消す
		wait(cycle_ms);
		
	}
	
}


int KnightRider(char num, int Looptime_KR)
{
	
	char i;
	int Time_KR = 50;
	
	volatile unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + Looptime_KR;	//
		
	//while(wait_end > cntcmt){
	for(i=0;i<num;i++)
	{
/*		LED0 = 1;
		LED1 = 0;
		LED2 = 1;
		LED3 = 0;
		LED4 = 1;
		
		wait(1000);
		
		LED0 = 0;
		LED1 = 1;
		LED2 = 0;
		LED3 = 1;
		LED4 = 0;
		
		wait(1000);
*/
		LED0 = 1;
		LED1 = 0;
		LED2 = 0;
		LED3 = 0;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 1;
		LED2 = 0;
		LED3 = 0;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 0;
		LED2 = 1;
		LED3 = 0;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 0;
		LED2 = 0;
		LED3 = 1;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 0;
		LED2 = 0;
		LED3 = 0;
		LED4 = 1;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 0;
		LED2 = 0;
		LED3 = 1;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 0;
		LED2 = 1;
		LED3 = 0;
		LED4 = 0;
		wait(Time_KR);
		
		LED0 = 0;
		LED1 = 1;
		LED2 = 0;
		LED3 = 0;
		LED4 = 0;
		wait(Time_KR);
					
	}
	LED0 = 1;
	LED1 = 0;
	LED2 = 0;
	LED3 = 0;
	LED4 = 0;
	wait(Time_KR);
	
	LED0 = 0;
	LED1 = 0;
	LED2 = 0;
	LED3 = 0;
	LED4 = 0;
	
}


int LEDsimasima(int Looptime_KR)
{
	
	int Time_simasima = 100;
	
	volatile unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + Looptime_KR;	//
	
	while(wait_end > cntcmt){
				
		LED0 = 1;
		LED1 = 0;
		LED2 = 1;
		LED3 = 0;
		LED4 = 1;
		
		wait(Time_simasima);
		
		LED0 = 0;
		LED1 = 1;
		LED2 = 0;
		LED3 = 1;
		LED4 = 0;
		
		wait(Time_simasima);
	}
	LED0 = 0;
	LED1 = 0;
	LED2 = 0;
	LED3 = 0;
	LED4 = 0;
	
}//LEDsimasima


int LEDflashWall(void)
{
	if(refSEN_R  >= TH_Wall_R ){LED0=1;}else{LED0=0;}
	if( (refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR) ){LED2=1;}else{LED2=0;}
	if(refSEN_L  >= TH_Wall_L ){LED4=1;}else{LED4=0;}
}		

