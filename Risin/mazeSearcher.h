/* mazeSearcher.h */

//
typedef struct {
	unsigned char	StepMap[MAPSIZE_X][MAPSIZE_Y];//歩数マップ
	unsigned char 	Wall[MAPSIZE_X][MAPSIZE_Y];	//区間ごとの壁情報(絶対座標)
	unsigned char	X;				//現在座標X
	unsigned char	Y;				//現在座標Y
	int				Psi;			//現在姿勢ψ
}t_mapState;

typedef struct {
	unsigned char				:5	;	//あまり
	unsigned char	isAskew		:1	;	//斜めなら1
	unsigned char	Turn		:5	;	//斜め含む全てのターン+2パターン(終了判定)
	unsigned char	Straight	:5	;	//直線部分のパス(0~31の32パターン
	
}t_Path;


//迷路情報構造体の宣言
t_mapState mapState;	//
t_Path Path1Pivot[PATH_SIZE]		 = {0};		//超新地旋回のみのパス
t_Path Path2PivotAskew[PATH_SIZE]	 = {0};		//超新地旋回で斜めパス
t_Path Path3LittleTurn[PATH_SIZE]	 = {0};		//小回りターン導入パス
t_Path Path4BigTurn[PATH_SIZE]		 = {0};		//大回りターン導入パス
t_Path Path5Askew[PATH_SIZE]		 = {0};		//斜めのターン導入パス

//歩数マップ関連の変数の宣言
	unsigned char nextStepMarker,nextStepChaser,nextStepValue;	//歩数マップ生成時のインデックス
	//max255	次の歩数の	印		印を追うモノ	歩数の値	次に更新する座標 16*16
	unsigned char nextCoordinate[256] = {0};// = { 0 };
	unsigned char searchStepMapX,searchStepMapY;
	
//足立法による行動選択に関する変数の宣言
	char vectorFrontX,vectorFrontY,vectorRightX,vectorRightY,vectorLeftX,vectorLeftY;	
		//方向によりXY座標に足すベクトルの成分(行動選択前に毎回更新されるので初期化不要)
	char frontStep,rightStep,leftStep;
		//行動選択時に比較する歩数を一次的に保存する変数(行動選択前に毎回更新されるので初期化不要)

//最短パス生成に関する変数の宣言
	char frontWall,rightWall,leftWall;
		//

	
//ゴール座標の変数定義
int X_goal;
int Y_goal;

char GoalCategory = 1;//ゴールの場合。enum使ってカテゴリー分けしたほうがいい。141102_0822



//volatile int Mode_Motion=100;	//動作を選択する(0:なし、1:前後、2:回転

volatile int Flag_wall_end = 0;	//ケツ(end)が壁に付いているとき1。スタートとUターン後、1になる

volatile int Flag_motion_LHM = 0;	//左手法での行動選択フラグ

//デバック用フラグ
char Flag_debug_stepmap = 0;		//歩数マップのデバッグ(myprintfによる計算をログの出力)を行う場合1になる。
char Flag_debug_kagensoku = 0;		//加減速のデバッグを行う場合1になる
char Flag_Not_run = 0;				//走らせない(デバックモード)時は1にする。



//座標を更新する
void UpdateCoordinate(t_mapState *mapState)
{
	
	switch(mapState->Psi)
	{
		//一般に
		case 0://North北向き
		mapState->X += 0;
		mapState->Y += 1;
		break;
		
		case 1://West西向き
		mapState->X += -1;
		mapState->Y += 0;
		break;
		
		case 2://South南向き
		mapState->X += 0;
		mapState->Y += -1;
		break;
		
		case 3://East東向き
		mapState->X += 1;
		mapState->Y += 0;
		break;
		
/*		//斜めを考慮すると
		case 45://NorthEast北東向き
		mapState->X += 1;
		mapState->Y += 1;
		break;
		
		case 135://NorthWest北西向き
		mapState->X += -1;
		mapState->Y += 1;
		break;
		
		case 225://SouthWest南西向き
		mapState->X += -1;
		mapState->Y += -1;
		break;
		
		case 315://SouthEast南東向き
		mapState->X += 1;
		mapState->Y += -1;
		break;
*/				
	}
	
	
}//UpdateCoordinate

void acquireSensorValue(void)
{

	refSEN_FR = SEN_FR;
	refSEN_R  = SEN_R;
	refSEN_L  = SEN_L;
	refSEN_FL = SEN_FL;
	
}

void initMap(t_mapState *mapState)
{
	char m,n;
	
	
	
	//壁情報の初期化(オールリセット(未知)なので上書きの=)
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState->Wall[m][n] = 0x00;//0000 0000
		}
	}
	//境界壁壁を入力(情報追加なので=でなくビットOR「|=」を使う
	//北(0)の境界を「壁あり」
	for(m=0;m<16;m++)
	{
		n=15;
		mapState->Wall[m][n] |= 0x11;// 0001 0001
	}
	//西(1)の境界を「壁あり」
	m=0;
	for(n=0;n<16;n++)
	{
		mapState->Wall[m][n] |= 0x22;// 0010 0010
	}
	//南(2)の境界を「壁あり」
	for(m=0;m<16;m++)
	{
		n=0;
		mapState->Wall[m][n] |= 0x44;// 0100 0100
	}
	//東(3)の境界を「壁あり」
	m=15;
	for(n=0;n<16;n++)
	{
		mapState->Wall[m][n] |= 0x88;// 1000 1000
	}
	//スタート座標の右壁を「壁あり」(上書きでなく、追加なので、=でなくビットOR「|=」とする。(右壁だけになっちゃうからね)
	mapState->Wall[0][0] |= 0x88;// 1000 1000
	mapState->Wall[1][0] |= 0x22;// 0010 0010
	//スタート座標の前壁を「壁なし」
	mapState->Wall[0][0] |= 0x10;// 0001 0000
	mapState->Wall[0][1] |= 0x40;// 0100 0000



	/*歩数マップの初期化*/
	//すべての区間の歩数を255(歩数未導出)とする
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState->StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
		}
	}
	
	
	//現在座標と姿勢の初期化
	mapState->X = 0;
	mapState->Y = 0;
	mapState->Psi = North;//つまり0

	
}//initMap


UpdateBehindWall(t_mapState *mapState)
{
	//＜壁ウラ更新＞隣接する区間の壁を更新する//
	//迷路の端(区間座標が0or15)にいる場合は、処理を行わない壁があることに注意
	//北(0)の壁ウラ更新
	if(!(mapState->Y==15))
	{
		mapState->Wall[mapState->X][mapState->Y+1] 
		|= ( (mapState->Wall[mapState->X][mapState->Y]&0x01)==0x01 )*0x44 + !( (mapState->Wall[mapState->X][mapState->Y]&0x01)==0x01 )*0x40;
	}
	//西(1)の壁ウラ更新
	if(!(mapState->X==0))
	{
		mapState->Wall[mapState->X-1][mapState->Y] 
		|= ( (mapState->Wall[mapState->X][mapState->Y]&0x02)==0x02 )*0x88 + !( (mapState->Wall[mapState->X][mapState->Y]&0x02)==0x02 )*0x80;
	}
	//南(2)の壁ウラ更新
	if(!(mapState->Y==0))
	{
		mapState->Wall[mapState->X][mapState->Y-1] 
		|= ( (mapState->Wall[mapState->X][mapState->Y]&0x04)==0x04 )*0x11 + !( (mapState->Wall[mapState->X][mapState->Y]&0x04)==0x04 )*0x10;
	}
	//東(3)の壁ウラ更新
	if(!(mapState->X==15))
	{
		mapState->Wall[mapState->X+1][mapState->Y] 
		|= ( (mapState->Wall[mapState->X][mapState->Y]&0x08)==0x08 )*0x22 + !( (mapState->Wall[mapState->X][mapState->Y]&0x08)==0x08 )*0x20;
	}
	
}//UpdateBehindWall

int MakeStepMap(t_mapState *mapState)
{
	char m,n;
	////////////////////////////////////////////////////
	///	現状の壁情報から歩数マップを作成する	///
	////////////////////////////////////////////////////
//		
//	myprintf("stepmap_init\n\r");

	//(一区間ごとの)歩数マップの初期化及び歩数マップ生成のための変数の初期化/
	//すべての区間の歩数を255(歩数未導出)とする
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState->StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
		}
	}

//ゴール座標によって、変更する
if(GoalCategory == 1)//ゴール座標が「ゴールの場合」
{

	//ゴールのサイズ(1マスか4マスかのどちらか)による初期化及びゴール座標を0に初期化
	if(GOAL_NUM == 1)
	{
		mapState->StepMap[GOAL_X][GOAL_Y] = 0;	//ゴール座標の初期化

		nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[nextStepChaser] = (GOAL_X << 4) + (GOAL_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;

	}
	else if(GOAL_NUM == 4)
	{
		//全日本などの16×16区画の迷路のゴール
		mapState->StepMap[GOAL_XL][GOAL_YL] = 0;
		mapState->StepMap[GOAL_XL][GOAL_YH] = 0;
		mapState->StepMap[GOAL_XH][GOAL_YL] = 0;
		mapState->StepMap[GOAL_XH][GOAL_YH] = 0;

		nextStepMarker = 3;//4;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[0] = (GOAL_XL << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[1] = (GOAL_XL << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[2] = (GOAL_XH << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[3] = (GOAL_XH << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする

		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;

	}
	else
	{
		LEDsimasima(2000);
		myprintf("GOAL_NUM=%d\n\r",GOAL_NUM);
		//Switch_LED
	}
}	
else if(GoalCategory==0)//ゴール座標が「スタート」の場合
{
	//帰りの歩数
	mapState->StepMap[0][0] = 0;	//スタート座標をゴール座標に初期化

	nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
	nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
	nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
	nextCoordinate[nextStepChaser] = (START_X << 4) + (START_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
	//ループの最初に、周囲の歩数を求める座標X,Y
	searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
	searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
	
}
else
{
	LEDsimasima(2000);
	myprintf("GOAL_CATEGORY=%d\n\r",GoalCategory);
	//Switch_LED
}
	
	//デバッグ
	if(Flag_debug_stepmap == 1)
	{
		myprintf("1st initial value check\n\r");
		//初めに、歩数マップ関係の変数の初期値を見ておく
		myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
		myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
		myprintf("nextStepValue =%4d\n\r",nextStepValue);
		myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\r"
		,(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
	}



//	LED4=1;
	//歩数マップを作成する
	while( (nextStepMarker>=nextStepChaser) & !(nextStepChaser==255)  )//歩数マップが完成する(≡印が印ヲ追ウモノに越される※イメージです)までループする
	{
	//	myprintf("in_while(nextStepMarker<=nextStepChaser)\n\n\r");

		//今ループで探索する座標周辺の歩数を確認する

		//北の座標の歩数値更新
	//	myprintf("N_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x11)==0 && mapState->StepMap[searchStepMapX][searchStepMapY+1]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x11)==0x10 | (mapState->Wall[searchStepMapX][searchStepMapY]&0x10)==0x00 ) 
		&& mapState->StepMap[searchStepMapX][searchStepMapY+1]==0xFF 
		)//ここまでif文
		//北の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので北端(Y=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX][searchStepMapY+1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = (searchStepMapX<<4) + (searchStepMapY+1);
			if(Flag_debug_stepmap == 1)
			{	
				myprintf("North\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y+1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY+1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r"
				,nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//西の座標の歩数値更新
	//	myprintf("W_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x22)==0 && mapState->StepMap[searchStepMapX-1][searchStepMapY]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x22)==0x20 | (mapState->Wall[searchStepMapX][searchStepMapY]&0x20)==0x00 ) 
		&& mapState->StepMap[searchStepMapX-1][searchStepMapY]==0xFF 
		)//ここまでif文
		//西の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので西端(X=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX-1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX-1)<<4 ) + searchStepMapY;
	
			if(Flag_debug_stepmap == 1)
			{
				myprintf("West(NISHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X-1,Y = (%3d,%3d)\n\r",searchStepMapX-1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//南の座標の歩数値更新
	//	myprintf("S_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x44)==0 && mapState->StepMap[searchStepMapX][searchStepMapY-1]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x44)==0x40 | (mapState->Wall[searchStepMapX][searchStepMapY]&0x40)==0x00 ) 
		&& mapState->StepMap[searchStepMapX][searchStepMapY-1]==0xFF 
		)//ここまでif文
		//南の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので南端(Y=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX][searchStepMapY-1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX)<<4 ) + (searchStepMapY - 1);
	
			if(Flag_debug_stepmap == 1)
			{
		
				myprintf("South\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y-1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY-1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//東の座標の歩数値更新
	//	myprintf("E_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x88)==0 && mapState->StepMap[searchStepMapX+1][searchStepMapY]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x88)==0x80 | (mapState->Wall[searchStepMapX][searchStepMapY]&0x80)==0x00 ) 
		&& mapState->StepMap[searchStepMapX+1][searchStepMapY]==0xFF
		)//ここまでif文
		//東の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので東端(X=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX+1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX + 1)<<4)  + (searchStepMapY);
	
			if(Flag_debug_stepmap == 1)
			{
				myprintf("East(HIGASHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X+1,Y = (%3d,%3d)\n\r",searchStepMapX+1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//次へ進む
		nextStepChaser++; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		//今ループで周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0 )>>4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
		//次に探索する座標の歩数+1が、周囲の区間に入れるべき値
		nextStepValue = mapState->StepMap[searchStepMapX][searchStepMapY] + 1 ;

		if(Flag_debug_stepmap == 1)
		{
	
			//デバック
			myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
			myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
			myprintf("nextStepValue =%4d\n\r",nextStepValue);
			myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\n\r"
			,(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
		}
		

	}//while
			
}//MakeStepMap


void MakeShortestStepMap(t_mapState *mapState)
{
		
	/////////////////////////////////////////////////////////////////////////////////////////
	//最短歩数経路の歩数マップを作成（※既知区間のみ歩数を更新。未知区間は歩数は255のまま）//
	/////////////////////////////////////////////////////////////////////////////////////////
	int m,n;
	
	LEDFlash(0x1,200,2);
	wait(50);
//	myprintf("stepmap_init\n\r");

	//(一区間ごとの)歩数マップの初期化及び歩数マップ生成のための変数の初期化/
	//すべての区間の歩数を255(歩数未導出)とする
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState->StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
		}
	}

	//ゴールのサイズ(1マスか4マスかのどちらか)による初期化及びゴール座標を0に初期化
	if(GOAL_NUM == 1)
	{
		mapState->StepMap[GOAL_X][GOAL_Y] = 0;	//ゴール座標の初期化

		nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[nextStepChaser] = (GOAL_X << 4) + (GOAL_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;

	}
	else if(GOAL_NUM == 4)
	{
		//全日本などの16×16区画の迷路のゴール
		mapState->StepMap[GOAL_XL][GOAL_YL] = 0;
		mapState->StepMap[GOAL_XL][GOAL_YH] = 0;
		mapState->StepMap[GOAL_XH][GOAL_YL] = 0;
		mapState->StepMap[GOAL_XH][GOAL_YH] = 0;

		nextStepMarker = 3;//4;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[0] = (GOAL_XL << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[1] = (GOAL_XL << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[2] = (GOAL_XH << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[3] = (GOAL_XH << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする

		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;

	}
	else
	{
		LEDsimasima(2000);
		//Switch_LED
	}

//	myprintf("stepmap_init2\n\r");
		

//	myprintf("stepmap_will_be_making\n\n\r");

	if(Flag_debug_stepmap == 1)
	{
		myprintf("1st initial value check\n\r");
		//初めに、歩数マップ関係の変数の初期値を見ておく
		myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
		myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
		myprintf("nextStepValue =%4d\n\r",nextStepValue);
		myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
	}

//	LED4=1;
	//歩数マップを作成する
	while( (nextStepMarker>=nextStepChaser) & !(nextStepChaser==255)  )//歩数マップが完成する(≡印が印ヲ追ウモノに越される※イメージです)までループする
	{
	//	myprintf("in_while(nextStepMarker<=nextStepChaser)\n\n\r");

		//今ループで探索する座標周辺の歩数を確認する

		//北の座標の歩数値更新
	//	myprintf("N_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x11)==0 && mapState->StepMap[searchStepMapX][searchStepMapY+1]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x11)==0x10 )//| (mapState->Wall[searchStepMapX][searchStepMapY]&0x10)==0x00 ) 
		&& mapState->StepMap[searchStepMapX][searchStepMapY+1]==0xFF 
		&& ( (mapState->Wall[searchStepMapX][searchStepMapY+1]&0xF0) ==240) 
		)//ifここまで
		//北の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)、かつ、既知区間であれば… &&なので北端(Y=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX][searchStepMapY+1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = (searchStepMapX<<4) + (searchStepMapY+1);
			if(Flag_debug_stepmap == 1)
			{	
				myprintf("North\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y+1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY+1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//西の座標の歩数値更新
	//	myprintf("W_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x22)==0 && mapState->StepMap[searchStepMapX-1][searchStepMapY]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x22)==0x20 )//| (mapState->Wall[searchStepMapX][searchStepMapY]&0x20)==0x00 ) 
		&& mapState->StepMap[searchStepMapX-1][searchStepMapY]==0xFF  
		&& ( (mapState->Wall[searchStepMapX-1][searchStepMapY]&0xF0)==240 )
		)//ifここまで
		//西の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので西端(X=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX-1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX-1)<<4 ) + searchStepMapY;

			if(Flag_debug_stepmap == 1)
			{
				myprintf("West(NISHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X-1,Y = (%3d,%3d)\n\r",searchStepMapX-1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//南の座標の歩数値更新
	//	myprintf("S_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x44)==0 && mapState->StepMap[searchStepMapX][searchStepMapY-1]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x44)==0x40 )//| (mapState->Wall[searchStepMapX][searchStepMapY]&0x40)==0x00 ) 
		&& mapState->StepMap[searchStepMapX][searchStepMapY-1]==0xFF  
		&& ( (mapState->Wall[searchStepMapX][searchStepMapY-1]&0xF0)==240 )
		)//ifここまで
		//南の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので南端(Y=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX][searchStepMapY-1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX)<<4 ) + (searchStepMapY - 1);

			if(Flag_debug_stepmap == 1)
			{
	
				myprintf("South\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y-1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY-1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//東の座標の歩数値更新
	//	myprintf("E_estimate=%d\n\n\r",(mapState->Wall[searchStepMapX][searchStepMapY]&0x88)==0 && mapState->StepMap[searchStepMapX+1][searchStepMapY]==0xFF);
		if(
		( (mapState->Wall[searchStepMapX][searchStepMapY]&0x88)==0x80 )//| (mapState->Wall[searchStepMapX][searchStepMapY]&0x80)==0x00 ) 
		&& mapState->StepMap[searchStepMapX+1][searchStepMapY]==0xFF  
		&& ( (mapState->Wall[searchStepMapX+1][searchStepMapY]&0xF0)==240 )
		)
		//東の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので東端(X=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState->StepMap[searchStepMapX+1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX + 1)<<4)  + (searchStepMapY);

			if(Flag_debug_stepmap == 1)
			{
				myprintf("East(HIGASHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X+1,Y = (%3d,%3d)\n\r",searchStepMapX+1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}

		//次へ進む
		nextStepChaser++; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		//今ループで周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0 )>>4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
		//次に探索する座標の歩数+1が、周囲の区間に入れるべき値
		nextStepValue = mapState->StepMap[searchStepMapX][searchStepMapY] + 1 ;

		if(Flag_debug_stepmap == 1)
		{

			//デバック
			myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
			myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
			myprintf("nextStepValue =%4d\n\r",nextStepValue);
			myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
		}
	

	}//while
	
	//未知区間だったら、255にする
/*	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			if( ((mapState->Wall[m][n])&0xf0)!=0xf0 )
			{
				mapState->StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
			}
		}
	}
*/	

	//導出終了の合図
	LEDFlash(16,200,3);
	wait(50);
}//MakeShortestStepMap


/*
int MakePath()
{

}//MakePath

*/









/////////////////////////////////////
////////////////////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/////////////////////////////////////
////////////////////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/////////////////////////////////////
////////////////////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/////////////////////////////////////
////////////////////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/////////////////////////////////////
////////////////////////////////////////////
//////////////////////////////////////////////////
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


void lefthandNonstop(void)
{
	//変数の初期化
	int i;
	int m,n;							//2次元配列用のインデックス(for文で回す時に使う) unsigned char型だと15から0までのforループ作ると死ぬ？
	int cntLHM = 1;						//なぞのカウンタ。前に左手法にランダム要素を入れようとしたときに利用した
	int time_run = 200;//200;//[msec]	//走行動作の間の待ち時間
	unsigned char tempWall = 0x00;		//壁情報のバッファ(開店前のデータを一時的に保存しておく変数)
										//これを一区画毎に初期化しなかったせいで壁情報が読めなかったorz
	char Flag_edge_maze = 0;			//区画が0,15にあることを表そうと思ったが、いらない
	
//MakePath関連の一時変数（関数に分けたら、そっちへ移動する）
	unsigned char numPath = 0;
	
	char mode_mazeSearcher=0;


	
	
	//壁情報・歩数マップ・座標の初期化
	initMap(&mapState);
	
	
	
//	myprintf("First\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
			
	wait(time_run);
/*	
	mapState.Wall[0][0] |= 0xFa;//1110
	mapState.Wall[0][1] |= 0xF3;//0011
	mapState.Wall[0][2] |= 0xF6;//0110
	mapState.Wall[0][3] |= 0xF3;//0011
	
	mapState.Wall[1][0] |= 0xF7;//0111
	mapState.Wall[1][1] |= 0xF4;//0100
	mapState.Wall[1][2] |= 0xF1;//0001
	mapState.Wall[1][3] |= 0xF5;//0101
	
	mapState.Wall[2][0] |= 0xF5;//0101
	mapState.Wall[2][1] |= 0xFd;//1101
	mapState.Wall[2][2] |= 0xFc;//1100
	mapState.Wall[2][3] |= 0xF1;//0001
	
	mapState.Wall[3][0] |= 0xFc;//1100
	mapState.Wall[3][1] |= 0xFa;//1010
	mapState.Wall[3][2] |= 0xFa;//1010
	mapState.Wall[3][3] |= 0xF9;//1001
*/
//	LED0=1;
	
	
	
	
	
	
	
	//第1走行(探索走行)
	
	//////////////////////////////////////////////////////////////////////////////////////////
	//走行開始(ループ抜け条件:1.走らないフラグON 2.ゴールに達した 3.緑スイッチが押された	//
	//////////////////////////////////////////////////////////////////////////////////////////
	
	while( !( Flag_Not_run==1 | goalCondition | (SW1!=0&SW2==0) ) )
	{
	//	LED1=1;
	if(Flag_debug_kagensoku==1){myprintf("//Acc90mm//");}
		
		//初めの一歩
		Acc90mm();//90mm加速し、区間の切れ目に達する
	//	LEDFlash(4,2,300);
		
		while(1)
		{//直進を続ける
				
			//区間の切れ目なので座標を更新する
			UpdateCoordinate(&mapState);//座標更新
			//myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
			
			//区間の切れ目なのでセンサ値取得する
			acquireSensorValue();
			
			//閾値を超えたらLED点灯
			LEDflashWall();
			
			//壁情報の更新
	//		UpdateWall			
			//////////////////////////////////////
			//区画の切れ目なので壁情報を更新する//
			//////////////////////////////////////
			//前に読み取ったデータを破棄する(1408_0912までのミス。変数の初期化ができてなかったため、積もっていた。
			tempWall = 0x00;
			
			tempWall |= 0xF0;														//1111 0000 現在区画の壁すべて既知にする
			//myprintf("KICHI:%3d\n\r",tempWall);
			tempWall |= (refSEN_R >= TH_Wall_R)								<<3;	//0000 ?000 東(3)の壁情報を取得する
			//myprintf("East :%3d\n\r",tempWall);
			tempWall |= 0													<<2;	//0000 0#00 南(2)を「壁なし」とする
			//myprintf("South:%3d\n\r",tempWall);
			tempWall |= (refSEN_L >= TH_Wall_L)								<<1;	//0000 00?0 西(1)の壁情報を取得する
			//myprintf("West :%3d\n\r",tempWall);
			tempWall |= ((refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR))	<<0;	//0000 000? 北(0)の壁情報を取得する
			//myprintf("North:%3d\n\n\r",tempWall);
			
					
			//取得した壁情報を姿勢mapState.Psiに応じて、回転させ(絶対座標系の)壁情報に保存する//
			//上位4bit(未知既知)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0xF0) << mapState.Psi ) | ( (tempWall & 0xF0) >> (4 - mapState.Psi) )  ) & 0xF0);
			//					  ビット和	3  2 1 上位4bit抽出  1　左にシフト	2	2 1	上位4bit抽出　1    1 右にシフト	 1 2  3	 上位4bit抽出
			//myprintf("UP4  :%3d\n\r",mapState.Wall[mapState.X][mapState.Y]);
			//下位4bit(壁の有無)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0x0F) << mapState.Psi ) | ( (tempWall & 0x0F) >> (4 - mapState.Psi) )  ) & 0x0F);
			//					  ビット和	3  2 1 下位4bit抽出  1　左にシフト	2	2 1	下位4bit抽出　1    1 右にシフト	 1 2  3	 下位4bit抽出
			//myprintf("DOWN4:%3d\n\n\n\n\r",mapState.Wall[mapState.X][mapState.Y]);
		//デバック用	
		//	mapState.Wall[mapState.X][mapState.Y] |= tempWall;
			
			
			//＜壁ウラ更新＞隣接する区間の壁を更新する//
			UpdateBehindWall(&mapState);
				
			//(現在の)壁情報から歩数マップを作成する
			GoalCategory = 1;//ゴール座標は「ゴール」
			MakeStepMap(&mapState);
	
	
			
			////////////////////////////////////////////
			///**************************************///
			///*	壁情報による行動選択(足立法)	*///
			///**************************************///
			////////////////////////////////////////////
	//	/*	
			//前、右、左の歩数マップを読み出す(回転角によって、異なる)
			
			//角度によって、足すベクトルを変える
			switch(mapState.Psi)
			{
				//一般に
				case 0://North北向き
					vectorFrontX =  0;
					vectorFrontY = +1;
					vectorRightX = +1;
					vectorRightY =  0;
					vectorLeftX	 = -1;
					vectorLeftY	 =  0;
				break;
		
				case 1://West西向き
					vectorFrontX = -1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = +1;
					vectorLeftX	 =  0;
					vectorLeftY	 = -1;
				break;
		
				case 2://South南向き
					vectorFrontX =  0;
					vectorFrontY = -1;
					vectorRightX = -1;
					vectorRightY =  0;
					vectorLeftX	 = +1;
					vectorLeftY	 =  0;
				break;
		
				case 3://East東向き
					vectorFrontX = +1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = -1;
					vectorLeftX	 =  0;
					vectorLeftY	 = +1;
				break;
			}
			
			frontStep 	= mapState.StepMap[mapState.X + vectorFrontX][mapState.Y + vectorFrontY];
			rightStep 	= mapState.StepMap[mapState.X + vectorRightX][mapState.Y + vectorRightY];
			leftStep 	= mapState.StepMap[mapState.X + vectorLeftX][mapState.Y + vectorLeftY];
			
			
			//隣の区間に対し、壁の有無と歩数の比較をすることによって、行動選択を行う
			if( (refSEN_R < TH_R) & ( rightStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】
			//	sla_RotRight(1);//さらに↓のbreakを取る
				break;
									
			}else if( (refSEN_FL < TH_FL | SEN_FR < TH_FR) & ( frontStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
			if(Flag_debug_kagensoku==1){myprintf("//Add180mm//");}
				Add180mm();//180mm追加で走る
				//LEDFlash(4,4,300);
							
			}else if( (refSEN_L < TH_L) & ( leftStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】
			//	sla_RotLeft(1);//さらにdownのbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
			
	//	*/
			////////////////////////////////////////////
			///**************************************///
			///*	壁情報による行動選択(左手法)	*///
			///**************************************///
			////////////////////////////////////////////
			
		/*
			if(refSEN_L < TH_L){//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】slaLeft();//さらにdownのbreakを取る
				break;
							
			}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
				Add180mm();//180mm追加で走る
							
			}else if(refSEN_R < TH_R){//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】slaRight();//さらに↓のbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
		//	*/
		
		
		}//whiel(1)直進を続ける
		
		//LED2=1;
	if(Flag_debug_kagensoku==1){myprintf("//Reduce90mm//");}
		Reduce90mm();
		//LEDFlash(10,2,300);
	//	Motor_stop();
	//	myprintf("AfterReduce\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
		wait(10);
	//	Motor_start();
		//LED1=1;
		if(Flag_motion_LHM==1)
		{
		//	LEDFlash(16,300,2);
			
		if(Flag_debug_kagensoku==1){myprintf("//RotLeft(1)//");}
	
			RotLeft(ROT90LRatio);	//左旋回
		//
		
		
	/*		Flag_control_on=0;//制御を切る
			run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90.0*0.7*1);
		//	Flag_control_on=1;//制御を入れる
			wait(300);
	*/		mapState.Psi += 1;
			if(mapState.Psi >= 4)
			{
				mapState.Psi -= 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);
		//	Motor_start();
		
		}
		else if(Flag_motion_LHM==3)
		{
		//	LEDFlash(1,300,2);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(1)//");}
		
			RotRight(ROT90Ratio);//右旋回
			//
	/*		Flag_control_on=0;//制御を切る
			run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,88.75*0.75*1);//r_ang);
		//	Flag_control_on=1;//制御を入れる
			wait(300);
	*/		mapState.Psi += -1;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);//200);
		//	Motor_start();
	
		}
		else if(Flag_motion_LHM==4)
		{
			
		//	LEDFlash(1,4,300);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(2)//");}
	
			//RotRight(ROT180Ratio);//Uターン
			RotRight(ROT180Ratio);//Uターン
			
			//
			mapState.Psi += -2;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);//200);
		//	Motor_start();
		
		}
		
				
		cntLHM++;
		
	}//while 走行開始
	
	
	
	
	
	
	
	
	
	
	
	
	LEDsimasima(2000);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//ゴールからスタートへ走る
	
	//////////////////////////////////////////////////////////////////////////////////////////
	//走行開始(ループ抜け条件:1.走らないフラグON 2.ゴールに達した 3.緑スイッチが押された	//
	//////////////////////////////////////////////////////////////////////////////////////////
	while( !( Flag_Not_run==1 | startCondition | (SW1!=0&SW2==0) ) )
	{
	//	LED1=1;
	if(Flag_debug_kagensoku==1){myprintf("//Acc90mm//");}
		
		//初めの一歩
		Acc90mm();//90mm加速し、区間の切れ目に達する
	//	LEDFlash(4,2,300);
		
		while(1)
		{//直進を続ける
				
			//区間の切れ目なので座標を更新する
			UpdateCoordinate(&mapState);//座標更新
			//myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
			
			//区間の切れ目なのでセンサ値取得する
			acquireSensorValue();
			//閾値を超えたらLED点灯
			if(refSEN_R  >= TH_Wall_R ){LED0=1;}else{LED0=0;}
			if( (refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR) ){LED2=1;}else{LED2=0;}
			if(refSEN_L  >= TH_Wall_L ){LED4=1;}else{LED4=0;}
		
						
			//////////////////////////////////////
			//区画の切れ目なので壁情報を更新する//
			//////////////////////////////////////
			//前に読み取ったデータを破棄する(1408_0912までのミス。変数の初期化ができてなかったため、積もっていた。
			tempWall = 0x00;
			
			tempWall |= 0xF0;														//1111 0000 現在区画の壁すべて既知にする
			//myprintf("KICHI:%3d\n\r",tempWall);
			tempWall |= (refSEN_R >= TH_Wall_R)								<<3;	//0000 ?000 東(3)の壁情報を取得する
			//myprintf("East :%3d\n\r",tempWall);
			tempWall |= 0													<<2;	//0000 0#00 南(2)を「壁なし」とする
			//myprintf("South:%3d\n\r",tempWall);
			tempWall |= (refSEN_L >= TH_Wall_L)								<<1;	//0000 00?0 西(1)の壁情報を取得する
			//myprintf("West :%3d\n\r",tempWall);
			tempWall |= ((refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR))	<<0;	//0000 000? 北(0)の壁情報を取得する
			//myprintf("North:%3d\n\n\r",tempWall);
			
					
			//取得した壁情報を姿勢mapState.Psiに応じて、回転させ(絶対座標系の)壁情報に保存する//
			//上位4bit(未知既知)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0xF0) << mapState.Psi ) | ( (tempWall & 0xF0) >> (4 - mapState.Psi) )  ) & 0xF0);
			//					  ビット和	3  2 1 上位4bit抽出  1　左にシフト	2	2 1	上位4bit抽出　1    1 右にシフト	 1 2  3	 上位4bit抽出
			//myprintf("UP4  :%3d\n\r",mapState.Wall[mapState.X][mapState.Y]);
			//下位4bit(壁の有無)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0x0F) << mapState.Psi ) | ( (tempWall & 0x0F) >> (4 - mapState.Psi) )  ) & 0x0F);
			//					  ビット和	3  2 1 下位4bit抽出  1　左にシフト	2	2 1	下位4bit抽出　1    1 右にシフト	 1 2  3	 下位4bit抽出
			//myprintf("DOWN4:%3d\n\n\n\n\r",mapState.Wall[mapState.X][mapState.Y]);
		//デバック用	
		//	mapState.Wall[mapState.X][mapState.Y] |= tempWall;
			
			
			//＜壁ウラ更新＞隣接する区間の壁を更新する//
			UpdateBehindWall(&mapState);
			
			//(現在の)壁情報から歩数マップを作成する
			GoalCategory = 0;//ゴール座標は「スタート」
			MakeStepMap(&mapState);
			
			////////////////////////////////////////////
			///**************************************///
			///*	壁情報による行動選択(足立法)	*///
			///**************************************///
			////////////////////////////////////////////
	//	/*	
			//前、右、左の歩数マップを読み出す(回転角によって、異なる)
			
			//角度によって、足すベクトルを変える
			switch(mapState.Psi)
			{
				//一般に
				case 0://North北向き
					vectorFrontX =  0;
					vectorFrontY = +1;
					vectorRightX = +1;
					vectorRightY =  0;
					vectorLeftX	 = -1;
					vectorLeftY	 =  0;
				break;
		
				case 1://West西向き
					vectorFrontX = -1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = +1;
					vectorLeftX	 =  0;
					vectorLeftY	 = -1;
				break;
		
				case 2://South南向き
					vectorFrontX =  0;
					vectorFrontY = -1;
					vectorRightX = -1;
					vectorRightY =  0;
					vectorLeftX	 = +1;
					vectorLeftY	 =  0;
				break;
		
				case 3://East東向き
					vectorFrontX = +1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = -1;
					vectorLeftX	 =  0;
					vectorLeftY	 = +1;
				break;
			}
			
			frontStep 	= mapState.StepMap[mapState.X + vectorFrontX][mapState.Y + vectorFrontY];
			rightStep 	= mapState.StepMap[mapState.X + vectorRightX][mapState.Y + vectorRightY];
			leftStep 	= mapState.StepMap[mapState.X + vectorLeftX][mapState.Y + vectorLeftY];
			
			
			//隣の区間に対し、壁の有無と歩数の比較をすることによって、行動選択を行う
			if( (refSEN_R < TH_R) & ( rightStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】
			//	sla_RotRight(1);//さらに↓のbreakを取る
				break;
									
			}else if( (refSEN_FL < TH_FL | SEN_FR < TH_FR) & ( frontStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
			if(Flag_debug_kagensoku==1){myprintf("//Add180mm//");}
				Add180mm();//180mm追加で走る
				//LEDFlash(4,4,300);
							
			}else if( (refSEN_L < TH_L) & ( leftStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】
			//	sla_RotLeft(1);//さらにdownのbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
			
	//	*/
			////////////////////////////////////////////
			///**************************************///
			///*	壁情報による行動選択(左手法)	*///
			///**************************************///
			////////////////////////////////////////////
			
		/*
			if(refSEN_L < TH_L){//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】slaLeft();//さらにdownのbreakを取る
				break;
							
			}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
				Add180mm();//180mm追加で走る
							
			}else if(refSEN_R < TH_R){//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】slaRight();//さらに↓のbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
		//	*/
		
		
		}//whiel(1)直進を続ける
		
		//LED2=1;
	if(Flag_debug_kagensoku==1){myprintf("//Reduce90mm//");}
		Reduce90mm();
		//LEDFlash(10,2,300);
	//	Motor_stop();
	//	myprintf("AfterReduce\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
		wait(10);//200);
	//	Motor_start();
		//LED1=1;
		if(Flag_motion_LHM==1)
		{
		//	LEDFlash(16,300,2);
			
		if(Flag_debug_kagensoku==1){myprintf("//RotLeft(1)//");}
	
			RotLeft(ROT90LRatio);	//左旋回
			
		//
		
		
	/*		Flag_control_on=0;//制御を切る
			run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90.0*0.7*1);
		//	Flag_control_on=1;//制御を入れる
			wait(300);
	*/		mapState.Psi += 1;
			if(mapState.Psi >= 4)
			{
				mapState.Psi -= 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);//200);
		//	Motor_start();
		
		}
		else if(Flag_motion_LHM==3)
		{
		//	LEDFlash(1,300,2);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(1)//");}
		
			RotRight(ROT90Ratio);//右旋回
			//
	/*		Flag_control_on=0;//制御を切る
			run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,88.75*0.75*1);//r_ang);
		//	Flag_control_on=1;//制御を入れる
			wait(300);
	*/		mapState.Psi += -1;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);//200);
		//	Motor_start();
	
		}
		else if(Flag_motion_LHM==4)
		{
			
		//	LEDFlash(1,4,300);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(2)//");}
	
			//RotRight(ROT180Ratio);//Uターン
			RotRight(ROT180Ratio);//Uターン
			
			//
			mapState.Psi += -2;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(10);//time_run);
			Motor_stop();
			wait(10);//200);
		//	Motor_start();
		
		}
		
				
		cntLHM++;
		
	}
	//↑スタートへ戻る	
	
	
	
	
	
	
	
	
	LEDsimasima(3000);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	//第2走行（最短歩数経路走行）
	
	//141018_1744　適当スラロームでいってみよー
	
	//141102_0512	最短経路しか通らないように、歩数マップ生成の条件に、既知区間ではない(Wall&0xf0!=240)ならば、進まない。もしくは、既知区間であれば、歩数を書く、とする
	
	//最短歩数経路導出
	MakeShortestStepMap(&mapState);
	
	//パス生成
	numPath = 0;//パスナンバーの初期化
	mapState.X=0;
	mapState.Y=0;
	
	//////////////////////////////////////////////////////////////////////////////////////////
	//走行開始(ループ抜け条件:1.走らないフラグON 2.ゴールに達した 3.緑スイッチが押された	//
	//////////////////////////////////////////////////////////////////////////////////////////
	while( !( Flag_Not_run==1 | goalCondition | (SW1!=0&SW2==0) ) )
	{
	//	LED1=1;
		if(Flag_debug_kagensoku==1){myprintf("//Acc90mm//");}
	
		
		//初めの一歩
	//	Acc90mm();//90mm加速し、区間の切れ目に達する
		Path1Pivot[numPath].Straight++;
	//	LEDFlash(4,2,300);
		
		while(1)
		{//直進を続ける
				
			//区間の切れ目なので座標を更新する
			UpdateCoordinate(&mapState);//座標更新
			//myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
			
			//区間の切れ目なのでセンサ値取得する
		/*	acquireSensorValue();
			
			//閾値を超えたらLED点灯
			if(refSEN_R  >= TH_Wall_R ){LED0=1;}else{LED0=0;}
			if( (refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR) ){LED2=1;}else{LED2=0;}
			if(refSEN_L  >= TH_Wall_L ){LED4=1;}else{LED4=0;}
		*/
			
			////////////////////////////////////////////////
			//保持している壁情報を進行方向に応じて回転する//
			////////////////////////////////////////////////
			//前に読み取ったデータを破棄する(1408_0912までのミス。変数の初期化ができてなかったため、積もっていた。
			tempWall = 0x00;		
			
			//保持している絶対座標系の壁情報を姿勢mapState.Psiに応じて、回転させ車両座標系の壁情報として保存する
			//上位4bit(未知既知)の回転
			tempWall = (
			(  ( (mapState.Wall[mapState.X][mapState.Y] & 0xF0) >> mapState.Psi ) | ( (mapState.Wall[mapState.X][mapState.Y] & 0xF0) << (4 - mapState.Psi) )  ) 
			& 0xF0
			);
			//				  ビット和	3  2 1 上位4bit抽出  1　左にシフト	2	2 1	上位4bit抽出　1    1 右にシフト	 1 2  3	 上位4bit抽出
			//myprintf("UP4  :%3d\n\r",mapState.Wall[mapState.X][mapState.Y]);
			//下位4bit(壁の有無)の回転
			tempWall = (
			(  ( (mapState.Wall[mapState.X][mapState.Y] & 0x0F) >> mapState.Psi ) | ( (mapState.Wall[mapState.X][mapState.Y] & 0x0F) << (4 - mapState.Psi) )  ) 
			& 0x0F
			);
			//mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0x0F) << mapState.Psi ) | ( (tempWall & 0x0F) >> (4 - mapState.Psi) )  ) & 0x0F);
			//					  ビット和	3  2 1 下位4bit抽出  1　左にシフト	2	2 1	下位4bit抽出　1    1 右にシフト	 1 2  3	 下位4bit抽出
			
			
			////////////////////////////////////////////
			//////
			///	壁情報によるキューへ格納する行動の選択(足立法)	///
			//////
			////////////////////////////////////////////
			
			//前、右、左の歩数マップを読み出す(回転角によって、異なる)
			
			//マウスの角度により、前右左の区間を見るために、足すベクトルを変える
			switch(mapState.Psi)
			{
				//一般に
				case 0://North北向き
					vectorFrontX =  0;
					vectorFrontY = +1;
					vectorRightX = +1;
					vectorRightY =  0;
					vectorLeftX	 = -1;
					vectorLeftY	 =  0;
				break;
		
				case 1://West西向き
					vectorFrontX = -1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = +1;
					vectorLeftX	 =  0;
					vectorLeftY	 = -1;
				break;
		
				case 2://South南向き
					vectorFrontX =  0;
					vectorFrontY = -1;
					vectorRightX = -1;
					vectorRightY =  0;
					vectorLeftX	 = +1;
					vectorLeftY	 =  0;
				break;
		
				case 3://East東向き
					vectorFrontX = +1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = -1;
					vectorLeftX	 =  0;
					vectorLeftY	 = +1;
				break;
			}
			
			//マウスから見て前隣の区間の歩数
			frontStep 	= mapState.StepMap[mapState.X + vectorFrontX][mapState.Y + vectorFrontY];
			frontWall	= mapState.Wall[mapState.X + vectorFrontX][mapState.Y + vectorFrontY];
			//マウスから見て右隣の区間の歩数
			rightStep 	= mapState.StepMap[mapState.X + vectorRightX][mapState.Y + vectorRightY];
			rightWall	= mapState.Wall[mapState.X + vectorRightX][mapState.Y + vectorRightY];
			//マウスから見て左隣の区間の歩数
			leftStep 	= mapState.StepMap[mapState.X + vectorLeftX ][mapState.Y + vectorLeftY ];
			leftWall	= mapState.Wall[mapState.X + vectorLeftX ][mapState.Y + vectorLeftY ];
			
			
			//隣の区間に対し、壁の有無と歩数の比較をすることによって、行動選択を行う
			if(  // ((frontWall&0xF0) == 240) & 
			((tempWall&0x01) == 0) & 
			( frontStep == mapState.StepMap[mapState.X][mapState.Y] - 1 ) )
			{//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
			if(Flag_debug_kagensoku==1){myprintf("//Add180mm//");}
			//	Add180mm();//180mm追加で走る
				Path1Pivot[numPath].Straight+=2;
				//LEDFlash(4,4,300);
							
			}else if(// ((rightWall&0xF0) == 240) & 
			((tempWall&0x08) == 0) & 
			( rightStep == mapState.StepMap[mapState.X][mapState.Y] - 1) )
			{//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】
			//	sla_RotRight(1);//さらに↓のbreakを取る
				break;
									
			}else if( //((leftWall&0xF0) == 240) & 
			((tempWall&0x02) == 0) & 
			( leftStep == mapState.StepMap[mapState.X][mapState.Y] - 1 ) )
			{//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】
			//	sla_RotLeft(1);//さらにdownのbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
		
		
		}//whiel(1)直進を続ける
		
		//LED2=1;
	if(Flag_debug_kagensoku==1){myprintf("//Reduce90mm//");}
	//	Reduce90mm();
		Path1Pivot[numPath].Straight++;
		
		//LEDFlash(10,2,300);
	//	Motor_stop();
	//	myprintf("AfterReduce\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
	//	wait(200);
	//	Motor_start();
		//LED1=1;
		if(Flag_motion_LHM==1)
		{
		//	LEDFlash(16,300,2);
			
			if(Flag_debug_kagensoku==1){myprintf("//RotLeft(1)//");}
	
		//	RotLeft(ROT90LRatio);	//左旋回
			Path1Pivot[numPath].Turn=2;
			numPath++;
		
			mapState.Psi += 1;
			if(mapState.Psi >= 4)
			{
				mapState.Psi -= 4;
			}
		}
		else if(Flag_motion_LHM==3)
		{
		//	LEDFlash(1,300,2);
			if(Flag_debug_kagensoku==1){myprintf("//RotRight(1)//");}
		
		//	RotRight(ROT90Ratio);//右旋回
			Path1Pivot[numPath].Turn=1;
			numPath++;
	
			mapState.Psi += -1;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}	
		}
		else if(Flag_motion_LHM==4)
		{
			
		//	LEDFlash(1,4,300);
			if(Flag_debug_kagensoku==1){myprintf("//RotRight(2)//");}
	
			//RotRight(ROT180Ratio);//Uターン
		//	RotRight(ROT180Ratio);//Uターン
			
//			最短導出終了
//			いや、ゴール達したら、whileループを抜けるはずだから、大丈夫
//			でも、これ、
		
			//
			mapState.Psi += -2;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
	
		}
		
				
		cntLHM++;
		
	}//while 走行開始
	numPath++;
	Path1Pivot[numPath].Turn = 16;
	//Path1Pivot（最短パス）生成終了
	
	//予定だと、ここから、Path2~5を変換する
	
	//その後、どれで走るのか、もしくは、終了するのかを選択する
	
	//超新地最短(Path1Pivot)
	//Path1Pivot
	numPath=0;
	while( !( Path1Pivot[numPath].Turn == 16 | (SW1!=0&SW2==0) ))
	{
		run(1,min_speed,800,min_speed,3000,3000,Path1Pivot[numPath].Straight*90);
		if(Path1Pivot[numPath].Turn==1)
		{
			wait(50);
			RotRight(1);
			wait(50);
			
		}
		else if(Path1Pivot[numPath].Turn==2)
		{
			wait(50);
			RotLeft(1);
			wait(50);
		}
		else if(Path1Pivot[numPath].Turn==0)
		{
			wait(50);
			RotRight(ROT180Ratio);
			wait(50);
		}
		else			
		{
			LEDsimasima(3000);
		}
		
		numPath++;
	}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
	KnightRider(3,1000);
	
	Motor_Disable();
	
	
	
	while(1)//以下無限ループ
	{
	
		mode_mazeSearcher = Switch_LED(mode_mazeSearcher);//mode_mainを書き換え、対応したLEDを光らせる。
		
		if(SW1!=0&SW2==0)
		{//SW1!=0&SW2==0){//緑スイッチのみON
			LEDFlash(mode_mazeSearcher, 100, 2);
			KnightRider(1,300);
		
			///////////////////////////////////////////////////////////////////////////////
			////////////////		最短（重ね含む）モード切り替え			///////////////////////////////
			///////////////////////////////////////////////////////////////////////////////
			
			switch(mode_mazeSearcher)
			{
				case 0:
					//重ね探索
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					mode_mazeSearcher=0;
					break;
				case 1:
					//Path1Pivot
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					numPath=0;
					while( Path1Pivot[numPath].Turn != 16 )
					{
						run(1,min_speed,900,min_speed,3000,3000,Path1Pivot[numPath].Straight*90);
						if(Path1Pivot[numPath].Turn==1)
						{
							wait(50);
							RotRight(1);
							wait(50);
			
						}
						else if(Path1Pivot[numPath].Turn==2)
						{
							wait(50);
							RotLeft(1);
							wait(50);
						}
						else if(Path1Pivot[numPath].Turn==0)
						{
							wait(50);
							RotRight(ROT180Ratio);
							wait(50);
						}
						else			
						{
							LEDsimasima(3000);
						}
		
						numPath++;
					}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
					KnightRider(3,1000);
					mode_mazeSearcher=0;

					break;
				
				case 2:
					//Path1Pivot
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					numPath=0;
					while( Path1Pivot[numPath].Turn != 16 )
					{
						run(1,min_speed,1000,min_speed,3000,3000,Path1Pivot[numPath].Straight*90);
						if(Path1Pivot[numPath].Turn==1)
						{
							wait(50);
							RotRight(1);
							wait(50);
			
						}
						else if(Path1Pivot[numPath].Turn==2)
						{
							wait(50);
							RotLeft(1);
							wait(50);
						}
						else if(Path1Pivot[numPath].Turn==0)
						{
							wait(50);
							RotRight(ROT180Ratio);
							wait(50);
						}
						else			
						{
							LEDsimasima(3000);
						}
		
						numPath++;
					}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
					KnightRider(3,1000);
					mode_mazeSearcher=0;

					//Path2PivotAskew
					mode_mazeSearcher=0;

					break;
				
				case 3:
					
					//Path1Pivot
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					numPath=0;
					while( Path1Pivot[numPath].Turn != 16 )
					{
						run(1,min_speed,1200,min_speed,3000,3000,Path1Pivot[numPath].Straight*90);
						if(Path1Pivot[numPath].Turn==1)
						{
							wait(50);
							RotRight(1);
							wait(50);
			
						}
						else if(Path1Pivot[numPath].Turn==2)
						{
							wait(50);
							RotLeft(1);
							wait(50);
						}
						else if(Path1Pivot[numPath].Turn==0)
						{
							wait(50);
							RotRight(ROT180Ratio);
							wait(50);
						}
						else			
						{
							LEDsimasima(3000);
						}
		
						numPath++;
					}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
					KnightRider(3,1000);
					mode_mazeSearcher=0;

					
				
					//Path3LittleTurn
				//	mode_mazeSearcher=0;

					break;
				
				case 4:
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					//Path1Pivot
					numPath=0;
					while( Path1Pivot[numPath].Turn != 16 )
					{
						run(1,min_speed,1400,min_speed,2500,2500,Path1Pivot[numPath].Straight*90);
						if(Path1Pivot[numPath].Turn==1)
						{
							wait(50);
							RotRight(1);
							wait(50);
			
						}
						else if(Path1Pivot[numPath].Turn==2)
						{
							wait(50);
							RotLeft(1);
							wait(50);
						}
						else if(Path1Pivot[numPath].Turn==0)
						{
							wait(50);
							RotRight(ROT180Ratio);
							wait(50);
						}
						else			
						{
							LEDsimasima(3000);
						}
		
						numPath++;
					}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
					KnightRider(3,1000);
					mode_mazeSearcher=0;

					
					//Path4BigTurn
				//	]::BZ_FREQ_SOb9
				//	mode_mazeSearcher=0;

					break;
					
				case 5:
					
				
					//Path1Pivot
					
					Motor_Enable();	//MotorEnableをHiに。
						
					while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
					{
					
					}
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					
					
					numPath=0;
					while( Path1Pivot[numPath].Turn != 16 )
					{
						run(1,min_speed,1500,min_speed,2500,2500,Path1Pivot[numPath].Straight*90);
						if(Path1Pivot[numPath].Turn==1)
						{
							wait(50);
							RotRight(1);
							wait(50);
			
						}
						else if(Path1Pivot[numPath].Turn==2)
						{
							wait(50);
							RotLeft(1);
							wait(50);
						}
						else if(Path1Pivot[numPath].Turn==0)
						{
							wait(50);
							RotRight(ROT180Ratio);
							wait(50);
						}
						else			
						{
							LEDsimasima(3000);
						}
		
						numPath++;
					}//while　Path1Pivot[numPath]がゼロ(初期値)になるまで
		
	
					KnightRider(3,1000);
					mode_mazeSearcher=0;

				
					//Path5Askew
				//	mode_mazeSearcher=0;

					break;
					
				case 6:
					KnightRider(5,1000);
					mode_mazeSearcher=0;

					goto ENDLOOP_lefthandNonstop;//while
					
				default:
					LEDFlash(mode_mazeSearcher,100,5);
					LEDsimasima(1000);
					mode_mazeSearcher = 0;
					
					break;
			}//switch(mode_mazeSearcher)
		}//if//緑スイッチのみON
		
		
	}	
	
	ENDLOOP_lefthandNonstop:
	
	
	
	//第2走行　ここまで
	
	
	
	
	
	
	
	
	
	LEDsimasima(2600);
	wait(400);
	
	
	
	
	
	
	//第2走行スタートへもどる
	
/*		
	//ゴールからスタートへ走る
	//適当スラロームslaRight(1),Left(1)で
	
	//////////////////////////////////////////////////////////////////////////////////////////
	//走行開始(ループ抜け条件:1.走らないフラグON 2.ゴールに達した 3.緑スイッチが押された	//
	//////////////////////////////////////////////////////////////////////////////////////////
	while( !( Flag_Not_run==1 | startCondition | (SW1!=0&SW2==0) ) )
	{
	//	LED1=1;
	if(Flag_debug_kagensoku==1){myprintf("//Acc90mm//");}
		
		//初めの一歩
		Acc90mm();//90mm加速し、区間の切れ目に達する
	//	LEDFlash(4,2,300);
		
		while(1)
		{//直進を続ける
				
			//区間の切れ目なので座標を更新する
			UpdateCoordinate(&mapState);//座標更新
			//myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
			
			//区間の切れ目なのでセンサ値取得する
			acquireSensorValue();
			//閾値を超えたらLED点灯
			if(refSEN_R  >= TH_Wall_R ){LED0=1;}else{LED0=0;}
			if( (refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR) ){LED2=1;}else{LED2=0;}
			if(refSEN_L  >= TH_Wall_L ){LED4=1;}else{LED4=0;}
		
						
		///*	//////////////////////////////////////
			//区画の切れ目なので壁情報を更新する//
			//////////////////////////////////////
			//前に読み取ったデータを破棄する(1408_0912までのミス。変数の初期化ができてなかったため、積もっていた。
			tempWall = 0x00;
			
			tempWall |= 0xF0;														//1111 0000 現在区画の壁すべて既知にする
			//myprintf("KICHI:%3d\n\r",tempWall);
			tempWall |= (refSEN_R >= TH_Wall_R)								<<3;	//0000 ?000 東(3)の壁情報を取得する
			//myprintf("East :%3d\n\r",tempWall);
			tempWall |= 0													<<2;	//0000 0#00 南(2)を「壁なし」とする
			//myprintf("South:%3d\n\r",tempWall);
			tempWall |= (refSEN_L >= TH_Wall_L)								<<1;	//0000 00?0 西(1)の壁情報を取得する
			//myprintf("West :%3d\n\r",tempWall);
			tempWall |= ((refSEN_FL >= TH_Wall_FL) & (refSEN_FR >= TH_Wall_FR))	<<0;	//0000 000? 北(0)の壁情報を取得する
			//myprintf("North:%3d\n\n\r",tempWall);
			
					
			//取得した壁情報を姿勢mapState.Psiに応じて、回転させ(絶対座標系の)壁情報に保存する//
			//上位4bit(未知既知)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0xF0) << mapState.Psi ) | ( (tempWall & 0xF0) >> (4 - mapState.Psi) )  ) & 0xF0);
			//					  ビット和	3  2 1 上位4bit抽出  1　左にシフト	2	2 1	上位4bit抽出　1    1 右にシフト	 1 2  3	 上位4bit抽出
			//myprintf("UP4  :%3d\n\r",mapState.Wall[mapState.X][mapState.Y]);
			//下位4bit(壁の有無)の回転
			mapState.Wall[mapState.X][mapState.Y] |=((  ( (tempWall & 0x0F) << mapState.Psi ) | ( (tempWall & 0x0F) >> (4 - mapState.Psi) )  ) & 0x0F);
			//					  ビット和	3  2 1 下位4bit抽出  1　左にシフト	2	2 1	下位4bit抽出　1    1 右にシフト	 1 2  3	 下位4bit抽出
			//myprintf("DOWN4:%3d\n\n\n\n\r",mapState.Wall[mapState.X][mapState.Y]);
		//デバック用	
		//	mapState.Wall[mapState.X][mapState.Y] |= tempWall;
			
			
			//＜壁ウラ更新＞隣接する区間の壁を更新する//
			UpdateBehindWall(&mapState);
			
			////////////////////////////////////////////////////
			///	現状の壁情報から歩数マップを作成する	///
			////////////////////////////////////////////////////
	//		
		//	myprintf("stepmap_init\n\r");
	
			//(一区間ごとの)歩数マップの初期化及び歩数マップ生成のための変数の初期化/
			//すべての区間の歩数を255(歩数未導出)とする
			for(m=0;m<16;m++)
			{
				for(n=0;n<16;n++)
				{
					mapState.StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
				}
			}
	
			//ゴールのサイズ(1マスか4マスかのどちらか)による初期化及びゴール座標を0に初期化
			if(GOAL_NUM == 1)
			{
				mapState.StepMap[GOAL_X][GOAL_Y] = 0;	//ゴール座標の初期化
		
				nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
				nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
				nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
				nextCoordinate[nextStepChaser] = (GOAL_X << 4) + (GOAL_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
				//ループの最初に、周囲の歩数を求める座標X,Y
				searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
				searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
	
			}
			else if(GOAL_NUM == 4)
			{
				//全日本などの16×16区画の迷路のゴール
				mapState.StepMap[GOAL_XL][GOAL_YL] = 0;
				mapState.StepMap[GOAL_XL][GOAL_YH] = 0;
				mapState.StepMap[GOAL_XH][GOAL_YL] = 0;
				mapState.StepMap[GOAL_XH][GOAL_YH] = 0;
		
				nextStepMarker = 3;//4;	//歩数が確定した座標の総数(≡印※イメージです)
				nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
				nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
				nextCoordinate[0] = (GOAL_XL << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
				nextCoordinate[1] = (GOAL_XL << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
				nextCoordinate[2] = (GOAL_XH << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
				nextCoordinate[3] = (GOAL_XH << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		
				//ループの最初に、周囲の歩数を求める座標X,Y
				searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
				searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
	
			}
			else
			{
				LEDsimasima(2000);
				//Switch_LED
			}
			
			//帰りの歩数
			mapState.StepMap[0][0] = 0;	//スタート座標をゴール座標に初期化
	
			nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
			nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
			nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
			nextCoordinate[nextStepChaser] = (START_X << 4) + (START_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
			//ループの最初に、周囲の歩数を求める座標X,Y
			searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
			searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
			
			
	
		//	myprintf("stepmap_init2\n\r");
					
	
		//	myprintf("stepmap_will_be_making\n\n\r");
	
			if(Flag_debug_stepmap == 1)
			{
				myprintf("1st initial value check\n\r");
				//初めに、歩数マップ関係の変数の初期値を見ておく
				myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
				myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
				myprintf("nextStepValue =%4d\n\r",nextStepValue);
				myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
			}
	
		//	LED4=1;
			//歩数マップを作成する
			while( (nextStepMarker>=nextStepChaser) & !(nextStepChaser==255)  )//歩数マップが完成する(≡印が印ヲ追ウモノに越される※イメージです)までループする
			{
			//	myprintf("in_while(nextStepMarker<=nextStepChaser)\n\n\r");
		
				//今ループで探索する座標周辺の歩数を確認する
		
				//北の座標の歩数値更新
			//	myprintf("N_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0 && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0x10 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x10)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF )
				//北の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので北端(Y=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX][searchStepMapY+1] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = (searchStepMapX<<4) + (searchStepMapY+1);
					if(Flag_debug_stepmap == 1)
					{	
						myprintf("North\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X,Y+1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY+1);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//西の座標の歩数値更新
			//	myprintf("W_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0 && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0x20 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x20)==0x00 ) && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF )
				//西の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので西端(X=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX-1][searchStepMapY] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX-1)<<4 ) + searchStepMapY;
			
					if(Flag_debug_stepmap == 1)
					{
						myprintf("West(NISHI)\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X-1,Y = (%3d,%3d)\n\r",searchStepMapX-1,searchStepMapY);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//南の座標の歩数値更新
			//	myprintf("S_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0 && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF);
				if( (  (mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0x40 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x40)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF )
				//南の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので南端(Y=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX][searchStepMapY-1] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX)<<4 ) + (searchStepMapY - 1);
			
					if(Flag_debug_stepmap == 1)
					{
				
						myprintf("South\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X,Y-1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY-1);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//東の座標の歩数値更新
			//	myprintf("E_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0 && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0x80 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x80)==0x00 ) && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF )
				//東の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので東端(X=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX+1][searchStepMapY] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX + 1)<<4)  + (searchStepMapY);
			
					if(Flag_debug_stepmap == 1)
					{
						myprintf("East(HIGASHI)\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X+1,Y = (%3d,%3d)\n\r",searchStepMapX+1,searchStepMapY);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//次へ進む
				nextStepChaser++; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
				//今ループで周囲の歩数を求める座標X,Y
				searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0 )>>4;
				searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
				//次に探索する座標の歩数+1が、周囲の区間に入れるべき値
				nextStepValue = mapState.StepMap[searchStepMapX][searchStepMapY] + 1 ;
		
				if(Flag_debug_stepmap == 1)
				{
			
					//デバック
					myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
					myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
					myprintf("nextStepValue =%4d\n\r",nextStepValue);
					myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
				}
				
	
			}//while
	//		
			
			////////////////////////////////////////////
			///	壁情報による行動選択(足立法)	///
			////////////////////////////////////////////
	//	/	
			//前、右、左の歩数マップを読み出す(回転角によって、異なる)
			
			//角度によって、足すベクトルを変える
			switch(mapState.Psi)
			{
				//一般に
				case 0://North北向き
					vectorFrontX =  0;
					vectorFrontY = +1;
					vectorRightX = +1;
					vectorRightY =  0;
					vectorLeftX	 = -1;
					vectorLeftY	 =  0;
				break;
		
				case 1://West西向き
					vectorFrontX = -1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = +1;
					vectorLeftX	 =  0;
					vectorLeftY	 = -1;
				break;
		
				case 2://South南向き
					vectorFrontX =  0;
					vectorFrontY = -1;
					vectorRightX = -1;
					vectorRightY =  0;
					vectorLeftX	 = +1;
					vectorLeftY	 =  0;
				break;
		
				case 3://East東向き
					vectorFrontX = +1;
					vectorFrontY =  0;
					vectorRightX =  0;
					vectorRightY = -1;
					vectorLeftX	 =  0;
					vectorLeftY	 = +1;
				break;
			}
			
			frontStep 	= mapState.StepMap[mapState.X + vectorFrontX][mapState.Y + vectorFrontY];
			rightStep 	= mapState.StepMap[mapState.X + vectorRightX][mapState.Y + vectorRightY];
			leftStep 	= mapState.StepMap[mapState.X + vectorLeftX][mapState.Y + vectorLeftY];
			
			
			//隣の区間に対し、壁の有無と歩数の比較をすることによって、行動選択を行う
			if( (refSEN_R < TH_R) & ( rightStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//右の壁がない→90mm減速、右に90度ターン、90mm前進
				Flag_motion_LHM = 3;
				//【予定】
			//	sla_RotRight(1);//さらに↓のbreakを取る
				break;
									
			}else if( (refSEN_FL < TH_FL | SEN_FR < TH_FR) & ( frontStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
			if(Flag_debug_kagensoku==1){myprintf("//Add180mm//");}
				Add180mm();//180mm追加で走る
				//LEDFlash(4,4,300);
							
			}else if( (refSEN_L < TH_L) & ( leftStep < mapState.StepMap[mapState.X][mapState.Y] ) )
			{//左の壁がない→90mm減速、左に90度ターン、90mm前進
				Flag_motion_LHM = 1;
				//【予定】
			//	sla_RotLeft(1);//さらにdownのbreakを取る
				break;
						
			}else{//その他（壁に囲まれた→90mm減速、(右に)180度ターン、90mm前進
				Flag_motion_LHM = 4;
				break;
				
			}
			

	
		
		
		}//whiel(1)直進を続ける
		
		//LED2=1;
	if(Flag_debug_kagensoku==1){myprintf("//Reduce90mm//");}
		Reduce90mm();
		//LEDFlash(10,2,300);
	//	Motor_stop();
	//	myprintf("AfterReduce\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
		wait(200);
	//	Motor_start();
		//LED1=1;
		if(Flag_motion_LHM==1)
		{
		//	LEDFlash(16,300,2);
			
		if(Flag_debug_kagensoku==1){myprintf("//RotLeft(1)//");}
	
			RotLeft(ROT90LRatio);	//左旋回
		//
		
		
	//		Flag_control_on=0;//制御を切る
	//		run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90.0*0.7*1);
		//	Flag_control_on=1;//制御を入れる
//			wait(300);
			mapState.Psi += 1;
			if(mapState.Psi >= 4)
			{
				mapState.Psi -= 4;
			}
			wait(time_run);
			Motor_stop();
			wait(200);
		//	Motor_start();
		
		}
		else if(Flag_motion_LHM==3)
		{
		//	LEDFlash(1,300,2);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(1)//");}
		
			RotRight(ROT90Ratio);//右旋回
			//
	//		Flag_control_on=0;//制御を切る
	//		run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,88.75*0.75*1);//r_ang);
		//	Flag_control_on=1;//制御を入れる
//			wait(300);
			mapState.Psi += -1;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(time_run);
			Motor_stop();
			wait(200);
		//	Motor_start();
	
		}
		else if(Flag_motion_LHM==4)
		{
			
		//	LEDFlash(1,4,300);
		if(Flag_debug_kagensoku==1){myprintf("//RotRight(2)//");}
	
			//RotRight(ROT180Ratio);//Uターン
			RotLeft(ROT180Ratio);//Uターン
			
			//
			mapState.Psi += -2;
			if(mapState.Psi < 0)
			{
				mapState.Psi += 4;
			}
			wait(time_run);
			Motor_stop();
			wait(200);
		//	Motor_start();
		
		}
		
				
		cntLHM++;
		
	};
	//↑スタートへ戻る	
	
	
	
	
	//再度戻る　ここまで
	

*/	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	KnightRider(3,1000);
	
	Motor_Disable();
	
	
	
	
//	myprintf("TEST after KnightRider\n\r");
	
	
	/*壁情報を手打ち入力する*/
	
/*	mapState.Wall[0][0] |= 0xFE;//1110
	mapState.Wall[0][1] |= 0xF3;//0011
	mapState.Wall[0][2] |= 0xFE;//1110
	mapState.Wall[0][3] |= 0xF3;//0011
	
	mapState.Wall[1][0] |= 0xF7;//0111
	mapState.Wall[1][1] |= 0xF4;//0100
	mapState.Wall[1][2] |= 0xF2;//0010
	mapState.Wall[1][3] |= 0xF9;//1001
	
	mapState.Wall[2][0] |= 0xF4;//0100
	mapState.Wall[2][1] |= 0xF9;//1001
	mapState.Wall[2][2] |= 0xF4;//0100
	mapState.Wall[2][3] |= 0xFb;//1011
	
	mapState.Wall[3][0] |= 0xFc;//1100
	mapState.Wall[3][1] |= 0xFa;//1010
	mapState.Wall[3][2] |= 0xF0;//1000
	mapState.Wall[3][3] |= 0xFb;//1011
*/	
//	LED2=1;
	
//	myprintf("Wallinfo\n\r");
	
/*	mapState.Wall[0][0] = 0xFa;//1110
	mapState.Wall[0][1] = 0xF3;//0011
	mapState.Wall[0][2] = 0xF6;//0110
	mapState.Wall[0][3] = 0xF3;//0011
	
	mapState.Wall[1][0] = 0xF7;//0111
	mapState.Wall[1][1] = 0xF4;//0100
	mapState.Wall[1][2] = 0xF1;//0001
	mapState.Wall[1][3] = 0xF5;//0101
	
	mapState.Wall[2][0] = 0xF5;//0101
	mapState.Wall[2][1] = 0xFd;//1101
	mapState.Wall[2][2] = 0xFc;//1100
	mapState.Wall[2][3] = 0xF1;//0001
	
	mapState.Wall[3][0] = 0xFc;//1100
	mapState.Wall[3][1] = 0xFa;//1010
	mapState.Wall[3][2] = 0xFa;//1010
	mapState.Wall[3][3] = 0xF9;//1001
*/
	//141008昼の迷路サンプル(エクセルで作った迷路)と同じ。つまり、手でやったシミュレートと同じことが起こるはずだ。
/*	mapState.Wall[0][0] = 0xFe;//1110
	mapState.Wall[0][1] = 0xFa;//1010
	mapState.Wall[0][2] = 0xFa;//1010
	mapState.Wall[0][3] = 0xF3;//0011
	mapState.Wall[0][4] = 0xF6;//0110
	
	mapState.Wall[1][0] = 0xF6;//0110
	mapState.Wall[1][1] = 0xFa;//1010
	mapState.Wall[1][2] = 0xF3;//0011
	mapState.Wall[1][3] = 0xF5;//0101
	mapState.Wall[1][4] = 0xF4;//0100
	
	mapState.Wall[2][0] = 0xF4;//0100
	mapState.Wall[2][1] = 0xFb;//1011
	mapState.Wall[2][2] = 0xFd;//1101
	mapState.Wall[2][3] = 0xF5;//0101
	mapState.Wall[2][4] = 0xF4;//0100
	
	mapState.Wall[3][0] = 0xFc;//1100
	mapState.Wall[3][1] = 0xFa;//1010
	mapState.Wall[3][2] = 0xFa;//1010
	mapState.Wall[3][3] = 0xF1;//0001
	mapState.Wall[3][4] = 0xF4;//0100
	
	mapState.Wall[4][0] = 0xF6;//0110
	mapState.Wall[4][1] = 0xF2;//0010
	mapState.Wall[4][2] = 0xF2;//0010
	mapState.Wall[4][3] = 0xF0;//0000
	mapState.Wall[4][4] = 0xF0;//0000
*/	

	//141009_1413 左下が更新できないバグ
/*	mapState.Wall[0][0] = 0xFe;//1110
	mapState.Wall[0][1] = 0xFa;//1010
	mapState.Wall[0][2] = 0xFa;//1010
	mapState.Wall[0][3] = 0xF3;//0011
	mapState.Wall[0][4] = 0xF6;//0110
	
	mapState.Wall[1][0] = 0xF6;//0110
	mapState.Wall[1][1] = 0xFa;//1010
//	*/
	
	
	////////////////////////////////////////////////////
	///**********************************************///
	///*	現状の壁情報から歩数マップを作成する	*///
	///**********************************************///
	////////////////////////////////////////////////////
	
	myprintf("stepmap_init\n\r");

/*	
	//(一区間ごとの)歩数マップの初期化及び歩数マップ生成のための変数の初期化//
	//すべての区間の歩数を255(歩数未導出)とする
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState.StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
		}
	}
	
	//ゴールのサイズ(1マスか4マスかのどちらか)による初期化及びゴール座標を0に初期化
	if(GOAL_NUM == 1)
	{
		mapState.StepMap[GOAL_X][GOAL_Y] = 0;	//ゴール座標の初期化
		
		nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[nextStepChaser] = (GOAL_X << 4) + (GOAL_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
	
	}
	if(GOAL_NUM == 4)
	{
		//全日本などの16×16区画の迷路のゴール
		mapState.StepMap[GOAL_XL][GOAL_YL] = 0;
		mapState.StepMap[GOAL_XL][GOAL_YH] = 0;
		mapState.StepMap[GOAL_XH][GOAL_YL] = 0;
		mapState.StepMap[GOAL_XH][GOAL_YH] = 0;
		
		nextStepMarker = 3;//4;	//歩数が確定した座標の総数(≡印※イメージです)
		nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
		nextCoordinate[0] = (GOAL_XL << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[1] = (GOAL_XL << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[2] = (GOAL_XH << 4) + (GOAL_YL);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		nextCoordinate[3] = (GOAL_XH << 4) + (GOAL_YH);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
		
		//ループの最初に、周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
	
	}
	else
	{
		LEDsimasima(2000);
		//Switch_LED
	}
	
//	myprintf("stepmap_init2\n\r");
					
	
//	myprintf("stepmap_will_be_making\n\n\r");
	
	
	if(Flag_debug_stepmap == 1)
	{
		myprintf("1st initial value check"\n\r);
		//初めに、歩数マップ関係の変数の初期値を見ておく
		myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
		myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
		myprintf("nextStepValue =%4d\n\r",nextStepValue);
		myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
	}
	
	
//	LED4=1;
	//歩数マップを作成する
	while( (nextStepMarker>=nextStepChaser) & !(nextStepChaser==255)  )//歩数マップが完成する(≡印が印ヲ追ウモノに越される※イメージです)までループする
	{
	//	myprintf("in_while(nextStepMarker<=nextStepChaser)\n\n\r");
		
		//今ループで探索する座標周辺の歩数を確認する
		
		//北の座標の歩数値更新
	//	myprintf("N_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0 && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF);
		if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0x10 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x10)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF )
		//北の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので北端(Y=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState.StepMap[searchStepMapX][searchStepMapY+1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = (searchStepMapX<<4) + (searchStepMapY+1);
			if(Flag_debug_stepmap == 1)
			{	
				myprintf("North\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y+1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY+1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}
		
		//西の座標の歩数値更新
	//	myprintf("W_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0 && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF);
		if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0x20 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x20)==0x00 ) && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF )
		//西の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので西端(X=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState.StepMap[searchStepMapX-1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX-1)<<4 ) + searchStepMapY;
			
			if(Flag_debug_stepmap == 1)
			{
				myprintf("West(NISHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X-1,Y = (%3d,%3d)\n\r",searchStepMapX-1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}
		
		//南の座標の歩数値更新
	//	myprintf("S_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0 && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF);
		if( (  (mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0x40 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x40)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF )
		//南の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので南端(Y=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState.StepMap[searchStepMapX][searchStepMapY-1] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX)<<4 ) + (searchStepMapY - 1);
			
			if(Flag_debug_stepmap == 1)
			{
				
				myprintf("South\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X,Y-1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY-1);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}
		
		//東の座標の歩数値更新
	//	myprintf("E_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0 && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF);
		if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0x80 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x80)==0x00 ) && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF )
		//東の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので東端(X=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
		{
			mapState.StepMap[searchStepMapX+1][searchStepMapY] = nextStepValue;	//
			//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
			nextStepMarker++;	//歩数が確定した座標のカウントを増やす
			nextCoordinate[nextStepMarker] = ( (searchStepMapX + 1)<<4)  + (searchStepMapY);
			
			if(Flag_debug_stepmap == 1)
			{
				myprintf("East(HIGASHI)\n\r");
				myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
				myprintf("	searchStepMap X+1,Y = (%3d,%3d)\n\r",searchStepMapX+1,searchStepMapY);
				myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
			}
		}
		
		//次へ進む
		nextStepChaser++; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
		//今ループで周囲の歩数を求める座標X,Y
		searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0 )>>4;
		searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
		//次に探索する座標の歩数+1が、周囲の区間に入れるべき値
		nextStepValue = mapState.StepMap[searchStepMapX][searchStepMapY] + 1 ;
		
		if(Flag_debug_stepmap == 1)
		{
			
			//デバック
			myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
			myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
			myprintf("nextStepValue =%4d\n\r",nextStepValue);
			myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
		}
		
	
	}//while
	
//	myprintf("Finish_stepmap_while(nextStepMarker<=nextStepChaser)_ \n\n\n\r");
	
	if(Flag_debug_stepmap == 1)
	{
		
		for(n=0;n<255;n++)
		{
			myprintf("nextCoordinate[%3d] = %3d = (%2d,%2d)\n\r",n,nextCoordinate[n],(nextCoordinate[n]&0xf0)>>4,nextCoordinate[n]&0x0f);
		}
		myprintf("\n\n\r");
	}
	
*/

	
	/****************/
	/*探索結果の表示*/
	/****************/
	//最終座標と姿勢の表示
	while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
	//	myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
		//なにも書かなければ、何もしないで待つことになる
	}
	wait(200);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//スタートへ帰るときの歩数作成デバッグ
//	Flag_debug_stepmap = 1;
	////////////////////////////////////////////////////
			///**********************************************///
			///*	現状の壁情報から歩数マップを作成する	*///
			///**********************************************///
			////////////////////////////////////////////////////
/*	//		
		//	myprintf("stepmap_init\n\r");
	
			//(一区間ごとの)歩数マップの初期化及び歩数マップ生成のための変数の初期化/
			//すべての区間の歩数を255(歩数未導出)とする
			for(m=0;m<16;m++)
			{
				for(n=0;n<16;n++)
				{
					mapState.StepMap[m][n] = 0xFF;	//0000 0000 //255に初期化
				}
			}
	
	
			//帰りの歩数
			mapState.StepMap[0][0] = 0;	//スタート座標をゴール座標に初期化
	
			nextStepMarker = 0;	//歩数が確定した座標の総数(≡印※イメージです)
			nextStepChaser = 0; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
			nextStepValue = 1;	//次のwhileループで書き込む座標の歩数値
			nextCoordinate[nextStepChaser] = (START_X << 4) + (START_Y);	//0xXY←上位4ビットをX座標、下位4ビットをY座標とする
			//ループの最初に、周囲の歩数を求める座標X,Y
			searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0) >> 4;
			searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
			
			
	
		//	myprintf("stepmap_init2\n\r");
					
	
		//	myprintf("stepmap_will_be_making\n\n\r");
	
			if(Flag_debug_stepmap == 1)
			{
				myprintf("1st initial value check\n\r");
				//初めに、歩数マップ関係の変数の初期値を見ておく
				myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
				myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
				myprintf("nextStepValue =%4d\n\r",nextStepValue);
				myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
			}
	
		//	LED4=1;
			//歩数マップを作成する
			while( (nextStepMarker>=nextStepChaser) & !(nextStepChaser==255)  )//歩数マップが完成する(≡印が印ヲ追ウモノに越される※イメージです)までループする
			{
			//	myprintf("in_while(nextStepMarker<=nextStepChaser)\n\n\r");
		
				//今ループで探索する座標周辺の歩数を確認する
		
				//北の座標の歩数値更新
			//	myprintf("N_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0 && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x11)==0x10 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x10)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY+1]==0xFF )
				//北の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので北端(Y=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX][searchStepMapY+1] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = (searchStepMapX<<4) + (searchStepMapY+1);
					if(Flag_debug_stepmap == 1)
					{	
						myprintf("North\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X,Y+1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY+1);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//西の座標の歩数値更新
			//	myprintf("W_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0 && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x22)==0x20 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x20)==0x00 ) && mapState.StepMap[searchStepMapX-1][searchStepMapY]==0xFF )
				//西の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので西端(X=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX-1][searchStepMapY] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX-1)<<4 ) + searchStepMapY;
			
					if(Flag_debug_stepmap == 1)
					{
						myprintf("West(NISHI)\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X-1,Y = (%3d,%3d)\n\r",searchStepMapX-1,searchStepMapY);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//南の座標の歩数値更新
			//	myprintf("S_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0 && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF);
				if( (  (mapState.Wall[searchStepMapX][searchStepMapY]&0x44)==0x40 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x40)==0x00 ) && mapState.StepMap[searchStepMapX][searchStepMapY-1]==0xFF )
				//南の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので南端(Y=0)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX][searchStepMapY-1] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX)<<4 ) + (searchStepMapY - 1);
			
					if(Flag_debug_stepmap == 1)
					{
				
						myprintf("South\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X,Y-1 = (%3d,%3d)\n\r",searchStepMapX,searchStepMapY-1);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//東の座標の歩数値更新
			//	myprintf("E_estimate=%d\n\n\r",(mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0 && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF);
				if( ( (mapState.Wall[searchStepMapX][searchStepMapY]&0x88)==0x80 | (mapState.Wall[searchStepMapX][searchStepMapY]&0x80)==0x00 ) && mapState.StepMap[searchStepMapX+1][searchStepMapY]==0xFF )
				//東の壁が「壁なし」または「未知」、かつ、歩数が初期値(255)ならば… &&なので東端(X=15)でも、第2オペランド(&&の右)を評価しないため、エラーにならない
				{
					mapState.StepMap[searchStepMapX+1][searchStepMapY] = nextStepValue;	//
					//補足：壁情報の初期設定で周囲の壁を作っていれば0~15の範囲外を指定することはない
					nextStepMarker++;	//歩数が確定した座標のカウントを増やす
					nextCoordinate[nextStepMarker] = ( (searchStepMapX + 1)<<4)  + (searchStepMapY);
			
					if(Flag_debug_stepmap == 1)
					{
						myprintf("East(HIGASHI)\n\r");
						myprintf("	nextStepMarker = %3d\n\r",nextStepMarker);
						myprintf("	searchStepMap X+1,Y = (%3d,%3d)\n\r",searchStepMapX+1,searchStepMapY);
						myprintf("	nextCoordinate = (%3d,%3d)\n\n\r",nextCoordinate[nextStepMarker]&0xf0>>4,nextCoordinate[nextStepMarker]&0x0f);
					}
				}
		
				//次へ進む
				nextStepChaser++; //次のwhileループで周囲4マスの座標を調べる起点となる座標(≡印ヲ追ウモノ※イメージです)
				//今ループで周囲の歩数を求める座標X,Y
				searchStepMapX = (nextCoordinate[nextStepChaser]&0xf0 )>>4;
				searchStepMapY = nextCoordinate[nextStepChaser]&0x0f;
				//次に探索する座標の歩数+1が、周囲の区間に入れるべき値
				nextStepValue = mapState.StepMap[searchStepMapX][searchStepMapY] + 1 ;
		
				if(Flag_debug_stepmap == 1)
				{
			
					//デバック
					myprintf("(nextStepMarker,nextStepChaser)=(%4d,%4d)\n\r",nextStepMarker,nextStepChaser);
					myprintf("(searchStepMapX,searchStepMapY)=(%4d,%4d)\n\r",searchStepMapX,searchStepMapY);
					myprintf("nextStepValue =%4d\n\r",nextStepValue);
					myprintf("(nextCoordinate[nextStepChaser]) =(%3d,%3d)\n\n\n\r",(nextCoordinate[nextStepChaser]&0xf0)>>4,nextCoordinate[nextStepChaser]&0x0f );
				}
				
	
			}//while
			
	//スタート帰り時のデバッグ　ここまで
	
//	Flag_debug_stepmap = 0;		
		
*/	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	myprintf("Finish_wait \n\n\r");

	//探索したマップ(壁情報)の表示
	
	if(GOAL_NUM == 1)
	{
		myprintf("GOAL=(%2d,%2d)\n\n\r",GOAL_X,GOAL_Y);
	}
	if(GOAL_NUM == 4)
	{
		myprintf("GOAL=\n\r");
		myprintf("(%2d,%2d),(%2d,%2d)\n\r",  GOAL_XL,GOAL_YH,GOAL_XH,GOAL_YH);
		myprintf("(%2d,%2d),(%2d,%2d)\n\n\r",GOAL_XL,GOAL_YL,GOAL_XH,GOAL_YL);
	}
	
	
	//上にX座標を書く(0,1,2,...,15)
	for(m=0;m<16;m++)
	{
		myprintf("%4d ",m);
	}
	myprintf("\n\r");
	//壁情報を書く
	for(n=15;n>=0;n--)
	{
		//柱と北の壁を書く
		for(m=0;m<16;m++)
		{
			
			//myprintf("+");//その区間(m,n)での左上の柱
			if( (mapState.Wall[m][n]&0x10)==0x00 )//北の壁が「未知」なら
			{
				myprintf("\033[36m+***+\033[39m");//0000");//####");//北の壁を表す 青で\033[36m　それで色を戻す\033[39m
			}
			else if( (mapState.Wall[m][n]&0x01)==0x01 )//北の壁が「壁あり」なら
			{
				myprintf("+---+");//北の壁を表す
			}
			else//「壁なし」
			{
				myprintf("+   +");
			}
		//	myprintf("+");//その区間(m,n)での右上の柱
		}
		//改行及び行頭へ移動
		myprintf("\n\r");
	
		
		//西の壁、壁情報(0~255)を書く、東の壁を書く
	//	LED2=1;
		for(m=0;m<16;m++)
		{
			if( (mapState.Wall[m][n]&0x20)==0x00 )//西の壁が「未知」なら
			{
				myprintf("\033[36m*\033[39m");//#");//西の壁が未知を表す
			}
			else if( (mapState.Wall[m][n]&0x02)==0x02 )//西の壁が「壁あり」なら
			{
				myprintf("|");//西の壁を表す
			}
			else//「壁なし」
			{
				myprintf(" ");
			}
			
			//数値を書く
			if( (mapState.Wall[m][n]&0x10)==0x00 )//北の壁が「未知」なら
			{
				myprintf("\033[36m%3d\033[39m",mapState.Wall[m][n]&0x0F);	
			//	myprintf("\033[36m%3d\033[39m",mapState.StepMap[m][n]);//&0x0F);
				//青で\033[36m　それで色を戻す\033[39m
			}
			else
			{
				myprintf("%3d",mapState.Wall[m][n]&0x0F);	
			//	myprintf("%3d",mapState.StepMap[m][n]);//&0x0F);
			}
		
			if( (mapState.Wall[m][n]&0x80)==0x00 )//東の壁が「未知」なら
			{
				myprintf("\033[36m*\033[39m");//#");//東の壁が未知を表す
			}
			else if( (mapState.Wall[m][n]&0x08)==0x08 )//東の壁が「壁あり」なら
			{
				myprintf("|");//東の壁を表す
			}
			else//「壁なし」
			{
				myprintf(" ");
			}
		}
		//右端にY座標を書く
		myprintf("%3d",n);//右端にY座標を書く
		//改行及び行頭に移動
		myprintf("\n\r");
		
		
		//柱と南の壁を書く
		for(m=0;m<16;m++)
		{
		//	myprintf("+");//その区間(m,n)での左下の柱
			if( (mapState.Wall[m][n]&0x40)==0x00 )//南の壁が「未知」なら
			{
				myprintf("\033[36m+***+\033[39m");//0000");//####");//南の壁を表す
			}
			else if( (mapState.Wall[m][n]&0x04)==0x04 )//南の壁が「壁あり」なら
			{
				myprintf("+---+");//南の壁を表す
			}
			else//「壁なし」
			{
				myprintf("+   +");
			}
		//	myprintf("+");//その区間(m,n)での右下の柱
		}
		//改行及び行頭へ移動
		myprintf("\n\r");
	
		
	}
	
	//改行及び行頭へ移動
	myprintf("\n\n\n\r");

	//myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);
	
	
	
	//上にX座標を書く(0,1,2,...,15)
	for(m=0;m<16;m++)
	{
		myprintf("%4d ",m);
	}
	myprintf("\n\r");
	//壁情報を書く
	for(n=15;n>=0;n--)
	{
		//柱と北の壁を書く
		for(m=0;m<16;m++)
		{
			
			//myprintf("+");//その区間(m,n)での左上の柱
			if( (mapState.Wall[m][n]&0x10)==0x00 )//北の壁が「未知」なら
			{
				myprintf("\033[36m+***+\033[39m");//0000");//####");//北の壁を表す 青で\033[36m　それで色を戻す\033[39m
			}
			else if( (mapState.Wall[m][n]&0x01)==0x01 )//北の壁が「壁あり」なら
			{
				myprintf("+---+");//北の壁を表す
			}
			else//「壁なし」
			{
				myprintf("+   +");
			}
		//	myprintf("+");//その区間(m,n)での右上の柱
		}
		//改行及び行頭へ移動
		myprintf("\n\r");
	
		
		//西の壁、壁情報(0~255)を書く、東の壁を書く
	//	LED2=1;
		for(m=0;m<16;m++)
		{
			if( (mapState.Wall[m][n]&0x20)==0x00 )//西の壁が「未知」なら
			{
				myprintf("\033[36m*\033[39m");//#");//西の壁が未知を表す
			}
			else if( (mapState.Wall[m][n]&0x02)==0x02 )//西の壁が「壁あり」なら
			{
				myprintf("|");//西の壁を表す
			}
			else//「壁なし」
			{
				myprintf(" ");
			}
			
			//数値を書く
			if( (mapState.Wall[m][n]&0x10)==0x00 )//北の壁が「未知」なら
			{
			//	myprintf("\033[36m%3d\033[39m",mapState.Wall[m][n]&0x0F);	
				myprintf("\033[36m%3d\033[39m",mapState.StepMap[m][n]);//&0x0F);
				//青で\033[36m　それで色を戻す\033[39m
			}
			else
			{
			//	myprintf("%3d",mapState.Wall[m][n]&0x0F);	
				myprintf("%3d",mapState.StepMap[m][n]);//&0x0F);
			}
		
			if( (mapState.Wall[m][n]&0x80)==0x00 )//東の壁が「未知」なら
			{
				myprintf("\033[36m*\033[39m");//#");//東の壁が未知を表す
			}
			else if( (mapState.Wall[m][n]&0x08)==0x08 )//東の壁が「壁あり」なら
			{
				myprintf("|");//東の壁を表す
			}
			else//「壁なし」
			{
				myprintf(" ");
			}
		}
		//右端にY座標を書く
		myprintf("%3d",n);//右端にY座標を書く
		//改行及び行頭に移動
		myprintf("\n\r");
		
		
		//柱と南の壁を書く
		for(m=0;m<16;m++)
		{
		//	myprintf("+");//その区間(m,n)での左下の柱
			if( (mapState.Wall[m][n]&0x40)==0x00 )//南の壁が「未知」なら
			{
				myprintf("\033[36m+***+\033[39m");//0000");//####");//南の壁を表す
			}
			else if( (mapState.Wall[m][n]&0x04)==0x04 )//南の壁が「壁あり」なら
			{
				myprintf("+---+");//南の壁を表す
			}
			else//「壁なし」
			{
				myprintf("+   +");
			}
		//	myprintf("+");//その区間(m,n)での右下の柱
		}
		//改行及び行頭へ移動
		myprintf("\n\r");
	
		
	}
	
	//改行及び行頭へ移動
	myprintf("\n\n\n\r");

	
	
	
	/****************************************/
	/*	既知・未知の表示(240で既知			*/
	/****************************************/
	//myprintf("既知・未知情報表示(上位4ビット16,32,64,128 min16~max255)");
/*	myprintf("1pair:  16=   up            , 32=left,            64= down,          128=right\n\r");
	myprintf("2pair:  48=   up left       , 80=   up down     ,144=   upright,      96= left down,  160= leftright,  192= downright\n\r");
	myprintf("3pair: 112=   up left down  ,176=   up leftright,208=   up downright,224= left downright\n\r");
	myprintf("4pair: 240=   up left downright\n\r");
*/
//	myprintf("hyouji_Map\\n\n\n\r");

	//LEDsimasima(500);
	//上にX座標を書く(0,1,2,...,15)
	for(m=0;m<16;m++)
	{
		myprintf("%4d",m);
	}
	myprintf("\n\r");
	//壁情報を書く
	for(n=15;n>=0;n--)//引いていく場合は注意、
	{
		//柱と北の壁を書く
		for(m=0;m<16;m++)
		{
			myprintf("+");//その区間(m,n)での左上の柱
			if( (mapState.Wall[m][n]&0x01)==0x01 )//北の壁が「壁あり」なら
			{
				myprintf("---");//北の壁を表す
			}
			else
			{
				myprintf("   ");
			}
		}
		//右端の柱を書く
		myprintf("+");//区間(15,n)での右上の柱
		//改行及び行頭へ移動
		myprintf("\n\r");
		
		//西の壁を書き、壁情報(0~255)を書く
		for(m=0;m<16;m++)
		{
			if( (mapState.Wall[m][n]&0x02)==0x02 )//西の壁が「壁あり」なら
			{
				myprintf("|");//西の壁を表す
			}
			else
			{
				myprintf(" ");
			}
		//	myprintf("%3d",mapState.Wall[m][n]&0xF0);
			if(mapState.Wall[m][n] &0xF0 >= 240)//既知区間なら
			{
				myprintf("\033[36m%3d\033[39m",mapState.StepMap[m][n]);//&0x0F);
			}
			else
			{
				myprintf("%3d",mapState.StepMap[m][n]);//&0x0F);
			}
			//myprintf("%4d",mapState.StepMap[m][n]);//&0x0F);
			
		}
		//X=15区画の東の壁を書く
		if( (mapState.Wall[15][n]&0x08)==0x08 )//東の壁が「壁あり」なら
		{
			myprintf("|");//東の壁を表す
		}
		else
		{
			myprintf(" ");
		}
		//右端にY座標を書く
		myprintf("%3d",n);//右端にY座標を書く
		//改行及び行頭に移動
		myprintf("\n\r");
		
	}
	//下端に柱と南の壁を書く
	for(m=0;m<16;m++)
	{
		myprintf("+");//Y=0の区間での左下の柱
		if( (mapState.Wall[m][0]&0x04)==0x04 )//南の壁が「壁あり」なら
		{
			myprintf("---");//南の壁を表す
		}
		else
		{
			myprintf("   ");
		}
	}
	//右端の柱を書く
	myprintf("+");//区間(15,0)での右下の柱
	//改行及び行頭へ移動
	myprintf("\n\n\r");
	
	//myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);

	//END 壁情報表示
	
	
	/////////////////////////////////////////
	//壁情報を出力
	/////////////////////////////////////////
	myprintf("WallInfomation for Debug!!!\n\r");
	
	for(m=0;m<15;m++)
	{
		for(n=0;n<15;n++)
		{
			myprintf("mapState.Wall[%d][%d] = %d;\n\r",m,n,mapState.Wall[m][n]);
		}
		myprintf("\n\r");
	}
	myprintf("WallInfomation_Ended!!\n\n\n\r");
	
	
	//141008昼の迷路サンプル(エクセルで作った迷路)と同じ。つまり、手でやったシミュレートと同じことが起こるはずだ。
/*	mapState.Wall[0][0] = 0xFe;//1110
	mapState.Wall[0][1] = 0xFa;//1010
	mapState.Wall[0][2] = 0xFa;//1010
	mapState.Wall[0][3] = 0xF3;//0011
	mapState.Wall[0][4] = 0xF6;//0110
	
	mapState.Wall[1][0] = 0xF6;//0110
	mapState.Wall[1][1] = 0xFa;//1010
	mapState.Wall[1][2] = 0xF3;//0011
	mapState.Wall[1][3] = 0xF5;//0101
	mapState.Wall[1][4] = 0xF4;//0100
	
	mapState.Wall[2][0] = 0xF4;//0100
	mapState.Wall[2][1] = 0xFb;//1011
	mapState.Wall[2][2] = 0xFd;//1101
	mapState.Wall[2][3] = 0xF5;//0101
	mapState.Wall[2][4] = 0xF4;//0100
	
	mapState.Wall[3][0] = 0xFc;//1100
	mapState.Wall[3][1] = 0xFa;//1010
	mapState.Wall[3][2] = 0xFa;//1010
	mapState.Wall[3][3] = 0xF1;//0001
	mapState.Wall[3][4] = 0xF4;//0100
	
	mapState.Wall[4][0] = 0xF6;//0110
	mapState.Wall[4][1] = 0xF2;//0010
	mapState.Wall[4][2] = 0xF2;//0010
	mapState.Wall[4][3] = 0xF0;//0000
	mapState.Wall[4][4] = 0xF0;//0000
*/	

	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//パスの出力
	/////////////////////////////////////////////////////////////////////////////////////
	
	myprintf("Path1Pivot\n\r");
	myprintf("| NUM | STRT | TURN |\n\r");
	for(numPath=0;numPath<255;numPath++)
	{
		myprintf("| %3d | %4d | %4d |\n\r",numPath,Path1Pivot[numPath].Straight,Path1Pivot[numPath].Turn);
	}
	

	wait(1000);
	//
	while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
	//	myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);	
	}
	
}//lefthandNonstop


























void lefthandslalom(void)
{
	
/*	int m,n;
	int cntLHM = 1;
	int time_run = 200;//200;

	int flag_seat = 1;		//ケツ当てをする場合は1になる。最初はケツ当てから始まるので1にする。
	//Flag_motion_LHM = 0;
	
	t_mapState mapState;
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState.Wall[m][n] = 255;
		}
	}
	mapState.X = 0;
	mapState.Y = 0;
	mapState.Psi = North;
	
	Flag_wall_end = 1;
	//First_run();//始めの一歩（要調整、ケツをくっつけた状態から始めるので、90mmではない）
	//run_start();
	Flag_wall_end = 0;
	
	mapState.X = 0;
	mapState.Y = 1;
	wait(time_run);
	
	while(!(mapState.X = X_goal & mapState.Y == Y_goal))
	{//cntLHM != 4*5){//Coordinate != Goal){
	
	Acc90mm();
		
		while(1)
		{//直進を続ける
			if(refSEN_L < TH_L){//左の壁がない→左に90度ターンして、180mm前進
				Flag_motion_LHM = 1;
				sla_RotLeft(1*1.01);
				/*if(refSEN_R > TH_R){//左に曲がる際、右壁があったらケツ当てをする
					flag_seat = 1;	//
				}*/
				//break;
/*							
			}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;//このとき等速から、減速に遷移しない。(Mode_motionが1のまま、つまり、等速のまま）
				Add180mm();//180mm追加で走る
			
			}else if(refSEN_R < TH_R){//右の壁がない→右に90度ターンして、180mm前進
				Flag_motion_LHM = 3;
				sla_RotRight(1*0.99);
				//flag_seat = 1;
				//break;
						
			}else{//その他（壁に囲まれた→右に180度ターンして、180mm前進
				Flag_motion_LHM = 4;
				flag_seat = 1;
				break;
				
			}			
		}
		if(Flag_motion_LHM==1){
						
		}else if(Flag_motion_LHM==3){
			
		}else if(Flag_motion_LHM==4){
			Reduce90mm();
			wait(time_run);
			RotRight(2);//Uターン
			wait(time_run);
			if(flag_seat == 1){
			//	Backward_tune();
				wait(time_run);
			}
		}
		
				
		cntLHM++;
		
	}	
	
	KnightRider(1200);
	*/
}//lefthandslalom

void AdachiMethod(void)
{
	
	
	
}


void lefthandmethod(void)
{
/*	int m,n;
	int cntLHM = 1;
	int time_run = 200;//200;
	
	t_mapState mapState;
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState.Wall[m][n] = 255;
		}
	}
	mapState.X = 0;
	mapState.Y = 0;
	mapState.Psi = North;
	
	X_goal = GORL_X;
	Y_goal = GORL_Y;

	
//	First_run();//始めの一歩（要調整、ケツをくっつけた状態から始めるので、90mmではない）
	mapState.X = 0;
	mapState.Y = 1;
	wait(time_run);
	
	while(!(mapState.X = X_goal & mapState.Y == Y_goal)){//cntLHM != 4*5){//Coordinate != Goal){
		
			
		
		if(refSEN_L < TH_L){//左の壁がない→左に90度ターンして、180mm前進
			RotLeft(1);
			wait(time_run);
		//	Forward(1);
			wait(time_run);
			
		}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進
		//	Forward(1);	
			wait(time_run);
			
		}else if(refSEN_R < TH_R){//右の壁がない→右に90度ターンして、180mm前進
			RotRight(1);
			wait(time_run);
		//	Forward(1);
			wait(time_run);
			
		}else{//その他（壁に囲まれた→右に180度ターンして、180mm前進
			RotRight(2);
			wait(time_run);
		//	Backward_tune();
			wait(time_run);
		//	First_run();
			wait(time_run);
			
		}
		
		cntLHM++;
		
	}	
	
	KnightRider(1200);
*/	
}//lefthandmethod


void lefthandstop(void)
{
/*	int i;
	int m,n;
	int cntLHM = 1;
	int time_run = 200;//200;//[msec]

//	Flag_motion_LHM = 0;

	t_mapState mapState;
	for(m=0;m<16;m++)
	{
		for(n=0;n<16;n++)
		{
			mapState.Wall[m][n] = 255;
		}
	}
	mapState.X = 0;
	mapState.Y = 0;
	mapState.Psi = North;

//	myprintf("First\n\rV=%f,Vmin=%f,V*=%f,D=%f\n\r",speed,min_speed,target_speed,distance);
			
	wait(time_run);

	while( !( (mapState.X == GOAL_X & mapState.Y == GOAL_Y) | (SW1!=0&SW2==0) ) )//緑スイッチが押されるまで)
	{
		Acc90mm();//半区画進む
		//区間の切れ目なのでセンサ値取得する
		LED3=1;
		acquireSensorValue();
		LED3=0;
		//区間の切れ目なので座標を更新する
		UpdateCoordinate(&mapState);//座標更新
		Reduce90mm();
		wait(500);
		//以下、センサ値によって行動を選択する
		if(refSEN_L < TH_L)
		{//左の壁がない→左に90度ターンして、180mm前進
			
			RotLeft(1);
			wait(time_run);
			
		}
		else if(refSEN_FL < TH_FL | SEN_FR < TH_FR)
		{//前壁がない→180mm前進
		//	Forward(1);
			wait(time_run);
			
		}
		else if(refSEN_R < TH_R)
		{//右の壁がない→右に90度ターンして、180mm前進
			RotRight(1);
			wait(time_run);
		
		}
		else
		{//その他（壁に囲まれた→右に180度ターンして、180mm前進
			RotRight(2);
			wait(time_run);
			
		}
					
		cntLHM++;
		
	}	
	
	KnightRider(3,1000);
	
	while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
		myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);	
	}
	wait(1000);
	
	while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
		myprintf("| (X,Y,Psi) = (%2d,%2d,%2d) |\n\r",mapState.X,mapState.Y,mapState.Psi);	
	}
	wait(1000);
	
	while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
	//	myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);	
	}
*/	
}//lefthandstop

