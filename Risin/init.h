/* init.h */

volatile unsigned long cntcmt=0;	//48day(永い)までカウントできる(周期1msecなら)



void init_adc(void)
{
	// スタンバイ解除 
	STB.CR4.BIT._AD1 = 0;
	STB.CR4.BIT._AD0 = 0;
	
	// ADIイネーブル 
	AD1.ADCSR.BIT.ADIE = 0;
	AD0.ADCSR.BIT.ADIE = 0;
	
	// トリガイネーブル 
	AD1.ADCSR.BIT.TRGE = 0;
	AD0.ADCSR.BIT.TRGE = 0;
	
	// ADF コントロール 
	AD1.ADCSR.BIT.CONADF = 0;
	AD0.ADCSR.BIT.CONADF = 0;
	
	// ステートコントロール 
	AD1.ADCSR.BIT.STC = 0; 
	AD0.ADCSR.BIT.STC = 0; 
	
	// A/D変換時間;Pφ/4 ⇒動かなかったら早くしよう
	AD1.ADCSR.BIT.CKSL = 0; 
	AD0.ADCSR.BIT.CKSL = 0; 
	
	// シングルモード=>4チャンネルスキャン、2チャンネルスキャン 
	AD1.ADCSR.BIT.ADM = 1;	//4ch	1;	//2ch	4
	AD0.ADCSR.BIT.ADM = 0;	//4ch	1;	//2ch	4//シングル	0
		
	// 1サイクルスキャン 
	AD1.ADCSR.BIT.ADCS = 0;
	//AD1.ADCSR.BIT.ADCS = 0;
	
	// チャネルセレクト CH0 AN0 
	AD1.ADCSR.BIT.CH = 3;	//011 ch1は、AN4,5,6,7	1;	//001 ch1は、AN4,5
	AD0.ADCSR.BIT.CH = 0;	//011 ch1は、AN4,5,6,7	1;	//001 ch1は、AN4,5
}


//CMT割り込み初期設定
//resetprg.cの21行目を右のようにする #define SR_Init    0x00000000
void init_cmt0(void)
{
	//動作設定
    STB.CR4.BIT._CMT = 0;	//CMTスタンバイ解除(クロック供給開始)
    CMT.CMSTR.BIT.STR0 = 0;    //設定のためカウント停止
	
	CMT0.CMCSR.BIT.CMF = 0;      // CMCNTとCMCORの値が一致したか否かを示す  0:不一致, 1:一致
    CMT0.CMCSR.BIT.CMIE = 1;   // 1:コンペアマッチ割り込み許可(イネーブル)
    CMT0.CMCSR.BIT.CKS = 3;//吉川値	0;    // 0:クロックセレクトPφ/8	1sec=3,125,000 10msec=31,250 1msec=3125
	INTC.IPRJ.BIT._CMT0 = 13;//吉川値	15;//13;	//割り込み優先度15

	//カウント周期設定
	CMT0.CMCOR = 49-1;//吉川値49-1で1ms	3125; //Pφ/8で1msec
	
	//吉川君は追加していた
	CMT.CMSTR.BIT.STR0 = 1;    //設定のためカウント停止
	
}


//MTU2割り込み初期設定(左右のモータPWM値)
void init_mtu_motor(void)
{
	//動作選択
	PFC.PECRL1.BIT.PE1MD = 0;//1;		//TOOC0BをMTU用端子に設定
	PFC.PECRL2.BIT.PE5MD = 0;//1;		//TIOC1BをMTU用端子に設定
	PFC.PEIORL.BIT.B1 = 1;			//TIOC0Bを出力端子に設定
//吉川君には書いてないので、コメントアウト
//	PFC.PEIORL.BIT.B5 = 1;			//TIOC1Bを出力端子に設定
	
	STB.CR4.BIT._MTU2 = 0;			//MTUスタンバイ解除(クロック供給開始)
	MTU2.TSTR.BYTE = 0; 			//カウンタ動作停止
	
	MTU20.TIER.BIT.TGIEB = 1;		//ch0のTGRBコンペアマッチによる割り込みを許可
	MTU21.TIER.BIT.TGIEB = 1;		//ch2のTGRBコンペアマッチによる割り込みを許可

	INTC.IPRD.BIT._MTU20G = 13;//15;		//割り込み優先度を高めに設定　14
	INTC.IPRD.BIT._MTU21G = 14;		//割り込み優先度を高めに設定　13

	
	//カウンタクロックの選択
	MTU20.TCR.BIT.TPSC = 1; 		// プリスケーラ MPφ/4  (MPφ=25MHz->0.04us) 1パルスで:0.16us
	MTU21.TCR.BIT.TPSC = 1; 		// プリスケーラ MPφ/4  (MPφ=25MHz->0.04us) 1パルスで:0.16us
	
/*	switch(Prescale_mtu){
		case 4:		
			MTU20.TCR.BIT.TPSC = 1; 		// プリスケーラ MPφ/4  (MPφ=25MHz->0.04us) 1パルスで:0.16us
			MTU21.TCR.BIT.TPSC = 1; 		// プリスケーラ MPφ/4  (MPφ=25MHz->0.04us) 1パルスで:0.16us
			break;
			
		case 16:
			MTU20.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us) 1パルスで:0.64us
			MTU21.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us) 1パルスで:0.64us
			break;
							
	}
*/


	
	//カウンタクリア要因の選択
	MTU20.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	MTU21.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	
	//アウトプットコンペアレジスタの選択(コンペアマッチ前後での波形出力レベルの設定0/1)
	MTU20.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU20.TIOR.BIT.IOB = 2;			//初期ゼロ、コンペアマッ１？//5; 		// 初期状態:1 コンペアマッチ:0
	MTU21.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU21.TIOR.BIT.IOB = 2;//5; 	// 初期状態:1 コンペアマッチ:0
	
	//周期・デューティー設定
	MTU20.TGRA = 10000;//65535;//6000;//156;		//PWM周期 10msec=15625cnt 1msec=1563cnt
	MTU20.TGRB = 5;//MTU20.TGRA/2;///5;//パルス幅4us	100;//MTU20.TGRA;	//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？
	MTU21.TGRA = 10000;//65535;//6000;//156;		//PWM周期 10msec=15625cnt 1msec=1563cnt
	MTU21.TGRB = 5;//MTU21.TGRA/2;//5;//パルス幅4us	100;//MTU21.TGRA;	//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？
	
	//PWMモードの設定
	MTU20.TMDR.BIT.MD = 3; 			// PWM2モード
	MTU21.TMDR.BIT.MD = 3; 			// PWM2モード
}

