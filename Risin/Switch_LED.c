/* Switch_LED(mode) */
#include"iodefine.h"
#include"mydefine.h"

int Switch_LED(mode){
	
	if(SW1==0&SW2!=0)//白スイッチのみON
	{
		mode += 1;
		if(mode > 32){mode=0;}
		wait(150);
		LED_all(mode);							
	}
	return mode;
}
