/* printf_uOLED.c */
//SCI0を使っている
#include <stdio.h>
#include <stdarg.h>
#include "iodefine.h"

void init_uOLED(void){

	long divider;
	unsigned short baud = 9600;//uOLEDは9600らしい。//38400;	//115200;//38400;			//ビットレート38400bps
	unsigned char tmp;

	STB.CR3.BIT._SCI0=0;		// STANDBY解除
	PFC.PACRL1.BIT.PA0MD=1;		//RXD0用シリアルポートを設定→uOLEDのTXにつなぐ
	PFC.PACRL1.BIT.PA1MD=1;		//TXD0用シリアルポートを設定→uOLEDのRXにつなぐ
	PFC.PACRL1.BIT.PA2MD=0;		//RES用I/Oポートを設定→uOLEDのRESにつなぐ
	SCI0.SCSCR.BYTE=0x00;		//送受信割り込み禁止
	
	//ビットレート関連の計算
	divider = 32;
	if ( baud < 300 ) {
		tmp = 0x03;
		divider = divider << 6;
	} else
	if ( baud < 1200 ) {
		tmp = 0x02;
		divider = divider << 4;
	} else
	if ( baud < 4800 ) {
		tmp = 0x01;
		divider = divider << 2;
	} else {
		tmp = 0x00;
	}
	
	SCI0.SCSMR.BYTE = tmp;		/* ASYNC、8bit、Parity-NONE、Stop-1、Clk = tmp	*/
	tmp = (unsigned char)(25000000/divider/baud)-1;
	SCI0.SCBRR = tmp;			//ビットレート設定
	SCI0.SCSCR.BIT.TE=1;		//送信許可
	SCI0.SCSCR.BIT.RE=1;		//受信許可
}
void init_uOLED_115200(void){

	long divider;
	unsigned short baud = 115200;//uOLEDは9600らしい。//38400;	//115200;//38400;			//ビットレート38400bps
	unsigned char tmp;

	STB.CR3.BIT._SCI0=0;		// STANDBY解除
	PFC.PACRL1.BIT.PA0MD=1;		//RXD0用シリアルポートを設定→uOLEDのTXにつなぐ
	PFC.PACRL1.BIT.PA1MD=1;		//TXD0用シリアルポートを設定→uOLEDのRXにつなぐ
	PFC.PACRL1.BIT.PA2MD=0;		//RES用I/Oポートを設定→uOLEDのRESにつなぐ
	SCI0.SCSCR.BYTE=0x00;		//送受信割り込み禁止
	
	//ビットレート関連の計算
	divider = 32;
	if ( baud < 300 ) {
		tmp = 0x03;
		divider = divider << 6;
	} else
	if ( baud < 1200 ) {
		tmp = 0x02;
		divider = divider << 4;
	} else
	if ( baud < 4800 ) {
		tmp = 0x01;
		divider = divider << 2;
	} else {
		tmp = 0x00;
	}
	
	SCI0.SCSMR.BYTE = tmp;		/* ASYNC、8bit、Parity-NONE、Stop-1、Clk = tmp	*/
	tmp = (unsigned char)(25000000/divider/baud)-1;
	SCI0.SCBRR = tmp;			//ビットレート設定
	SCI0.SCSCR.BIT.TE=1;		//送信許可
	SCI0.SCSCR.BIT.RE=1;		//受信許可
}
void put1byte_uOLED(char c) {
	while ( SCI0.SCSSR.BIT.TDRE == 0 ) ;
	SCI0.SCSSR.BIT.TDRE = 0;
	SCI0.SCTDR = c;
}

void putnbyte_uOLED(char *buf,int len) {
	int c;
   
    for(c = 0; c < len; c++){
		put1byte_uOLED(buf[c]);
	}           
}

int printf_uOLED(const char *fmt, ...){
	static char buffer[100];
	int len;
	
	va_list ap;
	va_start(ap, fmt);
	
	len = vsprintf(buffer, fmt, ap);
	putnbyte_uOLED(buffer, len);
	va_end(ap);
	return len;
}
