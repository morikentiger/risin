/* run.h */



/* プロトタイプ宣言 */
int run(char mode_run,float first_speed,float critical_speed,float end_speed,float Accel,float Decel,float target_distance);
void RotRight(float r_ang);
void RotLeft(float r_ang);
//プロトタイプ宣言を入れたら、run関数がlefthandNonstop内で、最低速一定でなく加減速した…。




volatile int refSEN_L,refSEN_FL,refSEN_FR,refSEN_R;//半区間でのセンサ値（左壁、前左、前右、右壁）
volatile int SEN0,SEN1,SEN2,SEN3;
volatile int SEN0_off,SEN1_off,SEN2_off,SEN3_off;
volatile int SEN0_on, SEN1_on, SEN2_on ,SEN3_on;
volatile int SEN_L,SEN_FL,SEN_FR,SEN_R;//常に測るセンサ値（左壁、前左、前右、右壁）



volatile float distance=0;	//距離[mm] 計算用
volatile float angle=0;		//角度[deg]計算用

volatile float accel = Def_accel;
volatile float decel = Def_decel;
volatile float ang_accel = Def_ang_accel;
volatile float ang_decel = Def_ang_decel;

volatile float speed=10;//50;
volatile float ang_speed=10;


volatile float target_speed = Def_target_speed;
volatile float target_ang_speed = Def_target_ang_speed;

volatile char  dir_L,dir_R;//左右輪の回転方向というか、スラロームで角速度を速度に変換するときに、足すのか引くのかの問題。

volatile unsigned int TGRA_R=65535;//3000;
volatile unsigned int TGRA_L=65535;//3000;//左右のTGRA


volatile int Flag_Calculation_Start = 0;//run関数での加減速時の計算をCMTでスタートさせるためのフラグ

volatile int Flag_Comb_Rot = 0;	//なんのフラグだろう(141006_0434)→Motor_前後左右のマクロ関数にて、モーターの動作の状態を表すフラグ(ステータス)

volatile char Flag_control_on = 1;
volatile char Flag_all_control= 1;

//デバック用フラグ(run.h内のみ)　run関数のプリントf
char flag_debug_kagensoku = 0;
	

//PID制御用の変数
double error,i_error,d_error,error_last;	//偏差
double control;								//制御量(PWM値のスケール 0〜1023)
//float speed_l=1,speed_r=1;						//左右のデューティ(0〜1563)
float KS_L	= KSINT_L;
float KS_FL	= KSINT_FL;
float KS_FR	= KSINT_FR;
float KS_R	= KSINT_R;

float KP = defKP;


unsigned char FlagkagensokuON = 0;//0で速度そのまま(なにもしない)1 で加減速する



int run(char mode_run,float first_speed,float critical_speed,float end_speed,float Accel,float Decel,float target_distance)
//void run(void)
{
	float distance_speed=0.0;
	float max_speed=0.0;
	float limit_speed=0.0;
	float decel_distance=0.0;
	char flag_once_printf=0.0;
	
	switch(mode_run)
	{
		case 1://前進
			if(flag_debug_kagensoku==1){myprintf("//Forward//\n\r");}
			//壁の有無を判断するセンサ値を格納したことを表すフラグをリセットする
//			Flag_sen_wall = 0;
			accel = Def_accel;//Accel;//Def_accel;
			decel = Def_decel;//Decel;//Def_decel;
			ang_accel = 0;
			ang_decel = 0;
			
			distance = 0.0;
			//speed = 0.0;
			speed=first_speed;
			max_speed = critical_speed;
			angle  =0.0;
			ang_speed = 0.0;
			target_speed = max_speed;
			FlagkagensokuON = 1;//加減速する
		//	distance_speed = sqrt( first_speed*first_speed + 2*Accel*target_distance );
		/*	if(distance_speed <= end_speed)
			{
				end_speed = distance_speed;
				target_speed = distance_speed;
			}
			else
			{
		*/		
		//	LED1 = 1;
			limit_speed = sqrt( (Decel*first_speed*first_speed + Accel*end_speed*end_speed + 2*Accel*Decel*target_distance)/(Accel+Decel) );
		//	LED1 = 0;	
				//上のlimit_speedは予めすべてのパラメータの組み合わせについて、計算しておいて(テーブル化)、そこから値だけ選ぶようにすべき？
				//target_speed = max_speed;//
				target_speed = (limit_speed>=max_speed)*max_speed + (limit_speed<max_speed)*limit_speed;
		//	}
			if(decel!=0){
				decel_distance = (target_speed*target_speed - end_speed*end_speed)/(2*decel);
			//	LED1=0;
			//	LED3=0;
			}else{
			//	LED1=1;
			//	LED3=1;
			}
			Motor_forward();//前回転
		if(flag_debug_kagensoku==1){myprintf("decel_distance=%f\n\r",decel_distance);}
		if(flag_debug_kagensoku==1){myprintf("AccelStart\n\rV=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_speed,distance);}
			Motor_start();
			
			Flag_Calculation_Start = 1;
			
/*			//加速度を段階分けする。元に戻す時は、ここからコメントアウトすればよい
			accel = Def_accel*0.1;//Accel;//Def_accel;
			decel = Def_decel*0.1;//Decel;//Def_decel;
			target_speed = min_speed;
			
			while(distance <= (target_distance*0.1) );
			
			accel = Def_accel*0.1;//Accel;//Def_accel;
			decel = Def_decel*0.1;//Decel;//Def_decel;
			target_speed = max_speed;
			while(distance <= (target_distance*0.2) );
			
			accel = Def_accel*0.5;//Accel;//Def_accel;
			decel = Def_decel*0.5;//Decel;//Def_decel;
			target_speed = max_speed;
			while(distance <= (target_distance*0.3) );
			
*/			
			while(distance <= (target_distance - decel_distance) )
			{
			/*	if(speed >= target_speed)
				{
					FlagkagensokuON = 0;
					speed = target_speed;
				}
			*/	
			}
		if(flag_debug_kagensoku==1){myprintf("DecelStart\n\rV=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_speed,distance);}
			//LED4=1;
			target_speed = end_speed;//(limit_speed>=end_speed)*end_speed + (limit_speed<end_speed)*limit_speed;//end_speed;
			FlagkagensokuON = 1;
			
		if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_speed,distance);}
			while(distance <= target_distance)
			{
			/*	if(speed <= target_speed)
				{
					FlagkagensokuON = 0;
					speed = target_speed;
				}
			*/
			}//cmtで1msec毎にdistanceを計算して180mmになるまでモータを動かす
		//	if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_speed,distance);}
		
			//終端速度が最低速度以下ならモーターを止める
			if(end_speed <= min_speed){Motor_stop();}
			Flag_Calculation_Start = 0;
		if(flag_debug_kagensoku==1){myprintf("Finish\n\rV=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\n\n\r",speed,max_speed,limit_speed,target_speed,distance);}
			break;
		
		
		case 2://右超新地旋回
		case 3://左超新地旋回
			
			if(flag_debug_kagensoku==1){myprintf("//Rotaition//\n\r");}
			accel = 0;
			decel = 0;
			ang_accel = Def_ang_accel;//Accel;//Def_ang_accel;
			ang_decel = Def_ang_decel;//Decel;//Def_ang_decel;
			angle = 0.0;
			//ang_speed = 0.0;
			ang_speed = first_speed;
			max_speed = critical_speed;
			speed = 0.0;
			//target_speed = 0.0;
			distance = 0.0;
		/*	distance_speed = sqrt( first_speed*first_speed + 2*Accel*target_distance );
			if(distance_speed <= end_speed)
			{
				end_speed = distance_speed;
				target_ang_speed = distance_speed;
			}
			else
			{
		*/	//	limit_speed = sqrt( (Decel*first_speed*first_speed + Accel*end_speed*end_speed + 2*Accel*Decel*target_distance)/(Accel+Decel) );
				//上のlimit_speedは予めすべてのパラメータの組み合わせについて、計算しておいて(テーブル化)、そこから値だけ選ぶようにすべき？
				target_ang_speed = max_speed;//(limit_speed>=max_speed)*max_speed + (limit_speed<max_speed)*limit_speed;
		//	}
		 	if(ang_decel!=0){
				decel_distance = (target_ang_speed*target_ang_speed - end_speed*end_speed)/(2.0*ang_decel);
			//	LED1=0;
			//	LED3=0;
			}else{
			//	LED1=1;
			//	LED3=1;
			}
		if(flag_debug_kagensoku==1){myprintf("decel_distance=%f\n\rtarget_ang_speed=%f\n\rend_speed=%f\n\rDecel=%f\n\r",decel_distance,target_ang_speed,end_speed,ang_decel);}
		if(flag_debug_kagensoku==1){myprintf("target_ang_speed^2=%f\n\rend_speed^2=%f\n\r",target_ang_speed*target_ang_speed,end_speed*end_speed,ang_decel);}
			
			if(mode_run == 2){Motor_CW();}//Right_rotation
			else{Motor_CCW();}//Left_rotation
			dir_R = dir_L = 1;//方向の初期化			
		if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_ang_speed,angle);}
			Motor_start();
			Flag_Calculation_Start = 1;//計算開始
			while(angle <= (target_distance - decel_distance) );
			//LED4=1;
			target_ang_speed = end_speed;//(limit_speed>=end_speed)*end_speed + (limit_speed<end_speed)*limit_speed;//end_speed;
		if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_ang_speed,angle);}
		
			while(angle <= target_distance);//cmtで1msec毎にdistanceを計算して180mmになるまでモータを動かす
			Motor_stop();
			Flag_Calculation_Start = 0;//計算終了
		
		if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_ang_speed,angle);}
		
			break;
			
		case 4://右スラローム
		
		case 5://左スラローム
			accel = 0;//Def_accel;
			decel = 0;//Def_decel;
			
			ang_accel = Def_ang_accel;
			ang_decel = Def_ang_decel;
			
			angle = 0.0;
			ang_speed = first_speed;
			max_speed = critical_speed;
			speed = Def_target_speed;//500 141004_1533時点
			target_speed = Def_target_speed;
				//これで重心の加減速は行われない
			
		/*	distance_speed = sqrt( first_speed*first_speed + 2*Accel*target_distance );
			if(distance_speed <= end_speed)
			{
				end_speed = distance_speed;
				target_ang_speed = distance_speed;
			}
			else
			{
		*/	//	limit_speed = sqrt( (Decel*first_speed*first_speed + Accel*end_speed*end_speed + 2*Accel*Decel*target_distance)/(Accel+Decel) );
				//上のlimit_speedは予めすべてのパラメータの組み合わせについて、計算しておいて(テーブル化)、そこから値だけ選ぶようにすべき？
				target_ang_speed = max_speed;//(limit_speed>=max_speed)*max_speed + (limit_speed<max_speed)*limit_speed;
		//	}
			decel_distance = (target_ang_speed*target_ang_speed - end_speed*end_speed)/(2*Decel);
			Motor_forward();//前回転
			
		/*	if(mode_run == 4){Motor_CW();}//Right_rotation
			else{Motor_CCW();}//Left_rotation
		*/	
			
		//	if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f\n\r",speed,max_speed,limit_speed,target_speed);}
			
			
			//前距離10mm進む
			distance = 0.0;
			dir_R = 1;dir_L = 1;//方向の初期化
			
			Flag_Calculation_Start = 1;
			Motor_start();
			
		LED0=1;
			while(distance <= Def_sla_forward_distance);
		LED0=0;	
			//旋回部
			angle = 0.0;
			if(mode_run == 4)//右スラロームの場合//おそらく逆なのでは？未修正141006_1332時点
			{
				dir_R =  1;//右の回転にマイナス
				dir_L = -1;//左の回転にプラス
			}
			else if(mode_run == 5)//左スラロームの場合
			{
		LED2=1;
				dir_R = -1;//右の回転にプラス
				dir_L =  1;//左の回転にマイナス
			}
		LED1=1;
			while(angle <= target_distance/3 );//(target_distance - decel_distance) );
			target_ang_speed = speed;//max_speed;
		LED1=0;
			
		LED2=1;
			while(angle <= target_distance*2/3);
		LED2=0;
			//LED4=1;
			target_ang_speed = speed;//(limit_speed>=end_speed)*end_speed + (limit_speed<end_speed)*limit_speed;//end_speed;
		LED3=1;
		//ここでずーと光続けていたので、angleがtarget_distanceになっていないと思われ
			while(angle <= target_distance);//{if(cntcmt%100==0)myprintf("a=%f,V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",ang_accel,speed,max_speed,limit_speed,target_ang_speed,angle);}
			//cmtで1msec毎にdistanceを計算して180mmになるまでモータを動かす
			
		LED3=0;	
			//後ろ距離10mm進む
			distance = 0.0;
			dir_R = 1;dir_L = 1;//方向の初期化
		LED4=1;
			while(distance <= Def_sla_backward_distance);
		LED4=0;				
			//if(end_speed <= min_speed) Motor_stop();スラロームなのでモーター止めない
			Flag_Calculation_Start = 0;
		//	if(flag_debug_kagensoku==1){myprintf("V=%f,Vmax=%f,Vlim=%f,Vtarget=%f,D=%f\n\r",speed,max_speed,limit_speed,target_speed,distance);}
		
			break;
		
		
	}
	
}//run



void testslalom_R(COUNT)
{
	
	int cntLHM = 1;
	int cntslalom = 0; 
	int time_run = 200;//200;

	int flag_seat = 1;		//ケツ当てをする場合は1になる。最初はケツ当てから始まるので1にする。
	
	wait(time_run);
		
//	LED1=1;	
	Flag_all_control=1;//1;//最初姿勢制御入れてまっすぐになってから		
	Acc90mm();
//	Add180mm();
	Flag_all_control=1;//制御を切る
						
//	LED1=0;
//	LED2=1;
	for(cntslalom=0;cntslalom<COUNT;cntslalom++)
	{
		sla_RotRight(1);
//		Add180mm();
	}
//	LED2=0;
//	LED3=1;
	Reduce90mm();
//	LED3=0;
	
	
	cntslalom++;
			
	cntLHM++;
	
	
}

void testslalom_L(COUNT)
{
	
	int cntLHM = 1;
	int cntslalom = 0; 
	int time_run = 200;//200;

	int flag_seat = 1;		//ケツ当てをする場合は1になる。最初はケツ当てから始まるので1にする。
	
	
	wait(time_run);
	
//	LED1=1;
	Flag_all_control=0;//1;//最初制御してまっすぐになってから
	Acc90mm();
//	Add180mm();
	Flag_all_control=0;//制御を切る
						
//	LED1=0;
//	LED2=1;
	for(cntslalom=0;cntslalom<COUNT;cntslalom++)
	{
		sla_RotLeft(1);
//		Add180mm();
	}
	
//	LED2=0;
//	LED3=1;
	Reduce90mm();
//	LED3=0;
	
	
	cntslalom++;
			
	cntLHM++;
	
	
}



int sla_RotRight(float r_ang)
{
	
	
	//run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);//90mm加速
	Flag_control_on=0;//制御を切る
	run(4,Def_sla_minangspeed,Def_slaRtargetangspeed,Def_sla_minangspeed,Def_sla_angAccel,Def_sla_angAccel,Def_slaR_angle);//右スラローム
	//run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);//90mm
	Flag_control_on = 1;
	

}

int sla_RotLeft(float r_ang)
{
	//run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);//90mm加速
	Flag_control_on=0;//制御を切る
	run(5,Def_sla_minangspeed,Def_slaLtargetangspeed,Def_sla_minangspeed,Def_sla_angAccel,Def_sla_angAccel,Def_slaL_angle);//右スラローム
	//run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);//90mm
	Flag_control_on = 1;
	
	
}

int First10mm(void)
{
	run(1,min_speed,min_speed,min_speed,accel,decel,10);
}

int Acc90mm(void)
{

	Flag_control_on=1;//制御を入れる
//	run(1,min_speed,min_speed,min_speed,accel,decel,DistanceFirstStep);
	run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90 );//-DistanceFirstStep) );
//	Motor_stop();
}

int Add180mm(void)
{
	Flag_control_on=1;//制御を入れる
	run(1,Def_target_speed,Def_target_speed,Def_target_speed,accel,decel,180);
	//	Motor_stop();
}

int Reduce90mm(void)
{
	Flag_control_on=1;//制御を入れる
	run(1,Def_target_speed,Def_target_speed,min_speed,accel,decel,90);
	wait(10);
	
		Motor_stop();
}
 
void RotRight(float r_ang)
{
	Flag_control_on=0;//制御を切る
	run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90*r_ang);
	Flag_control_on=1;//制御を入れる
	wait(10);
}

void RotLeft(float r_ang)
{
	Flag_control_on=0;//制御を切る
	run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90*r_ang);//91*r_ang);//tread83でRが+1(これでよいとする)
	Flag_control_on=1;//制御を入れる
	wait(10);
}

/*
void RotRight(float r_ang)
{
	Flag_control_on=0;//制御を切る
	run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,88.75*0.75*r_ang);
//	Flag_control_on=1;//制御を入れる
	wait(300);
}

void RotLeft(float r_ang)
{
	Flag_control_on=0;//制御を切る
	run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90.0*0.7*r_ang);
//	Flag_control_on=1;//制御を入れる
	wait(300);
}

*/

