/* printf_uOLED.h */
extern void init_uOLED(void);
extern void init_uOLED_115200(void);
extern void put1byte_uOLED(char c); 
extern void putnbyte_uOLED(char *buf,int len);
extern int printf_uOLED(const char *fmt, ...);

#define UOLED_RESET_N (PA.DRL.BIT.B2)	//uOLEDのRESへの出力　H(1)にすると送信コマンドを受け付けるようだ
