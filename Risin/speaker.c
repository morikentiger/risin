/* speaker.c */
//スピーカーのテストなど
//swで音を変化させるテスト

#include"iodefine.h"
#include"mydefine.h"



//MTU2割り込み初期設定(スピーカー)
//intprgの以下のように書き換える
// 105 MTU2_2 TGIB2
/*
void interrupt_speaker(void);
#pragma interrupt INT_MTU2_2_TGIB2
void INT_MTU2_2_TGIB2(void){
	interrupt_speaker();
}
*/

void init_mtu_speaker(void){
	//動作選択
	PFC.PECRL2.BIT.PE7MD = 1;		//TIOC2をMTU用端子に設定
	PFC.PEIORL.BIT.B7 = 1;			//TIOC2を出力端子に設定
	STB.CR4.BIT._MTU2 = 0;			//MTUスタンバイ解除(クロック供給開始)
	MTU2.TSTR.BYTE = 0; 			//カウンタ動作停止
	MTU22.TIER.BIT.TGIEB = 1;		//TGRBコンペアマッチによる割り込みを許可
	INTC.IPRD.BIT._MTU20G = 14;		//割り込み優先度を高めに設定　14


	//カウンタクロックの選択
	MTU22.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us):0.64usec
	
	//カウンタクリア要因の選択
	MTU22.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	
	//アウトプットコンペアレジスタの選択(コンペアマッチ前後での波形出力レベルの設定0/1)
	MTU22.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU22.TIOR.BIT.IOB = 5; 		// 初期状態:1 コンペアマッチ:0
	
	//周期・デューティー設定
	MTU22.TGRA = 3551;				//PWM周期 10msec=15625cnt 1msec=1563cnt 
			//今は適当な値(ドだったか？)
	MTU22.TGRB = MTU22.TGRA/2;		//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？ 
			//現在はTGRAの半分、つまり、TGRAが周期である矩形波を与えていることになっている→メインで値を変更し、音階が変わるようになっている
	
	//PWMモードの設定
	MTU22.TMDR.BIT.MD = 3; 			// PWM2モード
	
}




//MTU2割り込み関数(この関数で、左右のモータのデューティをセットする
void interrupt_speaker(void){
	
	int cntmtu = 0;
	
//	LED0 = 0;
//	LED1 = 0;
	
	MTU22.TSR.BIT.TGFB = 0;	//コンペアマッチ発生フラグクリア
	//MTU20.TGRB = speed_r;	//右モータPWMのデューティー





}

//スピーカーのテストなど
void speaker(void){
	
	int i;
	
	init_mtu_speaker();	//MTU2初期設定
	
	MTU2.TSTR.BIT.CST2 = 1;	//MTU2カウンタスタート(モータPWM割り込みスタート)
	
	

	//無限ループ
	while(1){
		
		//ポタン
		if(PE.DRL.BIT.B10 == 0){
			MTU22.TGRA += 10;
			for(i=0;i<100000;i++){}
		}
		
		if(PE.DRL.BIT.B11 == 0){
			MTU22.TGRA -= 10;
			for(i=0;i<100000;i++){}
		}
		
		
	}
	
		
}
