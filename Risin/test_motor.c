/* test_motor.c */
//とりあえずモーター回す

#include"iodefine.h"
#include"mydefine.h"


//グローバル変数宣言
unsigned long cntcmt=0;	//48day(永い)までカウントできる(周期1msecなら)


//CMT割り込み初期設定
//resetprg.cの21行目を右のようにする #define SR_Init    0x00000000
void init_cmt0(void)
{
	//動作設定
    STB.CR4.BIT._CMT = 0;	//CMTスタンバイ解除(クロック供給開始)
    CMT.CMSTR.BIT.STR0 = 0;    //設定のためカウント停止
	
	CMT0.CMCSR.BIT.CMF = 0;      // CMCNTとCMCORの値が一致したか否かを示す  0:不一致, 1:一致
    CMT0.CMCSR.BIT.CMIE = 1;   // 1:コンペアマッチ割り込み許可(イネーブル)
    CMT0.CMCSR.BIT.CKS = 0;    // 0:クロックセレクトPφ/8	1sec=3,125,000 10msec=31,250 1msec=3125
	INTC.IPRJ.BIT._CMT0 = 13;	//割り込み優先度15

	//カウント周期設定
	CMT0.CMCOR = 3125; //Pφ/8で1msec
}


//WAIT関数 引数msecだけ待つ
void wait(int time_msec){	//引数は、待ち時間(msec)
	
	unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + time_msec;	//
	
	while(wait_end != cntcmt){}	//cntcmtがwait_endでない場合ループする(＝待ち時間分だけ待つ)
	
}



//CMT割り込み 周期1msec　ここがメインの処理
void interrpt_cmt0(void){
		
	CMT0.CMCSR.BIT.CMF = 0;	//検知フラグを戻してカウント再開
	
	cntcmt++;	//グローバル変数 48dayまでカウントできる
	
}

/*
void interrupt_motor_right(void){
	// 入れないと怒られるので入れとく
}
*/





void test_motor(void){
	
	long i;
	
	PFC.PEIORL.WORD = 0xffff;		//PEすべて出力
	PFC.PEIORL.BIT.B3 = 1;			//MotorEnable端子を出力に
	
	
	init_cmt0();
	
	CMT.CMSTR.BIT.STR0 = 1;	//CMTカウントスタート(A/D変換)
	
	
	
	//PE.DRL.BIT.B2 = 1;	//RESETだが、電源投入時にリセットが起こってるので、必要ない。
	
	//PE.DRL.BIT.B2 = 0;
	
	PE.DRL.BIT.B3 = 1;	//MotorEnableをHiに。
	
	PE.DRL.BIT.B0 = 1;	//CW/CCW　後退
	PE.DRL.BIT.B4 = 0;
	
	wait(1000);	//7073のデータシート(14/39)を参照。100μs以上待つ必要ある。
	
	while(1){
		
		PE.DRL.BIT.B1 = 1;
		PE.DRL.BIT.B5 = 1;
		//wait(10);				//10msじわじわ 1msforの10000相当？
		for(i=0;i<3000;i++){}	//10000ゆっくり 1000脱調
		
		PE.DRL.BIT.B1 = 0;
		PE.DRL.BIT.B5 = 0;
	//	wait(10);
		for(i=0;i<3000;i++){}
			
		
	}
	
	
	
}
