/* mydefine.h */

//defineにはカッコをつけること(バグが起こる)

/*マイコンの設定*/

/*ハードっぽいポートなど*/
//インターフェースLED
#define LED0 (PA.DRL.BIT.B5) //ポート6←6ってなんだ？
#define LED1 (PA.DRL.BIT.B6)
#define LED2 (PA.DRL.BIT.B7)
#define LED3 (PA.DRL.BIT.B8)
#define LED4 (PA.DRL.BIT.B9)

//スイッチ
#define SW1 (PE.DRL.BIT.B11)	//白
#define SW2 (PE.DRL.BIT.B10)	//緑
#define SWW (PE.DRL.BIT.B11)	//白
#define SWG (PE.DRL.BIT.B10)	//緑
#define SWC (PE.DRL.BIT.B9)		//クリック
#define SWU (PE.DRL.BIT.B12)	//↑
#define SWL (PE.DRL.BIT.B13)	//←
#define SWD (PE.DRL.BIT.B14)	//↓
#define SWR (PE.DRL.BIT.B15)	//→

//センサ用LED
#define LEDS_Rr (PA.DRL.BIT.B12) //右後ろ
#define LEDS_Rf (PA.DRL.BIT.B13) //右前;
#define LEDS_Lr (PA.DRL.BIT.B14) //左後ろ
#define LEDS_Lf (PA.DRL.BIT.B15) //左前

//センサLED待ち時間
#define LED_Wait 2048//1024 //256回

/*
//(東北本番迷路センサ値)
//センサ閾値(壁なしの閾値)(東北本番迷路)
#define TH_L	 	 30//19
#define TH_FL	 	 15
#define TH_FR	 	 25
#define TH_R	  	 45//23

//センサ閾値(壁ありの閾値)(東北本番迷路)
#define TH_Wall_L 	 30
#define TH_Wall_FL	 15
#define TH_Wall_FR	 25
#define TH_Wall_R	 45//23

//制御ONOFF用センサ閾値(東北本番迷路)
#define THcton_L	  40//24//24柱でビクンビクンするので140611
#define THcton_FL	   1//不要
#define THcton_FR	   1//不要
#define THcton_R	  70

//制御用センサ値(東北本番迷路)
#define THctrl_L	  90
#define THctrl_FL	   1//不要
#define THctrl_FR	   1//不要
#define THctrl_R	 112
*/
/*141025土まで(センサ曲げる前)の値

//センサ閾値(壁なしの閾値)
#define TH_L	 	 30//19
#define TH_FL	 	  8//4//19
#define TH_FR	 	 15//9//24
#define TH_R	  	 45//23

//センサ閾値(壁ありの閾値)
#define TH_Wall_L 	 30//19
#define TH_Wall_FL	  8//14//19
#define TH_Wall_FR	 15//19//24
#define TH_Wall_R	 45//23

//制御ONOFF用センサ閾値
#define THcton_L	 45//24//24柱でビクンビクンするので140611
#define THcton_FL	 1//36
#define THcton_FR	 1//48
#define THcton_R	 65//80//49//39
//制御用センサ値
#define THctrl_L	 77//80//56//60//63
#define THctrl_FL	 1//36
#define THctrl_FR	 1//48
#define THctrl_R	 104//120//112//134//71

141025土まで

/*141121金あさ　ここから

//141025土から、センサ曲げ直した
//センサ閾値(壁なしの閾値)
#define TH_L	 	 30//19
#define TH_FL	 	  8//4//19
#define TH_FR	 	 15//9//24
#define TH_R	  	 45//23

//センサ閾値(壁ありの閾値)
#define TH_Wall_L 	 30//19
#define TH_Wall_FL	 15// 8//14//19
#define TH_Wall_FR	 15//15//19//24
#define TH_Wall_R	 45//23

//制御ONOFF用センサ閾値
#define THcton_L	 30//45//24//24柱でビクンビクンするので140611
#define THcton_FL	 1//36
#define THcton_FR	 1//48
#define THcton_R	 45//65//80//49//39
//制御用センサ値
#define THctrl_L	 54//77//80//56//60//63
#define THctrl_FL	 120//1//36
#define THctrl_FR	 180//1//48
#define THctrl_R	 84//104//120//112//134//71

141121金の朝　ここまで
*/

//141025土から、センサ曲げ直した
//センサ閾値(壁なしの閾値)
#define TH_L	 	 39//35//30//20//30//19
#define TH_FL	 	 20//15//5//10// 8//4//19
#define TH_FR	 	 20//15//5//10//15//9//24
#define TH_R	  	 29//22//45//35//45//23

//センサ閾値(壁ありの閾値)
#define TH_Wall_L 	 30//20//30//19
#define TH_Wall_FL	 15// 8//14//19
#define TH_Wall_FR	 15//15//19//24
#define TH_Wall_R	 45//35//45//23

//制御ONOFF用センサ閾値
#define THcton_L	 39//30//45//24//24柱でビクンビクンするので140611
#define THcton_FL	 1//36
#define THcton_FR	 1//48
#define THcton_R	 29//45//65//80//49//39
//制御用センサ値
#define THctrl_L	 78//67//70//66//54//77//80//56//60//63
#define THctrl_FL	 120//1//36
#define THctrl_FR	 180//1//48
#define THctrl_R	 51//58//50//86//84//104//120//112//134//71



/*外乱除去(差分方式)前
//センサ値
#define TH_L	 20// 15// 45//300//テキトー
#define TH_FL	 25// 20//300
#define TH_FR	 25// 25//300
#define TH_R	 50//140//300

//制御ONOFF用センサ閾値
#define THcton_L	 40//30//50// 15// 45//300//テキトー
#define THcton_FL	 25// 20//300
#define THcton_FR	 25// 25//300
#define THcton_R	 80//60//80//140//300


//制御用センサ値
#define THctrl_L	 72//65//67//69//61//53// 15// 45//300//テキトー
#define THctrl_FL	 11//12// 20//300
#define THctrl_FR	 18//15// 25//300
#define THctrl_R	108//97//99//109//150//140//300
*/

#define KSINT_L		1
#define KSINT_FL	1
#define KSINT_FR	1
#define KSINT_R		1

#define defKP 1//0.5//1//2//0.1//1//3強い震える出だしがひどい//5強い//2.5//5強くて吸い込まれる
//0.5//0.1割りといいが遅い//こっからはD制御入ってたんで関係ない10でかい//0.02
#define KI 0//0.000020 //初期値0.000001　０が5つ
#define KD 1.5


/*マクロ関数*/
//モーター用マクロ関数
#define Motor_Riset()	PE.DRL.BIT.B2 = 1;wait(1);PE.DRL.BIT.B2=0
#define Motor_Enable()	PE.DRL.BIT.B3 = 1
#define Motor_Disable()	PE.DRL.BIT.B3 = 0
#define Motor_start()	MTU2.TSTR.BIT.CST0 = 1;MTU2.TSTR.BIT.CST1 = 1	
	//MTU2のch0,ch1カウンタスタート(モータPWM割り込みスタート)
#define Motor_stop()	MTU2.TSTR.BIT.CST0 = 0;MTU2.TSTR.BIT.CST1 = 0	
	//MTU2のch0,ch1カウンタストップ(モータPWM割り込みストップ)
	
#define Motor_forward()		PE.DRL.BIT.B0 = 1;PE.DRL.BIT.B4 = 0;Flag_Comb_Rot=1	//前回転
#define Motor_backward()	PE.DRL.BIT.B0 = 0;PE.DRL.BIT.B4 = 1;Flag_Comb_Rot=2	//後ろ回転
#define Motor_CW()			PE.DRL.BIT.B0 = 1;PE.DRL.BIT.B4 = 1;Flag_Comb_Rot=3	//時計回転
#define Motor_CCW()			PE.DRL.BIT.B0 = 0;PE.DRL.BIT.B4 = 0;Flag_Comb_Rot=4	//反時計回転


/*cmt*/
#define CMT_ON()	CMT.CMSTR.BIT.STR0 = 1//CMTカウントスタート
#define CMT_OFF()	CMT.CMSTR.BIT.STR0 = 0

/*ADC*/
#define ADC_ON()	AD1.ADCR.BIT.ADST = 1//ADC開始
#define ADC_OFF()	AD1.ADCR.BIT.ADST = 0//ADC停止

////////////////////////
//物理量ベース計算関係//
////////////////////////

//タイヤ
#define Dia_L 52.32//53mdki//49ngi//50ngi//51ngi//52.32//52.32おっけ！//52.3125//軸の4分の1くらい長い//52.28125マイナスでもないな。さっきのであってる//52.33375プラスじゃないな//52.3125//52.3125かなりいい感じ。あとはこれに多少±して、//52.375ちょい短い。この間だ！//52.25ちょい長い//52//51.5//49.25//49長い(７区間で半区間ながい)//49.5ちょい短い//50ちょい短い//48長い//56逆だ//53短い//52短い//51.5短い//長い141117月//52長い

//52//52.125短い？//52.1若干長い//52.15//ほぼおｋ1mくらい短い？//52柱半分弱長い
//55めっちゃ短くなったｗ//52.3柱半分だけ少ない//52.3ok//52.5短い//53１ｃｍ短い//(52ちょっと長い)//.7)//49.25//0.0//49.5			//[mm] 交差+0.5
#define Dia_R (Dia_L)//0.905)//49.0//50.0//49.5			//[mm] 交差+0.5

#define SPEEDtoTGRAbyDIAMETER	(49087)	//


#define rDegSlaR 1.00//0.9
#define rDegSlaL 1.05


//トレッド
#define tread 83//右回りは角速度90deg/sでピッタリ、左は最後すこし残る１５度くらいかな？。２度くらい多い
//83//R+1L-43//81R-47L-135////90R+360L+272//85R+42L+33//30//50//70多いな・・・？
//110多すぎ//80多い//85.01//多いR+50L+0//85.01R2-L5+//85.05R2-L5+//85.1R2-L5+//85.3R5+L8+
//85.5 R５L10度多い//85足りない//86多い//84足りない//81大きく足りない//83.05足りない(Dia52.3時)
//83.125↑//83.25↑//83↓//83.5↑//84↑//88↑//83↓//79.90//80.0//79.0//(89.75-7)//6.35	)//(89.75-7.0)
//[mm]誤差が多そう…orz タイヤをホイールに固定しないとなぁ。というかホイールを一回り大きくしたい
#define ROT00Ratio	0.20//スタート時に微小に左回りするための比率
#define ROT90Ratio	1//0.80//1//0.83*0.925//0.85多い//0.8少ない//0.88多い//90度回るための比率
#define ROT90LRatio 1//1.20//1//0.83//
#define ROT180Ratio 2.04//2.05チョビ多い2.01不足//2.20//2//1.85//*1.0375//*0.9625//180度回るための比率	



//ステッピングモータ
#define deg_step 0.9//1.8	//[deg] 1パルスで回る角度→200パルスで一回転360deg

#define dt 				0.001	//[sec]刻み時間　割り込み周期と合わせて1msecとした

//重心速度関係
#define Def_accel 			3000//3000//10//3000//3000//4000//2000//2000//5000//4000//3000//2000//1000//500	//[mm/s^2]加減速度
#define Def_decel			2700//3000//10//3000//3000//4000//2000//2000
#define Def_target_speed 	 500//450//500//500// 500//300//500//900//800//700//500//200//200	//[mm/s]目標並進速度
#define min_speed 		 	 100//50//100			//[mm/s]最低並進速度TGRA12149相当(？要検算)
#define DistanceFirstStep	10	//mm 加速しないで進む距離
//(141020、静止から加速時に右にズレる問題を対処するため、初めは速度を変えずに進む距離を設けた
//141027以降、右ズレ問題は、フレームの歪みにより、左右で摩擦が異なることが原因であることが判明したため、
//フレームを削りなおした。曲げに気をつける必要がある。

#define target_first_speed	 50//200
//#define target_distance→変数化 180//180	//[mm]目標距離
#define section_distance 180	//一区間分の距離
#define fist_distance (180+22)//27)	//ケツをつけた状態から半区画までの距離。90ではないけど暫定として。180-lr-t_wall/2

//角速度関係
#define Def_ang_accel			4500//3000//2500//1400.0//1400//	2000//	3000	//[deg/s^2]角加速度
#define Def_ang_decel			4500//3000//2500//1400.0//1400//	2000//	3000
#define Def_target_ang_speed	540//360//180//500//180//500//.0//	400//	500		//[deg/s]目標角速度
#define min_ang_speed			90//100//50.0//50//50//100//100		//[deg/s]最低角速度
//#define target_angle→変数化		(90)//(360*10)//(360*3)	//[deg]目標回転角 
#define right_angle 90 //直角の意味

/* スラローム関係 */
//小回り
#define Def_sla_speed				500//
#define Def_sla_forward_distance 	30//10//5//10//4.5//2.5//5//
//1.5//1//10//12.9	//前距離
#define Def_sla_backward_distance 	30//10//5//10//5//10.81192//
//4.38//1//5//10//13.2	//後ろ距離

#define Def_sla_angAccel			5471.344//4000//5471.344//5314.18//2000//2000	//deg/s^2
#define Def_sla_minangspeed			0

#define Def_slaRtargetangspeed	572.9578//400//572.9578//1000	//deg/s^2
#define Def_slaR_angle			81//82曲がりすぎ//80//84//82不足//87//88.57//88.625ちょい多い//88.825多い//88.75ズレ？//88.5少ない(88.5~89の間)//89若干多い//88不足//91まだまだ多い//92多い//93多め//95過多//90不足//89.75//89.75おｋ角加速度5314.18時//89明らかに足りてない
//90ほんの僅か曲がりすぎ//90.5わずか曲がりすぎ//h91わずかに曲がりすぎ//92曲がりすぎ//90たりてない//89///90		//deg	

#define Def_slaLtargetangspeed	572.9578//400//572.9578//1000	//deg/s^2
#define Def_slaL_angle			81//82曲がりすぎ//80//82//82よし//87//85.5//86多い//87多い//85足りなすぎ(85~88)//88多い//89曲がり過//88不足//91まだまだ多い//93過多//95過多//90不足//91.5//91.5おｋ角加速度5314.18時//91足りてない//92ちょい曲がりすぎ//89///90		//deg	


/* 迷路関係 */

#define North	0
#define West	1
#define South	2
#define East	3

enum {NORTH, WEST, SOUTH, EAST};

//マップサイズ
#define MAPSIZE_X	16
#define MAPSIZE_Y	16


//ゴールは何マスか？(1or4)
#define GOAL_NUM 4//4//4


//1マス時のゴールJK(条件)
//#define	goalCondition  (mapState.X == GOAL_X & mapState.Y == GOAL_Y)
//4マス時のゴールJK(条件)
#define	goalCondition	( (mapState.X == GOAL_XL & mapState.Y == GOAL_YL) \
						| (mapState.X == GOAL_XL & mapState.Y == GOAL_YH) \
						| (mapState.X == GOAL_XH & mapState.Y == GOAL_YL) \
						| (mapState.X == GOAL_XH & mapState.Y == GOAL_YH) )
	//\で#defineの改行

//1マスゴールの場合　(X,Y)
#define GOAL_X 1//4//1//2//3////1
#define GOAL_Y 0//7//0//1//11///0

//4マスゴールの場合　(XL,YL)から(XH,YH)まで
#define GOAL_XL 7//0//1//7//0//3//4//3
#define GOAL_YL 7//6//0//7//8//3//7//3
#define GOAL_XH (GOAL_XL+1)
#define GOAL_YH (GOAL_YL+1)

//スタート座標
#define START_X 0
#define START_Y 0

//スタートへ戻った条件
#define startCondition (mapState.X == START_X & mapState.Y == START_Y)

/* 最短関係 */
//パスのサイズ
#define PATH_SIZE	256//	今井ちゃんのオススメは256だが、俺は2倍も必要ないと思う。
//なぜなら、一つのパスで最低2マスの移動になる。1マスのみの移動はありえない。
//いや、超新地斜めをしようとすると、ありうるな。いやいや、N区間直進斜め135度なら、ただ、斜め135度での、走行関数を、超新地旋回と直進で掛けばいいだけ。	//
//ただし、128一杯まで使う場合、無限ループに陥る可能性があるので、ループの上限設定には気をつけうrこと
