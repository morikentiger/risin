/* testgame.c */
#include "testgame.h"

//グローバル変数
CharaData jikidata;
CharaData jitama[3];
char trigger = 0;
CharaData tekidata[3];
t_colour colour;



void testgame(void){
	long divider;
	unsigned short baud = 31250;//uOLEDは9600らしい。//38400;	//115200;//38400;			//ビットレート38400bps
	unsigned char tmp;

	
	unsigned char countuOLED = 0; 
	unsigned char lineuOLED = 0;
	unsigned char columnuOLED = 0;
	char str[100] = {"hogehoge"};
	char *ad_str;
//	t_colour colour;
	int i;
	int j;
	int i_max;
	int j_max;
	
	//乱数のシード値入力
	srand(1);
	
	jicharaInit();
	
	
	tekiInit();;
	
	init_uOLED();	//uOLED接続用シリアル通信SCI0を使う。
	wait(200);
	UOLED_RESET_N = 1;		//uOLEDのリセット
	//ピンの接続は右の通り マイコンのピン(→uOLEDのピン)と表示。(RX0(→TX),TX0(→RX),PA2(→RES)の設定)printf_uOLED.cにて
	wait(200);
	
/*デバッグ
	myprintf("testval=%d\n\r", *testval);
	*testval=20;
	myprintf("testval=%d\n\r", *testval);
	return;	
	*/
	
	//Set Graphics Parameters
/*	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xD8);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x12);
	put1byte_uOLED(0x14);
	put1byte_uOLED(0x00);
*/	
	//Outline Colour
/*	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x67);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x1F);
	wait(1000);
*/	
	
	//Change Colour
/*	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBE);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x00);
	setColour(&colour,0,0,31);
	put1byte_uOLED(colour.MSB);
	put1byte_uOLED(colour.LSB);
	wait(1000);
*/	
/*	lineuOLED = countuOLED%8;
	columnuOLED = countuOLED%13;
	moveCursorLine0_7Column0_12(lineuOLED,columnuOLED);
	wait(10);
*/						
	//Put String
	wait(10);
//	putString("hogehoge1");
	//ClipWindow
/*	i_max = 32;
	j_max = 32;
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x0A);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(i_max);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(j_max);
	
	for(j=0;j<j_max;j++){
		for(i=0;i<i_max;i++){
			put1byte_uOLED(0x00);
			put1byte_uOLED(0x1F);
		}
	}
*/
/*	put1byte_uOLED(0x00);
	put1byte_uOLED(0x1F);
	
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x1F);
	
	put1byte_uOLED(0x00);
	put1byte_uOLED(0xFF);
	
	put1byte_uOLED(0x1F);
	put1byte_uOLED(0x00);
*/	
//	wait(3000);


	clearScreen();
	
	//Put String
	wait(10);
	putString("hogehoge1");
	wait(2000);
	clearScreen();
	
	//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x01);//clipping off
	
	//ClipWindow
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBF);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(95);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(63);//y2(LSB)
	wait(10);
	
	//Put String
	wait(10);
	putString("foo bar1");
	
	wait(1000);
	
	i=0;
	while(i<30){
		//Draw Rectangle(矩形)
		setColour(&colour,0,63,0);
		drawRectangle(30-i, 30-i, 
		30+i, 30+i,
		&colour);
		wait(50);
		i=i+3;
	}
	
	wait(100);
	//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x00);//clipping off
	
	//Put String
	wait(10);
	putString("hogehoge1");
	wait(2000);
	
	//Extend Clip Region
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBC);
	wait(1000);
	
	clearScreen();
	//Extend Clip Region
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBC);
	wait(1000);
/*	
	//Extend Clip Region
//	put1byte_uOLED(0xFF);
//	put1byte_uOLED(0xBC);
//	wait(1000);
		
	
	//Put String
	wait(10);
	putString("hogehoge2");
	wait(1000);
	clearScreen();
		
	
	//Put String
	wait(10);
	putString("foo bar2");
	wait(1000);
	
	//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x01);//clipping on
	
	//Put String
	wait(10);
	putString("foo bar2-2");
	wait(1000);
	
	//ClipWindow
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBF);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(95);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(63);//y2(LSB)
	wait(10);
	
	//Put String
	wait(10);
	putString("foo bar2-3");
	wait(1000);
	
	
	
	
	wait(1000);
	
	i=0;
	while(i<30){
	
		
		//Draw Rectangle(矩形)
		setColour(&colour,0,0,31);
		drawRectangle(30-i, 30-i, 
		30+i, 30+i,
		&colour);
		wait(50);
		i=i+3;
		
	}
	//Extend Clip Region
//	put1byte_uOLED(0xFF);
//	put1byte_uOLED(0xBC);
//	wait(1000);
	
	//Put String
	wait(10);
	putString("hogehoge3");
	wait(1000);
	clearScreen();
		
	//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x00);//clipping off
	
	
	//Put String
	wait(10);
	putString("foo bar3");
	
	wait(1000);
	
	i=0;
	while(i<30){
	
		
		//Draw Rectangle(矩形)
		setColour(&colour,31,0,0);
		drawRectangle(30-i, 30-i, 
		30+i, 30+i,
		&colour);
		wait(50);
		i=i+3;
		
	}
	//Extend Clip Region
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBC);
	wait(1000);
		
	
	//Put String
	wait(10);
	putString("hogehoge4");
	wait(1000);
	clearScreen();
		
	
	//Put String
	wait(10);
	putString("foo bar4");
	wait(1000);
	
	//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x01);//clipping on
	
	//ClipWindow
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBF);
	put1byte_uOLED(0x00);
	put1byte_uOLED(28);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(28);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(100);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(100);//y2(LSB)
	wait(10);
	
	
	wait(1000);
	
	i=0;
	while(i<30){
	
		
		//Draw Rectangle(矩形)
		setColour(&colour,31,0,0);
		drawRectangle(30-i, 30-i, 
		30+i, 30+i,
		&colour);
		wait(50);
		i=i+3;
		
	}
	//Extend Clip Region
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBC);
	wait(1000);
	
	//Put String
	wait(10);
	putString("hogehoge5");
	wait(1000);
	clearScreen();
		
	
	//Put String
	wait(10);
	putString("foo bar5");
	wait(1000);
		
	//Extend Clip Region
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBC);
	
	wait(3000);
	
	//Put String
	wait(10);
	putString("hogehoge6");
	wait(1000);
	clearScreen();
		
	
	//Put String
	wait(10);
	putString("foo bar6");
	wait(1000);
*/	
	while(1)
	{	
		
		countuOLED++;
		
		//左右移動入力処理
		if((SWL==0)){//SW1!=0&SW2==0){//←SWON
			if(jikidata.vx<=0)jikidata.vx-=jikidata.ax;	//←速度
			if(jikidata.vx>0)jikidata.vx-=jikidata.ax*2;	//ブレーキは減速度２倍
			if(jikidata.vx<-jikidata.vxmax) jikidata.vx=-jikidata.vxmax;	//最大速度
		}
		if((SWR==0)){//→SWON
			if(jikidata.vx>=0)jikidata.vx+=jikidata.ax;
			if(jikidata.vx<0)jikidata.vx+=jikidata.ax*2;
			if(jikidata.vx>jikidata.vxmax)jikidata.vx=jikidata.vxmax;
		}
		if(SWL!=0&SWR!=0){
			if(jikidata.vx>0){
				jikidata.vx-=jikidata.ax;
				if(jikidata.vx<0)jikidata.vx=0;
			}
			if(jikidata.vx<0){
				jikidata.vx+=jikidata.ax;
				if(jikidata.vx>0)jikidata.vx=0;
			}
		}			
	
		//ジャンプ処理
		if((jikidata.jumponce!=1)&&(jikidata.ground==1&jikidata.jump==0)&&(SWW==0)){
			jikidata.jumponce=1;
			jikidata.ground=0;//空中
			jikidata.jump=1;	
			jikidata.vy = jikidata.vymax;				
		}
		if(jikidata.jump==1)
		{
			jikidata.vy -=jikidata.ay;
		}
		if(SWW!=0)jikidata.jumponce=0;
		
		//自キャラ移動
		jikidata.x += jikidata.vx;
		jikidata.y += jikidata.vy;
		if(jikidata.y<=0)
		{
			jikidata.jump=0;
			jikidata.ground=1;//地面
			jikidata.vy=0;
			jikidata.y=0;
		}
		//Clear Screen
		clearScreen();
		
		//Extend Clip Region
//	put1byte_uOLED(0xFF);
//	put1byte_uOLED(0xBC);
		
		//Clipping
//		put1byte_uOLED(0xFF);
//		put1byte_uOLED(0x6C);
//		put1byte_uOLED(0x00);
///		put1byte_uOLED(0x01);//clipping on
	
		//ClipWindow
/*		put1byte_uOLED(0xFF);
		put1byte_uOLED(0xBF);
		put1byte_uOLED(0x00);
		put1byte_uOLED(20);//x1(LSB)
		put1byte_uOLED(0x00);
		put1byte_uOLED(0);//y1(LSB)
		put1byte_uOLED(0x00);
		put1byte_uOLED(40);//x2(LSB)
		put1byte_uOLED(0x00);
		put1byte_uOLED(63);//y2(LSB)
*/	
		
/*		//Clipping
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0x6C);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0x00);//clipping off
	
	//ClipWindow
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xBF);
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(0);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(95);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(63);//y2(LSB)
//	wait(10);
*/	

	
		
		JitamaMove();
		TekiMove();
		if(AtariHantei()==1) break;
		
		//Draw Line
		setColour(&colour,0,32,16);
		drawLine(0x01, 0x3F, 
		0x5F, 0x3F,
		&colour);
		
		//Draw Rectangle(矩形)
		setColour(&colour,31,0,0);
		drawRectangle(10+jikidata.x, 63-jikidata.y, 
		10+jikidata.image_w+jikidata.x, 63-jikidata.image_h-jikidata.y,
		&colour);
		
	//	put1byte_uOLED(0xFF);
	//	put1byte_uOLED(0x6C);
	//	put1byte_uOLED(0x00);
	//	put1byte_uOLED(0x00);//clipping off
	
		//Extend Clip Region
//		put1byte_uOLED(0xFF);
//		put1byte_uOLED(0xBC);
	
		
		//Extend Clip Region
//	put1byte_uOLED(0xFF);
//	put1byte_uOLED(0xBC);
//	wait(1000);
		//待ち時間
		wait(10);
	
	}
	
}


char IsAtari(CharaData a, CharaData b){
	char retval = 0;
	char ax1 = a.x + (a.image_w - a.bounds_w)/2;
	char ay1 = a.y + (a.image_h - a.bounds_h)/2;
	char ax2 = a.x + (a.image_w + a.bounds_w)/2;
	char ay2 = a.y + (a.image_h + a.bounds_h)/2;
	char bx1 = b.x + (b.image_w - b.bounds_w)/2;
	char by1 = b.y + (b.image_h - b.bounds_h)/2;
	char bx2 = b.x + (b.image_w + b.bounds_w)/2;
	char by2 = b.y + (b.image_h + b.bounds_h)/2;
	
	if( (ax1<bx2) && (bx1<ax2) && (ay1<by2) && (by1<ay2) ){
		retval = 1;
	}
	return(retval);
}

char AtariHantei(void){
	char i,j;
	for(i=0;i<3;i++){
		if(tekidata[i].life>0){
			//自機と敵の判定
			if(IsAtari(jikidata, tekidata[i])==1) return(1);
			//弾と敵の判定
			for(j=0;j<3;j++){
				if(jitama[j].life>0){
					if(IsAtari(jitama[j], tekidata[i])==1){
						jitama[j].life = 0;
						tekidata[i].life = 0;
					}
				}
			}
		}
	}
	return(0);
}
