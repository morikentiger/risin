/* myprintf_SCI2.c */
#include <stdio.h>
#include <stdarg.h>
#include "iodefine.h"

void init_sci2(void){

	long divider;
	unsigned short baud = 38400;	//115200;//38400;			//ビットレート38400bps
	unsigned char tmp;

	STB.CR3.BIT._SCI2=0;		// STANDBY解除
	PFC.PACRL3.BIT.PA8MD=1;		//シリアルポートを設定
	PFC.PACRL3.BIT.PA9MD = 1;	//シリアルポートを設定
	SCI2.SCSCR.BYTE=0x00;		//送受信割り込み禁止
	
	//ビットレート関連の計算
	divider = 32;
	if ( baud < 300 ) {
		tmp = 0x03;
		divider = divider << 6;
	} else
	if ( baud < 1200 ) {
		tmp = 0x02;
		divider = divider << 4;
	} else
	if ( baud < 4800 ) {
		tmp = 0x01;
		divider = divider << 2;
	} else {
		tmp = 0x00;
	}
	
	SCI2.SCSMR.BYTE = tmp;		/* ASYNC、8bit、Parity-NONE、Stop-1、Clk = tmp	*/
	tmp = (unsigned char)(25000000/divider/baud)-1;
	SCI2.SCBRR = tmp;			//ビットレート設定
	SCI2.SCSCR.BIT.TE=1;		//送信許可
	SCI2.SCSCR.BIT.RE=1;		//受信許可
}
void put1byte_SCI2(char c) {
	while ( SCI2.SCSSR.BIT.TDRE == 0 ) ;
	SCI2.SCSSR.BIT.TDRE = 0;
	SCI2.SCTDR = c;
}

void putnbyte_SCI2(char *buf,int len) {
	int c;
   
    for(c = 0; c < len; c++){
		put1byte_SCI2(buf[c]);
	}           
}

int myprintf_SCI2(const char *fmt, ...){
	static char buffer[100];
	int len;
	
	va_list ap;
	va_start(ap, fmt);
	
	len = vsprintf(buffer, fmt, ap);
	putnbyte_SCI2(buffer, len);
	va_end(ap);
	return len;
}
