/* jichara.c */

#include "testgame.h"

void jicharaInit(void){
	//自機の初期設定
	jikidata.x=0;
	jikidata.y=0;
	jikidata.vx=0;
	jikidata.vxmax=10;
	jikidata.vy=0;
	jikidata.vymax=12;
	jikidata.ax=2;
	jikidata.ay=3;
	jikidata.image_w=5;
	jikidata.image_h=10;
	jikidata.bounds_w=3;
	jikidata.bounds_h=8;
	jikidata.jump=0;//ジャンプし始めに1
	jikidata.jumponce=0;
	jikidata.ground=1;//床にいると1
	return;
}
void JitamaMove(void){
	char i=0;
	//弾の発射
	if( (SWG==0) && (trigger==0) ){
		for(i=0; i<3; i++){
			if(jitama[i].life == 0){
				jitama[i].life = 1;
				jitama[i].image_w=6;
				jitama[i].image_h=2;
				jitama[i].bounds_w=6;
				jitama[i].bounds_h=2;
				jitama[i].x = jikidata.x + jikidata.image_w + 2;//jitama[i].image_w;
				jitama[i].y = jikidata.y + jikidata.image_h - 6;
				break;
			}
		}
		trigger = 3;
	}
	if(trigger>0) trigger = trigger - 1;
	//弾の移動
	for(i=0; i<3; i++){
		if(jitama[i].life > 0){
			jitama[i].x = jitama[i].x + jitama[i].image_w;
			//Draw Line
		//	wait(10);
			put1byte_uOLED(0xFF);
			put1byte_uOLED(0xCF);//cmd
			put1byte_uOLED(0x00);
			put1byte_uOLED(10+jitama[i].x);//x1(LSB)
			put1byte_uOLED(0x00);
			put1byte_uOLED(63-jitama[i].y);//y1(LSB)
			put1byte_uOLED(0x00);
			put1byte_uOLED(10+jitama[i].image_w+jitama[i].x);//x2(LSB)
			put1byte_uOLED(0x00);
			put1byte_uOLED(63-jitama[i].image_h-jitama[i].y);//y2(LSB)
			setColour(&colour,0,0,16);
			put1byte_uOLED(colour.MSB);
			put1byte_uOLED(colour.LSB);//colour　青暗い
			wait(2);
			//画面外に出た時の処理
			if(jitama[i].x>96) jitama[i].life = 0;
		}
	}
}