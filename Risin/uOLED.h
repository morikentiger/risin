/* uOLED.h */
typedef struct {
	unsigned char	RGB[3];	//RGB 最大値31,63,31
	unsigned char	MSB;	//送信時のMSB
	unsigned char	LSB;	//送信時のLSB
}t_colour;
extern void setColour(t_colour *Colour, char r, char g, char b);
extern void clearScreen(void);
extern void moveCursorLine0_7Column0_12(unsigned char line, unsigned char column);
extern void putString(char *string, ...);
extern void drawRectangle(char x1, char y1, char x2, char y2, t_colour *colour);


/*
extern void put1byte_SCI2(char c); 
extern void putnbyte_SCI2(char *buf,int len);
extern int myprintf_SCI2(const char *fmt, ...);

*/