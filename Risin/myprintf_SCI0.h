/* myprintf_SCI0.h */
extern void init_sci0(void);
extern void put1byte_SCI0(char c); 
extern void putnbyte_SCI0(char *buf,int len);
extern int myprintf_SCI0(const char *fmt, ...);

