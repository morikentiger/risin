/* run.c */


void Forward(float r_dis){
	
	distance = 0.0;
	Mode_Motion = 1;//前後加減速
	Mode_accel = 0;//まずは加速から
	speed = min_speed;//0.0;//止まってるので速度ゼロ？
		//現在速度が存在するならゼロにする必要ないのでは？
	Motor_forward();//前回転
	Motor_start();
	while(distance <= target_distance*r_dis);//cmtで1msec毎にdistanceを計算して180mmになるまでモータを動かす
	LED0=0;
	LED1=0;
	LED2=0;
	LED3=0;
	Motor_stop();
		//speed = 0.0;//止まったので速度ゼロ
		//最低速度を設定すればゼロでなくてもいいのでは？
		//例min_speedとか？
	Mode_Motion = 0;//動作なし(CMT計算終了)
		
}

void RotRight(float r_ang){
	//LED4=1;
	angle = 0.0;
	Mode_Motion = 2;//超信地旋回加減速
	Mode_accel = 5;	//角加速から
	ang_speed = min_ang_speed;
	Motor_CW();//Right_rotation
	Motor_start();
	while(angle <= target_angle*r_ang);
	Motor_stop();
	Mode_Motion = 0;//動作なし(CMT計算終了)
	//LED4=0;
	//LED2 =0;

	
}

void RotLeft(float r_ang){
	angle = 0.0;
	Mode_Motion = 2;//超信地旋回加減速
	Mode_accel = 5;	//加速
	ang_speed = min_ang_speed;
	Motor_CCW();//LEFT_Rotation
	Motor_start();
	while(angle <= target_angle*r_ang);
	Motor_stop();
	Mode_Motion = 0;//動作なし(CMT計算終了)
	
}
