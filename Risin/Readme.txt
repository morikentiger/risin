-------- PROJECT GENERATOR --------
PROJECT NAME :	Risin
PROJECT DIRECTORY :	C:\WorkSpace\Risin\Risin
CPU SERIES :	SH-2
CPU TYPE :	SH7125
TOOLCHAIN NAME :	Renesas SuperH RISC engine Standard Toolchain
TOOLCHAIN VERSION :	9.3.0.0
GENERATION FILES :
    C:\WorkSpace\Risin\Risin\dbsct.c
        Setting of B,R Section
    C:\WorkSpace\Risin\Risin\typedefine.h
        Aliases of Integer Type
    C:\WorkSpace\Risin\Risin\sbrk.c
        Program of sbrk
    C:\WorkSpace\Risin\Risin\iodefine.h
        Definition of I/O Register
    C:\WorkSpace\Risin\Risin\intprg.c
        Interrupt Program
    C:\WorkSpace\Risin\Risin\vecttbl.c
        Initialize of Vector Table
    C:\WorkSpace\Risin\Risin\vect.h
        Definition of Vector
    C:\WorkSpace\Risin\Risin\resetprg.c
        Reset Program
    C:\WorkSpace\Risin\Risin\Risin.c
        Main Program
    C:\WorkSpace\Risin\Risin\sbrk.h
        Header file of sbrk file
    C:\WorkSpace\Risin\Risin\stacksct.h
        Setting of Stack area
START ADDRESS OF SECTION :
 H'000000000	DVECTTBL,DINTTBL,PIntPRG
 H'000000800	PResetPRG
 H'000001000	P,C,C$BSEC,C$DSEC,D
 H'0FFFFA000	B,R
 H'0FFFFBC00	S

* When the user program is executed,
* the interrupt mask has been masked.
* 
* Program start H'1000.
* RAM start H'FFFFA000.

DATE & TIME : 2013/11/13 18:29:46
