/* speaker2.c */
//スピーカーのテストなど
//swで音を変化させるテスト

#include"iodefine.h"
#include"mydefine.h"

void drv_bz(int, int);                            //ブザードライバ
 
//ブザーMTUポート
#define MTU_BZ               MTU2.TSTR.BIT.CST2   //ブザー用MTU
//ブザーMTU発振数
#define MTU_BZ_CLOCK        24000000              //ブザー用MTUの発振数（24MHz）
//ブザーMTUレジスタ
#define MTU_BZ_CYCLE        MTU22.TGRB            //ブザー周期を設定
#define MTU_BZ_PULSWIDTH    MTU22.TGRA            //ブザーパルス幅を設定



//グローバル変数宣言
volatile unsigned long cntcmt=0;	//48day(永い)までカウントできる(周期1msecなら)


//CMT割り込み初期設定
//resetprg.cの21行目を右のようにする #define SR_Init    0x00000000
void init_cmt0(void)
{
	//動作設定
    STB.CR4.BIT._CMT = 0;	//CMTスタンバイ解除(クロック供給開始)
    CMT.CMSTR.BIT.STR0 = 0;    //設定のためカウント停止
	
	CMT0.CMCSR.BIT.CMF = 0;      // CMCNTとCMCORの値が一致したか否かを示す  0:不一致, 1:一致
    CMT0.CMCSR.BIT.CMIE = 1;   // 1:コンペアマッチ割り込み許可(イネーブル)
    CMT0.CMCSR.BIT.CKS = 0;    // 0:クロックセレクトPφ/8	1sec=3,125,000 10msec=31,250 1msec=3125
	INTC.IPRJ.BIT._CMT0 = 13;	//割り込み優先度15

	//カウント周期設定
	CMT0.CMCOR = 3125; //Pφ/8で1msec
}


//WAIT関数 引数msecだけ待つ
void wait_ms(int time_msec){	//引数は、待ち時間(msec)
	
	unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + time_msec;	//現在のカウントに、これから待つだけの時間を加える
	
	while(wait_end != cntcmt){}	//cntcmtがwait_endでない場合ループする(＝待ち時間分だけ待つ)
	
}



//CMT割り込み 周期1msec　ここがメインの処理
void interrpt_cmt0(void){
		
	CMT0.CMCSR.BIT.CMF = 0;	//検知フラグを戻してカウント再開
	
	cntcmt++;	//グローバル変数 48dayまでカウントできる
	
	
}



//MTU2割り込み初期設定(スピーカー)
//intprgの以下のように書き換える
// 105 MTU2_2 TGIB2
/*
void interrupt_speaker(void);
#pragma interrupt INT_MTU2_2_TGIB2
void INT_MTU2_2_TGIB2(void){
	interrupt_speaker();
}
*/

void init_mtu_speaker(void){
	//動作選択
	PFC.PECRL2.BIT.PE7MD = 1;		//TIOC2をMTU用端子に設定
	PFC.PEIORL.BIT.B7 = 1;			//TIOC2を出力端子に設定
	STB.CR4.BIT._MTU2 = 0;			//MTUスタンバイ解除(クロック供給開始)
	MTU2.TSTR.BYTE = 0; 			//カウンタ動作停止
	MTU22.TIER.BIT.TGIEB = 1;		//TGRBコンペアマッチによる割り込みを許可
	INTC.IPRD.BIT._MTU20G = 14;		//割り込み優先度を高めに設定　14


	//カウンタクロックの選択
	MTU22.TCR.BIT.TPSC = 0;//2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us):0.64usecでない
	
	//カウンタクリア要因の選択
	MTU22.TCR.BIT.CCLR = 2;//1; 		// TGRAでなくBコンペアマッチでTCNTクリア
	
	//アウトプットコンペアレジスタの選択(コンペアマッチ前後での波形出力レベルの設定0/1)
	MTU22.TIOR.BIT.IOA = 2;//1; 		// 初期状態:0 コンペアマッチ:0->1
	MTU22.TIOR.BIT.IOB = 1;//5; 		// 初期状態:1->0 コンペアマッチ:0
	
	//周期・デューティー設定
	MTU22.TGRA = 6000;//3551;				//PWM周期 10msec=15625cnt 1msec=1563cnt 
			//今は適当な値(ドだったか？)
	MTU22.TGRB = 12000;//MTU22.TGRA/2;		//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？ 
			//現在はTGRAの半分、つまり、TGRAが周期である矩形波を与えていることになっている→メインで値を変更し、音階が変わるようになっている
	
	//PWMモードの設定
	MTU22.TMDR.BIT.MD = 2;//3; 			// PWM2でなく1モード
	
}




//MTU2割り込み関数(この関数で、左右のモータのデューティをセットする
void interrupt_speaker(void){
	
	int cntmtu = 0;
	
//	LED0 = 0;
//	LED1 = 0;
	
	MTU22.TSR.BIT.TGFB = 0;	//コンペアマッチ発生フラグクリア
	//MTU20.TGRB = speed_r;	//右モータPWMのデューティー





}


/************************************************/
/*  周波数、時間（ms）を指定してブザーを鳴らす  */
/*----------------------------------------------*/
/*  IN :int   i_freq   … 周波数                */
/*      int   i_ms     … 時間（ms）            */
/************************************************/
void drv_bz(int i_freq, int i_ms)
{
    //ブザードライバ
    float cycle;
    float pulsWidth;
    //
    if (i_freq == 0)
    {
        //Wait
        wait_ms(i_ms);
    }
    else
    {
        //周波数を設定
        cycle = (float)MTU_BZ_CLOCK / (float)i_freq;
        pulsWidth = cycle /2.0;
        MTU_BZ_CYCLE = (short)cycle;
        MTU_BZ_PULSWIDTH = (short)pulsWidth;
        //発振
        MTU_BZ = BZ_ON;            //ブザーの発振を開始
        wait_ms(i_ms);         //指定時間 待つ
        MTU_BZ = BZ_OFF;           //ブザーの発振を終了
    }
}

void ctrl_bz(int i_freq, int i_ms)
{
    //ブザー処理
    drv_bz(i_freq, i_ms);             //鳴らす
}

/************************************************/
/*    IN :int i_freq  … 周波数                 */
/*        int i_beat  … 拍数（四分音符＝2）    */
/*        int i_tempo … テンポ（1分間の拍数）  */
/************************************************/
void ctrl_bz_melody(int i_freq, int i_beat, int i_tempo)
{
    //ブザー処理(メロディ)
    int ms = (i_beat * 60 * 1000 / 2) / i_tempo;
    //
    drv_bz(i_freq, (int)(ms * 0.9));        //鳴らす
    drv_bz(BZ_FREQ_REST, (int)(ms * 0.1));  //間隔を空ける
}

/*
void interrupt_motor_right(void){
	// 入れないと怒られるので入れとく
}
*/


//スピーカーのテストなど
void speaker2(void){
	
	//レベルアップ音
    int tempo = 200;
	
	int i;
	int i_mode = 0;
	
	int    freq[15] = {BZ_FREQ_DO5
                   ,BZ_FREQ_RE5
                   ,BZ_FREQ_MI5
                   ,BZ_FREQ_FA5
                   ,BZ_FREQ_SO5
                   ,BZ_FREQ_LA5
                   ,BZ_FREQ_SI5
                   ,BZ_FREQ_DO6
                   ,BZ_FREQ_RE6
                   ,BZ_FREQ_MI6
                   ,BZ_FREQ_FA6
                   ,BZ_FREQ_SO6
                   ,BZ_FREQ_LA6
                   ,BZ_FREQ_SI6
                   ,BZ_FREQ_DO7
                  };
	
	
	init_cmt0(); //CMT初期設定
	
	init_mtu_speaker();	//MTU2初期設定
	
	CMT.CMSTR.BIT.STR0 = 1;	//CMTカウントスタート(A/D変換)
	
	MTU2.TSTR.BIT.CST2 = 1;	//MTU2カウンタスタート(モータPWM割り込みスタート)
	
	
	
    //
    ctrl_bz_melody(BZ_FREQ_SO5,1, tempo);
    ctrl_bz_melody(BZ_FREQ_SO5,1, tempo);
    ctrl_bz_melody(BZ_FREQ_SO5,1, tempo);
    ctrl_bz_melody(BZ_FREQ_FA5,1, tempo);
    ctrl_bz_melody(BZ_FREQ_REST,1, tempo);
    ctrl_bz_melody(BZ_FREQ_LA5,1, tempo);
    ctrl_bz_melody(BZ_FREQ_SO5,4, tempo);
	

	//無限ループ
	while(1){
		
		//ポタン
		if(PE.DRL.BIT.B10 == 0){
		//	MTU22.TGRA += 10;
			if(i_mode<15){
				i_mode++;
			}else{
				i_mode=15;
			}
		
			wait_ms(10);
		}
		
		if(PE.DRL.BIT.B11 == 0){
		//	MTU22.TGRA -= 10;
			if(i_mode>0){
				i_mode--;
			}else{
				i_mode=0;
			}
			wait_ms(10);			
			
		}
	
		ctrl_bz(freq[i_mode-1], 100);        //ブザー鳴動
		
	}
	
		
}
