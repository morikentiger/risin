/* motor.c */
//とりあえずモーター回す

#include"iodefine.h"
#include"mydefine.h"


//グローバル変数宣言

//unsigned long cntcmt=0;	//48day(永い)までカウントできる(周期1msecなら)


//MTU2割り込み初期設定(左右のモータPWM値)
void init_mtu_motor(void){
	//動作選択
	PFC.PECRL1.BIT.PE1MD = 1;		//TOOC0BをMTU用端子に設定
	PFC.PECRL2.BIT.PE5MD = 1;		//TIOC1BをMTU用端子に設定
	PFC.PEIORL.BIT.B1 = 1;			//TIOC0Bを出力端子に設定
	PFC.PEIORL.BIT.B5 = 1;			//TIOC1Bを出力端子に設定
	STB.CR4.BIT._MTU2 = 0;			//MTUスタンバイ解除(クロック供給開始)
	MTU2.TSTR.BYTE = 0; 			//カウンタ動作停止
	MTU20.TIER.BIT.TGIEB = 1;		//ch0のTGRBコンペアマッチによる割り込みを許可
	MTU21.TIER.BIT.TGIEB = 1;		//ch2のTGRBコンペアマッチによる割り込みを許可
	INTC.IPRD.BIT._MTU20G = 14;	//割り込み優先度を高めに設定　14
	INTC.IPRD.BIT._MTU21G = 13;	//割り込み優先度を高めに設定　13

	//カウンタクロックの選択
	MTU20.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us) :0.64us
	MTU21.TCR.BIT.TPSC = 2; 		// プリスケーラ MPφ/16 (MPφ=25MHz->0.04us) :0.64us
	
	//カウンタクリア要因の選択
	MTU20.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	MTU21.TCR.BIT.CCLR = 1; 		// TGRAコンペアマッチでTCNTクリア
	
	//アウトプットコンペアレジスタの選択(コンペアマッチ前後での波形出力レベルの設定0/1)
	MTU20.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU20.TIOR.BIT.IOB = 5; 		// 初期状態:1 コンペアマッチ:0
	MTU21.TIOR.BIT.IOA = 1; 		// 初期状態:0 コンペアマッチ:0
	MTU21.TIOR.BIT.IOB = 5; 		// 初期状態:1 コンペアマッチ:0
	
	//周期・デューティー設定
	MTU20.TGRA = 6000;//156;				//PWM周期 10msec=15625cnt 1msec=1563cnt
	MTU20.TGRB = 100;//MTU20.TGRA;				//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？
	MTU21.TGRA = 6000;//156;				//PWM周期 10msec=15625cnt 1msec=1563cnt
	MTU21.TGRB = 100;//MTU20.TGRA;				//デューティー てきとーな値※TGRA超えないこと！カウントが終わりません？
	
	//PWMモードの設定
	MTU20.TMDR.BIT.MD = 3;			// PWM2モード
	MTU21.TMDR.BIT.MD = 3; 			// PWM2モード
}



//MTU2割り込み関数(この関数で、左右のモータのデューティをセットする
void interrupt_motor_right(void){
	
	int cntmtu = 0;
	
//	LED0 = 0;
//	LED1 = 0;
	
	MTU20.TSR.BIT.TGFB = 0;	//コンペアマッチ発生フラグクリア
	MTU21.TSR.BIT.TGFB = 0;
	MTU20.TGRA = 1500;//300;	//右モータのパルス幅
	MTU21.TGRA = 1500;	//左モータのパルス幅

		cntmtu++;
	if(cntmtu<500){
		LED0 = 1;
	}else if(cntmtu<1000){
		LED0 = 0;
	}else cntmtu = 0;

		


}




/*140601Sun serialADCでCMT割り込みするために一時的にコメントアウト

//CMT割り込み初期設定
//resetprg.cの21行目を右のようにする #define SR_Init    0x00000000
void init_cmt0(void)
{
	//動作設定
    STB.CR4.BIT._CMT = 0;	//CMTスタンバイ解除(クロック供給開始)
    CMT.CMSTR.BIT.STR0 = 0;    //設定のためカウント停止
	
	CMT0.CMCSR.BIT.CMF = 0;      // CMCNTとCMCORの値が一致したか否かを示す  0:不一致, 1:一致
    CMT0.CMCSR.BIT.CMIE = 1;   // 1:コンペアマッチ割り込み許可(イネーブル)
    CMT0.CMCSR.BIT.CKS = 0;    // 0:クロックセレクトPφ/8	1sec=3,125,000 10msec=31,250 1msec=3125
	INTC.IPRJ.BIT._CMT0 = 13;	//割り込み優先度15

	//カウント周期設定
	CMT0.CMCOR = 3125; //Pφ/8で1msec
}


//WAIT関数 引数msecだけ待つ
void wait(int time_msec){	//引数は、待ち時間(msec)
	
	unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + time_msec;	//
	
	while(wait_end != cntcmt){}	//cntcmtがwait_endでない場合ループする(＝待ち時間分だけ待つ)
	
}



//CMT割り込み 周期1msec　ここがメインの処理
void interrpt_cmt0(void){
		
	CMT0.CMCSR.BIT.CMF = 0;	//検知フラグを戻してカウント再開
	
	cntcmt++;	//グローバル変数 48dayまでカウントできる
	
	
}

*///ここまで140601Sun serialADCでCMT割り込みするために一時的にコメントアウト

void motor(void){
	
	long i;
	
	PFC.PEIORL.WORD = 0xffff;		//PEすべて出力
	//PFC.PEIORL.BIT.B3 = 1;			//MotorEnable端子を出力に
	
	
	init_mtu_motor();	//MTU2初期設定

/*140601Sun serialADCでCMT割り込みするために一時的にコメントアウト	
	init_cmt0();
	
	CMT.CMSTR.BIT.STR0 = 1;	//CMTカウントスタート(A/D変換)
*/	
	
	
	//PE.DRL.BIT.B2 = 1;	//RESETだが、電源投入時にリセットが起こってるので、必要ない。
	
	//PE.DRL.BIT.B2 = 0;
	
	PE.DRL.BIT.B3 = 1;	//MotorEnableをHiに。
	
	PE.DRL.BIT.B0 = 1;	//CW/CCW
	PE.DRL.BIT.B4 = 1;//0;
	
	
	
	
	wait(1000);	//7073のデータシート(14/39)を参照。100μs以上待つ必要ある。
	
	//Motor_start();//左右のモーターが動き始める（Ch0,ch1カウントスタート)
			
	while(1){

//ここなんだっけ？140526Mon数ヶ月ぶりのブランクによる記憶消去が原因		
/*		PE.DRL.BIT.B1 = 1;
		PE.DRL.BIT.B5 = 1;
		for(i=0;i<10000;i++){}
		
		PE.DRL.BIT.B1 = 0;
		PE.DRL.BIT.B5 = 0;
		for(i=0;i<10000;i++){}
*/

		
			
		
	}
	
	
	
}
