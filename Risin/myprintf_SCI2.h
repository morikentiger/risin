/* myprintf_SCI2.h */
extern void init_sci2(void);
extern void put1byte_SCI2(char c); 
extern void putnbyte_SCI2(char *buf,int len);
extern int myprintf_SCI2(const char *fmt, ...);

