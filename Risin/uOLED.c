/* uOLED.c */
#include <stdarg.h>
#include "printf_uOLED.h"
#include "uOLED.h"


/*
typedef struct {
	t_colour colour;
//	unsigned char 	Wall[MAPSIZE_X][MAPSIZE_Y];	//ĉÔ²ĈÌÇîñ(âÎÀW)
//	unsigned char	X;				//ğŬÀWX
//	unsigned char	Y;				//ğŬÀWY
//	int				Psi;			//ğŬp¨Ġ
}t_uOLED;
*/
//t_uOLED uOLED;


void setColour(t_colour *Colour, char r, char g, char b)
{
	Colour->RGB[0] = r & 0x1F;
	Colour->RGB[1] = g & 0x3F;
	Colour->RGB[2] = b & 0x1F;
	
	Colour->MSB = (Colour->RGB[0] << 3 | ((Colour->RGB[1]) & 0x07));
	Colour->LSB = (Colour->RGB[1] << 5 | ((Colour->RGB[2]) & 0x1F));
}

void clearScreen(void)
{
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xD7);
}

void moveCursorLine0_7Column0_12(unsigned char line, unsigned char column)
{
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xE4);
	put1byte_uOLED(0x00);
	put1byte_uOLED(line);
	put1byte_uOLED(0x00);
	put1byte_uOLED(column);
}

void putString(char *string, ...)
{
/*	static char buffer[100];
	int len;
	va_list ap;
	va_start(ap, string);
	vsprintf(buffer, string, ap);
*/	put1byte_uOLED(0x00);
	put1byte_uOLED(0x06);
	printf_uOLED("%s\n",string);
	put1byte_uOLED(0x00);
//	va_end(ap);
}

void drawLine(char x1, char y1, char x2, char y2, t_colour *colour){
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xD2);//cmd
	put1byte_uOLED(0x00);
	put1byte_uOLED(x1);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(y1);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(x2);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(y2);//y2(LSB)
	put1byte_uOLED(colour->MSB);//0xF8);
	put1byte_uOLED(colour->LSB);//0x00);//colour Ô
	wait(2);
	return;
	
}

void drawRectangle(char x1, char y1, char x2, char y2, t_colour *colour){
	put1byte_uOLED(0xFF);
	put1byte_uOLED(0xCF);//cmd
	put1byte_uOLED(0x00);
	put1byte_uOLED(x1);//x1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(y1);//y1(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(x2);//x2(LSB)
	put1byte_uOLED(0x00);
	put1byte_uOLED(y2);//y2(LSB)
	put1byte_uOLED(colour->MSB);//0xF8);
	put1byte_uOLED(colour->LSB);//0x00);//colour Ô
	wait(2);
	return;
}
		


/*		//Move Cursor
						wait(10);
						lineuOLED = countuOLED%8;
						columnuOLED = countuOLED%13;
						moveCursorLine0_7Column0_12(lineuOLED,columnuOLED);
						wait(10);
						
						//Put String
						wait(10);
						putString("hogehoge");
						wait(10);
						
						//Text Foreground Colour
						wait(10);
						put1byte_uOLED(0xFF);
						put1byte_uOLED(0x7F);
						setColour(colour,0,0,0x1F);
						put1byte_uOLED(colour.MSB);
						put1byte_uOLED(colour.LSB);
						wait(10);
						
					
						
						//Draw Circle
						wait(10);
						put1byte_uOLED(0xFF);
						put1byte_uOLED(0xCD);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x1E);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x1E);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x14);
						put1byte_uOLED(0x80);
						put1byte_uOLED(0x10);//F
						wait(10);
						
						//Draw Filled Circle
						wait(10);
						put1byte_uOLED(0xFF);
						put1byte_uOLED(0xCC);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x2D);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x28);
						put1byte_uOLED(0x00);
						put1byte_uOLED(0x23);
						put1byte_uOLED(0x84);
						put1byte_uOLED(0x10);//DF
						wait(10);
				*/			