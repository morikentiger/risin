/* serialADC.c */
//ボーレート38400

#include"iodefine.h"
#include"mydefine.h"
#include"myprintf.h"	//シリアル通信用(古川さん作成)


int SEN0,SEN1,SEN2,SEN3;
int i;

//グローバル変数宣言
unsigned long cntcmt=0;	//48day(永い)までカウントできる(周期1msecなら)




void init_adc(void){
	// スタンバイ解除 
//	STB.CR4.BIT._AD0 = 0;
	STB.CR4.BIT._AD1 = 0;
	// ADIイネーブル 
//	AD0.ADCSR.BIT.ADIE = 0;
	AD1.ADCSR.BIT.ADIE = 0;
	// トリガイネーブル 
//	AD0.ADCSR.BIT.TRGE = 0;
	AD1.ADCSR.BIT.TRGE = 0;
	// ADF コントロール 
//	AD0.ADCSR.BIT.CONADF = 0;
	AD1.ADCSR.BIT.CONADF = 0;
	// ステートコントロール 
//	AD0.ADCSR.BIT.STC = 0; 
	AD1.ADCSR.BIT.STC = 0; 
	// A/D変換時間;Pφ/4 ⇒動かなかったら早くしよう
//	AD0.ADCSR.BIT.CKSL = 0; 
	AD1.ADCSR.BIT.CKSL = 0; 
	// シングルモード=>4チャンネルスキャン、2チャンネルスキャン 
//	AD0.ADCSR.BIT.ADM = 1;	//4ch
	AD1.ADCSR.BIT.ADM = 1;	//4ch	3;	//2ch
	// 1サイクルスキャン 
//	AD0.ADCSR.BIT.ADCS = 0;
	AD1.ADCSR.BIT.ADCS = 0;
	// チャネルセレクト CH0 AN0 
//	AD0.ADCSR.BIT.CH = 3;	//011 ch0は、AN0,1,2,3
	AD1.ADCSR.BIT.CH = 3;	//011 ch1は、AN4,5,6,7	1;	//001 ch1は、AN4,5
}



//CMT割り込み初期設定
//resetprg.cの21行目を右のようにする #define SR_Init    0x00000000
void init_cmt0(void)
{
	//動作設定
    STB.CR4.BIT._CMT = 0;	//CMTスタンバイ解除(クロック供給開始)
    CMT.CMSTR.BIT.STR0 = 0;    //設定のためカウント停止
	
	CMT0.CMCSR.BIT.CMF = 0;      // CMCNTとCMCORの値が一致したか否かを示す  0:不一致, 1:一致
    CMT0.CMCSR.BIT.CMIE = 1;   // 1:コンペアマッチ割り込み許可(イネーブル)
    CMT0.CMCSR.BIT.CKS = 0;    // 0:クロックセレクトPφ/8	1sec=3,125,000 10msec=31,250 1msec=3125
	INTC.IPRJ.BIT._CMT0 = 13;	//割り込み優先度15

	//カウント周期設定
	CMT0.CMCOR = 3125; //Pφ/8で1msec
}


//WAIT関数 引数msecだけ待つ
void wait(int time_msec){	//引数は、待ち時間(msec)
	
	unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + time_msec;	//
	
	while(wait_end != cntcmt){}	//cntcmtがwait_endでない場合ループする(＝待ち時間分だけ待つ)
	
}



//CMT割り込み 周期1msec　ここがメインの処理
void interrpt_cmt0(void){
		
	CMT0.CMCSR.BIT.CMF = 0;	//検知フラグを戻してカウント再開
	
	cntcmt++;	//グローバル変数 48dayまでカウントできる
	
	//センサLEDon
	LEDS_Rr = 1;//Rrが点いた
	LEDS_Rf = 1;
	LEDS_Lf = 1;
	LEDS_Lr = 1;//Lrが点いた
	
	LED0 = 1;
	
	//wait(500);
	for(i=0;i<LED_Wait;i++){};//LEDを完全に光らせるために待つ。立ち上がりが完全なステップでないので。
		
	//センサ読み取り
	AD1.ADCR.BIT.ADST = 0;
	AD1.ADCSR.BIT.CH = 3;	//011 ch1は、AN4,5,6,7	//1;	//001 ch1は、AN4,5
	AD1.ADCR.BIT.ADST = 1;
	while(AD1.ADCSR.BIT.ADF == 0);
	SEN0 = AD1.ADDR4 >> 6;
	SEN1 = AD1.ADDR5 >> 6;
	SEN2 = AD1.ADDR6 >> 6;
	SEN3 = AD1.ADDR7 >> 6;
	AD1.ADCSR.BIT.ADF = 0;
	AD1.ADCR.BIT.ADST = 0;
	
	//センサLEDoff
	LEDS_Rr = 0;
	LEDS_Rf = 0;
	LEDS_Lf = 0;
	LEDS_Lr = 0;
	
	LED0 = 0;
	
//	wait(500);
		
	
}




void serialADC(void){
	
	PFC.PEIORL.WORD = 0xffff;	//PEを出力に設定 motor,LED
	PFC.PAIORL.WORD = 0xf3e0;	//PAの一部を出力に設定。PA5,6,7,8,9の5つ。故に、1111 0011 1110 0000 = 0xf3e0
	
	
	init_adc();
	init_sci1();	//シリアル通信の初期設定 myprintf.hにて
	
	init_cmt0();	//CMT割り込み初期設定
	
	CMT.CMSTR.BIT.STR0 = 1;	//CMTカウントスタート(A/D変換)
	
	
	while(1){
		
		myprintf("| %3d %3d | %3d %3d |\n\r",SEN0,SEN1,SEN2,SEN3);	
		
	}
	
}