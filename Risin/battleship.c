#include"iodefine.h"
#include"mydefine.h"
#include"myprintf.h"
#include"printf_uOLED.h"	//uOLED接続用のSCI0の設定関数のエクスターンプロトタイプ宣言
#include"uOLED.h"
#include <math.h>
#include"init.h"			//ADCとCMTとMTUの初期設定
#include"run.h"				//走行関数をまとめたもの
//#include"mazeSearcher.h"	//迷路探索関数

#include"mymusic.h"
#include"interFace.h"

//円周率πはM_pi

/*グローバル変数宣言*/
volatile int mode_flag_main = 0;//メイン文でモード切り替え中に1、それ以外0になり、LEDの点滅をモードか、センサか分けている。
volatile int i;
volatile unsigned long cntmtu = 20000;//モーターの実験、20000から下げていく
volatile int Mode_accel=8;	//加減速推移の状態を表す 0:加速、1:等速、2:減速、3:未4:未、5:角加速、6:角等速、7:角減速、8:なし 
//unsigned long 
volatile int A,B;
volatile int TEST_TGRA;
volatile float t_accel,accel_distance;
volatile float speed_l=10;
volatile float speed_r=10;
volatile float speed_L=10;
volatile float speed_R=10;
volatile float accel_angle=10;
volatile float target_distance,target_angle;
//volatile char Prescale_mtu=4;//プリスケーラを4にする（今まで16だったが、精度が低くなるので。）
volatile float Speed2TGRA_R,Speed2TGRA_L,onebyspeed;

volatile float voltBattery = 0;	//

volatile int num_turn = 0;//超新地旋回用に回転回数を選ぶ

//SpeedtoTGRA_R = 49.0 *49087.38594;//( Dia_R*49087.38594);//3.1415927*deg_step/360.0*(25.0*1000000.0/4) );
//SpeedtoTGRA_L = 40.25*49087.38594;//( Dia_L*49087.38594);//3.1415927*deg_step/360.0*(25.0*1000000.0/4) );


//double TGRA_4[4][ 


// フラグ変数宣言
volatile int Flag_reduce = 0;	//減速する場合1になる

volatile char Flag_Battery = 0;	//バッテリー電圧見る場合に1になる。


volatile long tempL=5;
volatile long tempR=5;//TGRAに入れるまえに入れて、判定する値
	


/*【ここまで】グローバル変数宣言*/



//CMT割り込み 周期1msec　ここがメインの処理
void interrpt_cmt0(void)
{
	CMT0.CMCSR.BIT.CMF = 0;	//検知フラグを戻してカウント再開
	cntcmt++;
	

		/////////// 電源電圧読み取り ///////////
	//電源電圧の読み取り、1/3にしてある)
	if(Flag_Battery==1){
		
	AD0.ADCR.BIT.ADST = 0;	//ADST=0のとき、ADCSRのCH(3ビット、モード切り替え)を変更出来る
	AD0.ADCSR.BIT.CH = 0;//000 ch0で、AN0シングルモード//3;	//011 ch1は、AN4,5,6,7	//1;	//001 ch1は、AN4,5
	AD0.ADCR.BIT.ADST = 1;
	while(AD0.ADCSR.BIT.ADF == 0);
	voltBattery = (float)( (float)(AD0.ADDR0*5.0*3.0)/1024.0/2.0/2.0/2.0/2.0/2.0/2.0) ;///1.024*1.260;//AD値を3倍に復元して保存する
	AD0.ADCSR.BIT.ADF = 0;
	AD0.ADCR.BIT.ADST = 0;
	}


	/////////// センサ読み取り ///////////
	//センサ読み取り(LEDOFF時)
	AD1.ADCR.BIT.ADST = 0;
	AD1.ADCSR.BIT.CH = 3;	//011 ch1は、AN4,5,6,7	//1;	//001 ch1は、AN4,5
	AD1.ADCR.BIT.ADST = 1;
	while(AD1.ADCSR.BIT.ADF == 0);
	SEN0_off = AD1.ADDR4 >> 6;
	SEN1_off = AD1.ADDR5 >> 6;
	SEN2_off = AD1.ADDR6 >> 6;
	SEN3_off = AD1.ADDR7 >> 6;
	AD1.ADCSR.BIT.ADF = 0;
	AD1.ADCR.BIT.ADST = 0;
	
	//センサLEDon
	LEDS_Rr = 1;//Rrが点いた
	LEDS_Rf = 1;
	LEDS_Lf = 1;
	LEDS_Lr = 1;//Lrが点いた
	
	for(i=0;i<LED_Wait;i++){};//LEDを完全に光らせるために待つ。立ち上がりが完全なステップでないので。
		
	//センサ読み取り(LEDON時)
	AD1.ADCR.BIT.ADST = 0;
	AD1.ADCSR.BIT.CH = 3;	//011 ch1は、AN4,5,6,7	//1;	//001 ch1は、AN4,5
	AD1.ADCR.BIT.ADST = 1;
	while(AD1.ADCSR.BIT.ADF == 0);
	SEN0_on = AD1.ADDR4 >> 6;
	SEN1_on = AD1.ADDR5 >> 6;
	SEN2_on = AD1.ADDR6 >> 6;
	SEN3_on = AD1.ADDR7 >> 6;
	AD1.ADCSR.BIT.ADF = 0;
	AD1.ADCR.BIT.ADST = 0;
	
	//センサLEDoff
	LEDS_Rr = 0;
	LEDS_Rf = 0;
	LEDS_Lf = 0;
	LEDS_Lr = 0;
	
	//センサ差分を取る
	SEN_FR = SEN0_on - SEN0_off;//AD1.ADDR4 >> 6;
	SEN_R  = SEN1_on - SEN1_off;//AD1.ADDR5 >> 6;
	SEN_L  = SEN2_on - SEN2_off;//AD1.ADDR6 >> 6;
	SEN_FL = SEN3_on - SEN3_off;//AD1.ADDR7 >> 6;
	
	//閾値を超えたらLED点灯
/*	if(mode_flag_main==0){
		if(SEN_R  > TH_R ){LED0=1;}else{LED0=0;}
		if(SEN_FR > TH_FR){LED1=1;}else{LED1=0;}
		if(SEN_FL > TH_FL){LED3=1;}else{LED3=0;}
		if(SEN_L  > TH_L ){LED4=1;}else{LED4=0;}
	}
*/	
	
		
			
	//制御をいれるかどうかの判定 柱がギリギリある場合は除いている。判定は半区間の地点とする。前壁はいらない？
	if(SEN_L  < THcton_L  | Flag_control_on!=1){KS_L  = 0;}else{KS_L  = 1;}
//	if(SEN_FL < THcton_FL | Flag_control_on!=1){KS_FL = 0;}else{KS_FL = 1;}
//	if(SEN_FR < THcton_FR | Flag_control_on!=1){KS_FR = 0;}else{KS_FR = 1;}
	if(SEN_R  < THcton_R  | Flag_control_on!=1){KS_R  = 0;}else{KS_R  = 1;}
//*/	

/*	if(SEN_L  < THcton_L  | Flag_control_on!=1)
	{KS_L  = 0;}
	else if(  !( (SEN_FR < THcton_FR) & (SEN_FL < THcton_FL) )  )
	{KS_L = 0;}
	else
	{KS_L  = 1;}
//	if(SEN_FL < THcton_FL | Flag_control_on!=1){KS_FL = 0;}else{KS_FL = 1;}
//	if(SEN_FR < THcton_FR | Flag_control_on!=1){KS_FR = 0;}else{KS_FR = 1;}
	if(SEN_R  < THcton_R  | Flag_control_on!=1)
	{KS_R  = 0;}
	else if(  !( (SEN_FR < THcton_FR) & (SEN_FL < THcton_FL) )  )
	{KS_R = 0;}
	else
	{KS_R  = 1;}
//*/	
	
						
	//PID制御による制御量の算出	
	error = KS_L*(SEN_L - THctrl_L) - KS_R*(SEN_R - THctrl_R);//+ KS_FL*(SEN_FL - THctrl_FL) - KS_FR*(SEN_FR - THctrl_FR) 

	i_error += error;

	d_error = error - error_last;

	control = Flag_all_control*( KP * error );//+ KD * d_error + KI * i_error;
	
	error_last = error;
	
	if(speed>=min_speed)
	{
		if(control > speed-min_speed)
		{
			control = speed-min_speed;
		}
	}
	//意味なさすぎ吹いたｗ
	
	///////////* 加減速の計算を行う *///////////
	
	//積分計算
	if(Flag_Calculation_Start==1)
	{
		//LED0=1;
		//速度
//		if(FlagkagensokuON==1)
//		{
			if(speed < target_speed){	speed += accel*dt;}
			else if(speed > target_speed){	speed -= decel*dt;}//よくない/////////////////////////////////////byImai
//		}
		else
		{
			//speed = target_speed;
		}
		
		
		//角速度
	//	if(FlagkagensokuON==1)
//		{
			if(ang_speed < target_ang_speed){	ang_speed += ang_accel*dt;}
			else if(ang_speed > target_ang_speed){	ang_speed -= ang_decel*dt;}
//		}
		else
		{
		//	ang_speed = target_ang_speed;
		}
		
		distance += speed*dt;
		angle += ang_speed*dt;
		
		if(cntcmt%200 == 0)
		{
		//	myprintf("V=%f,D=%f\n\r",speed,distance);
		}
	
	
	}//積分計算
	
	
	
	
	speed_l = speed - (int)control;
	speed_r = speed + (int)control;

	//左右のモーターが制御によって、ありえない角度にならないようにするための下限リミッタ
	//こいつのせいで、制御量が一定以上かからなくなる、旋回中に速度が下がらなくなるというバグが発生するのでは？
//	if(speed_r < min_speed){speed_r = min_speed;}//ここを１にしていたせいで、制御がかかると１になりオーバーフローして死んだ140607
//	if(speed_l < min_speed){speed_l = min_speed;}
		//↑141018ここが超新地の旋回角バグの原因じゃね？
		//とりあえず、速度の合成をする前に、移動した。speed_Rをspeed_rにした。
		//結果、旋回が速くなり、角度が９０度から140度くらいになっている。なお、直進には影響なし。
		//[対策]上に書いたように、controlとspeed-min_speedを比べる。
		//そこで、controlを引くことにより、speedがmin_speedを下回らないようするためcontrolの値に上限を設けた。
		//このことで、speedがmin_speed未満にならなくなった
		//つまり、超新地の値は、min_speedによらなくなった。
		//結果、９０度正しく旋回できることが確認できた(多少は調整が必要だが)
	
	//目標並進速度、目標角速度を左右のタイヤの周速度に変換
	speed_R = speed_r + dir_R*(tread/2)*(3.141592/180)*ang_speed;
	speed_L = speed_l + dir_L*(tread/2)*(3.141592/180)*ang_speed;
	

	
	//TGRAに入れる変数に、左右のタイヤの周速度を変換
//	tempL

	TGRA_L = 1;//(long)(Dia_L*SPEEDtoTGRAbyDIAMETER/speed_R);//.38594/speed_R;
	if(TGRA_L >= 65535)
	{
		TGRA_L = 65535;
	}
/*	if(tempL>=65535-1)
	{
		TGRA_L = 65535-1;
	}
	else if(tempL <= 5)
	{
		TGRA_L = 5;
	}
	else
	{
*///		TGRA_L = tempL;
//	}
	//( ang_speed*3.141592*tread/360.0 );//(180.0/3.14159)/(0.5*tread) );
	//(int)( Dia_L*3.14159*deg_step/360.0*(25.0*1000000.0/Prescale_mtu)/( ang_speed*(180.0/3.14159)/(0.5*Dia_L) ) );
	
//	tempR
	TGRA_R = 30000;//(long)(Dia_R*SPEEDtoTGRAbyDIAMETER/speed_L);//.38594/speed_L;
	if(TGRA_R >=65535)
	{
		TGRA_R = 65535;
	}
/*	if(tempR>=65535-1)
	{
		TGRA_R = 65535-1;
	}
	else if(tempR <= 5)
	{
		TGRA_R = 5;
	}
	else
	{
*///		TGRA_R = (int)tempL;
//	}
	//( ang_speed*3.141592*tread/360.0 );//(180.0/3.14159)/(0.5*tread) );
	//(int)( Dia_R*3.14159*deg_step/360.0*(25.0*1000000.0/Prescale_mtu)/( ang_speed*(180.0/3.14159)/(0.5*Dia_R) ) );
//	TGRA_L = (long)(speed*360.0*(25.0*1000000.0/Prescale_mtu)/(Dia_L*3.14159*deg_step));
//	TGRA_R = (long)(speed*360.0*(25.0*1000000.0/Prescale_mtu)/(Dia_R*3.14159*deg_step));

	if(cntcmt%200==0)
	{
//		myprintf("TGRA_R=%d,speed_R=%f,speed_r=%f,TGRA_L=%d,speed_L=%f,speed_l=%f,ang_speed=%f\n\r",TGRA_R,speed_R,speed_r,TGRA_L,speed_L,speed_l,ang_speed);
	//	myprintf("
	}

//ブザー関係
	
	//LED4=1;
	if(Flag_bz_on==1)//ブザーONフラグが1なら音をならす
	{
		//LED3=1;
		switch(num_music){
			case 0:PlayMusic(FFfanfale[num_notes-1], size_FFfanfale, 200*2);break;
			case 1:PlayMusic(HanayamataOP[num_notes-1], size_HanayamataOP, 150*2);break;
			case 2:PlayMusic(Legendary_Air_Raid_Machine[num_notes-1], size_Legendary_Air_Raid_Machine, 154*2);break;
			case 3:PlayMusic(Soreha_bokutachi_no_kiseki_LOVELIVE[num_notes-1], size_Soreha_bokutachi_no_kiseki_LOVELIVE, 180*2);break;
			case 4:PlayMusic(FFfanfale[num_notes-1], size_FFfanfale, 180*2);break;
			
		}
		
	}//Flag_bz_on
	else
	{
		MTU_BZ_OFF();           //ブザーの発振を終了
		//値、フラグの初期化
		//num_notes=1;
		Flag_bz_start = 0;
		cnt_bz = 0;
	}
	
	
	//デバッグ用シリアル通信
	//if(cntcmt%1 & 
	if(SW1==0 )//| cntcmt%500)
	{
	//	myprintf("%6d %6d\n\r", TGRA_R,TGRA_L);
/*		myprintf("%d | %5.3f | %5.3f %5.3f | %5.3f %5.3f %5.3f | %5.3f %5.3f | %6d %6d | %6d %6d | %6d %6d |\n\r", FlagkagensokuON,speed, speed_r,speed_l, 
		ang_speed, dir_R*(tread/2)*(3.141592/180)*ang_speed, dir_L*(tread/2)*(3.141592/180)*ang_speed,
		speed_R, speed_L,
		TGRA_R,TGRA_L,
		MTU21.TGRA,MTU21.TGRB,
		MTU20.TGRA,MTU20.TGRB);
*/	}
	
	
}//ここまでCMT割り込み

//WAIT関数 引数msecだけ待つ
int wait(int time_msec)
{	//引数は、待ち時間(msec)
	
	volatile unsigned long wait_end = 0;	//待ち時間の最後(上手く表現できない
	
	wait_end = cntcmt + time_msec;	//
	
	while(wait_end != cntcmt){}	//cntcmtがwait_endでない場合ループする(＝待ち時間分だけ待つ)
	
}

//MTU2割り込み関数(この関数で、左右のモータのデューティをセットする
void interrupt_motor_right(void)
{
	
	MTU20.TSR.BIT.TGFB = 0;//フラグクリア
	MTU20.TGRA = TGRA_L;//(long)(Dia_L*3.14159*deg_step/360.0*(25.0*1000000.0/Prescale_mtu)/speed);
//	MTU20.TGRB = MTU20.TGRA*0.5;
		
}

void interrupt_motor_left(void)
{
		
	MTU21.TSR.BIT.TGFB = 0;	//コンペアマッチ発生フラグクリア
	MTU21.TGRA = TGRA_R;//(long)(Dia_R*3.14159*deg_step/360.0*(25.0*1000000.0/Prescale_mtu)/speed);
//	MTU21.TGRB = MTU21.TGRA*0.5;//
}

//run関数が別の関数下でちゃんと動作するか確認するためのデバック用関数
void testRun(void)
{
//	run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
	run(1,50,1600,50,accel,decel,180*15);
										
}











////////////////////////
////				////
////	メイン文	////
////				////
////////////////////////
void battleship(void)
{
	
	int i_sar;		//？sarってなんだろ。
	int i=0;		//適当にfor文回すための変数だな。
	int mode_main = 0;//メイン文（これ）でのモード切り替え用変数
	int numRot = 1;
	unsigned long timelength = 1;
	char val=0;
	
	//ポート設定(ピンファンクションコントローラー)
	PFC.PEIORL.WORD = 0x01ff;	//PEをI/O設定1が出力、0が入力PE10,11はスイッチなので入力 motor,LED 0000 0001 1111 1111 = 0x01ff
								//160209火PE,912,13,14,15を入力→↑のままでOk
	PFC.PAIORL.WORD = 0xf3e0;	//PAの一部を出力に設定。PA5,6,7,8,9の5つ。故に、1111 0011 1110 0000 = 0xf3e0
	
	//初期設定用関数
	init_adc();
	init_sci1();	//シリアル通信の初期設定 myprintf.hにて
	init_mtu_motor();	//MTU2初期設定
	init_cmt0();	//CMT割り込み初期設定
	
	//CMT割り込み開始
	CMT_ON();	//CMTカウントスタート(A/D変換)
	
	
		
	//PE.DRL.BIT.B2 = 1;	//RESETだが、電源投入時にリセットが起こってるので、必要ない。
	
	//PE.DRL.BIT.B2 = 0;
	
	
	
	//モーター起動・励磁かける
	Motor_Enable();	//MotorEnableをHiに。
	wait(300);	//7073のデータシート(14/39)を参照。100μs以上待つ必要ある。

	//モード選択時に無駄な電力消費しないように励磁を切る
	Motor_Disable();
	
	Motor_Riset();
	
	//AD変換を切っておく
	ADC_OFF();
	
	//インターフェース用LEDを切る
	LED_all(0);

	
	
	
	
	////////////////////////
	//バッテリー電圧の警告
	///////////////////////
	LED2=1;
	Flag_Battery = 1;//電圧用AD変換を行うようにする
	wait(1);
	LED2=0;
	LED_all(0x0a);//LED1,3両方点灯
	myprintf("voltBattery = %f\n\r",voltBattery);
	if(voltBattery <= 11.8)
	{
		LEDsimasima(1000);//警告のつもり
		myprintf("UNDER 12.0V CAREFULL!\n\r");
	}
	if(voltBattery <= 11.6)
	{
		LEDsimasima(3000);
		myprintf("UNDER 11.0V NOT DESTROY Li-Po Battery !\n\r");
	}
	Flag_Battery = 0;//電圧用AD変換を行わないようにする
	LED_all(0);
	
	while(1)//以下無限ループ
	{

		mode_flag_main=1;
		mode_main = Switch_LED(mode_main);//mode_mainを書き換え、対応したLEDを光らせる。
		
		
		if(SW1!=0&SW2==0){//SW1!=0&SW2==0){//緑スイッチのみON
			LEDFlash(mode_main, 100, 2);
			KnightRider(1,300);
		
			mode_flag_main = 0;
			ADC_ON();//AD変換開始
			
			
			
			///////////////////////////////////////////////////////////////////////////////
			////////////////		モード切り替え			///////////////////////////////
			///////////////////////////////////////////////////////////////////////////////
			
			switch(mode_main){
				case 0:
				//	myprintf("val=%d\n\r", val);
				//	val++;
				//	myprintf("val=%d\n\r", val);
					PE.DRL.BIT.B1=1;
					PE.DRL.BIT.B5=0;
				//	Motor_start();
				//	testgame();
					
				//	myprintf("val=%d\n\r", val);
				//	val++;
				//	myprintf("val=%d\n\r", val);
				
				//Draw Filled Rectangle(矩形)
				//→なぜか動かない。コマンドは正しいはずだが…。ノーマルのRectangleと共存できないのか？
				/*	wait(10);
				put1byte_uOLED(0xFF);
				put1byte_uOLED(0xCE);//cmd
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x0A);//x1(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x14);//y1(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x5A);//x2(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x3C);//y2(LSB)
				put1byte_uOLED(0x07);
				put1byte_uOLED(0xE0);//colour 
				wait(10);
			*/	
	
			/*	//Draw Rectangle(矩形)
				wait(10);
				put1byte_uOLED(0xFF);
				put1byte_uOLED(0xCF);//cmd
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x0A);//x1(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x14);//y1(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x5A);//x2(LSB)
				put1byte_uOLED(0x00);
				put1byte_uOLED(0x3C);//y2(LSB)
				put1byte_uOLED(0xF8);
				put1byte_uOLED(0x00);//colour 赤
				wait(10);
			*/
				
				
				/*	
					Flag_all_control = 1;
					Flag_control_on=1;
					while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
						myprintf("| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);
					//	myprintf("| %3d %3d | %3d %3d |       |\n\r",SEN_R >= TH_Wall_R,0,SEN_L >= TH_Wall_L,refSEN_FL >= TH_Wall_FL & SEN_FR >= TH_Wall_FR);
					//	myprintf("| %3d %3d | %3d %3d |       |\n\n\r",(SEN_R >= TH_Wall_R)& 0x08,0& 0x04,(SEN_L >= TH_Wall_L)& 0x02,(refSEN_FL >= TH_Wall_FL & SEN_FR >= TH_Wall_FR)& 0x01 );
					//	myprintf("| %3d %3d | %3d %3d |       |\n\n\r",(SEN_R >= TH_Wall_R)<<3,0<<2,(SEN_L >= TH_Wall_L)<<1,(SEN_FL >= TH_Wall_FL | SEN_FR >= TH_Wall_FR)<<0 );
						
						wait(200);
					}
					
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
									
					Flag_control_on=1;
				
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
				
					lefthandNonstop();
					
					wait(500);
					Motor_Disable();	//MotorEnableをHiに。
					wait(200);
					
					
					Flag_control_on = 1;//制御OFF
					wait(200);
					
					wait(500);
					Motor_Disable();	//MotorEnableをHiに。
					Flag_control_on = 1;//制御ON
			*/
					break;
				case 1:
					init_uOLED();	//uOLED接続用シリアル通信SCI0を使う。
					wait(200);
					UOLED_RESET_N = 1;		//uOLEDのリセット
					//ピンの接続は右の通り マイコンのピン(→uOLEDのピン)と表示。(RX0(→TX),TX0(→RX),PA2(→RES)の設定)printf_uOLED.cにて
					wait(200);
					while(1)
					{
					//Beep
					put1byte_uOLED(0xFF);
					put1byte_uOLED(0xDA);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x2D);
					put1byte_uOLED(0x03);
					put1byte_uOLED(0xE8);
					}
				
				
					//Media Init
					put1byte_uOLED(0xFF);
					put1byte_uOLED(0xB1);
					wait(200);
					
					//Set Byte Address
					put1byte_uOLED(0xFF);
					put1byte_uOLED(0xB9);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x02);
					put1byte_uOLED(0x01);
					
				/*	//Set Sector Address
					put1byte_uOLED(0xFF);
					put1byte_uOLED(0xB8);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x0A);
					
				*/	
					wait(200);
					while(1)
					{
					//Display Image (RAW)
					put1byte_uOLED(0xFF);
					put1byte_uOLED(0xB3);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x0A);
					put1byte_uOLED(0x00);
					put1byte_uOLED(0x14);
					wait(500);
					}
					
			//	*/
				/*
				//	accel = 2000;
				//	target_speed = 300;
				
					num_turn = 1;
					
					while(1)//以下無限ループ
					{
						Motor_Enable();	//MotorEnableをHiに。
							
						
						//num_turn = Switch_LED(mode_main);//mode_mainを書き換え、対応したLEDを光らせる。
		
		
						if(SW1!=0&SW2==0)
						{//緑スイッチのみON
							
							
							//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
							//wait(500);
								while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
							{
							//	numRot = Switch_LED(1);
							//	wait(100);
							}
											
							Flag_control_on=0;
					
							LED2 = 1;
							wait(50);
							LED2 = 0;
							wait(200);
						//	num_music = 2;//2:伝説のエアライドマシン
						//	Flag_bz_on=1;
					
							//First_run();
							//sla_RotRight(4*0.99);
							testslalom_R(4*5-1);
				
						//	RotRight(ROT90Ratio);//0.75);//1);//4*10);//4*num_turn);//4*10 );
							
				
					//		RotRight(ROT180Ratio);
							wait(500);
					
							Flag_bz_on=0;
							LEDsimasima(200);
							Motor_Disable();	//MotorEnableをHiに。
							
							break;//num_turn選択のためのwhile(1)を抜ける			
													
						}
					}
					
		*/
					break;
					
				case 2:
				//	accel = 2000;
				//	target_speed = 300;
				
					Motor_Enable();	//MotorEnableをHiに。
							
					while(1)//以下無限ループ
					{

						
					//	num_turn = Switch_LED(mode_main);//mode_mainを書き換え、対応したLEDを光らせる。
		
		
						if(SW1!=0&SW2==0)
						{//緑スイッチのみON
							
							
				
							Motor_Enable();	//MotorEnableをHiに。
							//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
							//wait(500);
								while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
							{
							//	numRot = Switch_LED(1);
							//	wait(100);
							}
											
							Flag_control_on=0;
					
							LED2 = 1;
							wait(50);
							LED2 = 0; 
							wait(200);
							//Forward(3);
							//sla_RotLeft(4*1.01);
							
							testslalom_L(4*5-1);
						
						//	RotLeft(ROT90Ratio);//0.75);//1);//4*10);//4*num_turn	);//4*10 );
					
						//	RotLeft(ROT180Ratio);
							
							wait(500);
							LEDsimasima(200);
							Motor_Disable();	//MotorEnableをHiに。
							
							break;
						}
					}
			
				
					break;
				case 3:
			//		accel = 2000;
			//		target_speed = 300;
			//		wait(500);
					//	if(SEN_FR > TH_FR & SEN_FL > TH_FL){	
						
						Motor_Enable();	//MotorEnableをHiに。
						//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
						//wait(500);
						
						
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
												
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
					
						//testslalom();
					//	lefthandslalom();
						//lefthandmethod();
					
						//16区間の調整
						Flag_all_control=0;
					//	run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*1);//5);//15);
					//	Acc90mm();
					//	Reduce90mm();
						
						
					//速度500mm/s	
						Motor_Enable();	//MotorEnableをHiに。
						
						while(!(SW1!=0&SW2==0)  )//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
												
						myprintf("V=500mm/s\n\r");
						run(1,min_speed,500,500,accel,decel,180*1);//5);//15);
						timelength = cntcmt;
						while(!(SW1!=0&SW2==0) & (cntcmt < timelength + 500) )//緑スイッチが押されるまで
						{
							run(1,500,500,500,accel,decel,180*1);//5);//15);
					
						}
						run(1,500,500,min_speed,accel,decel,180);
												
						wait(500);
						Motor_Disable();	//MotorEnableをHiに。
					
					//速度600mm/s	
						Motor_Enable();	//MotorEnableをHiに。
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
												
						myprintf("V=600mm/s\n\r");
						run(1,min_speed,600,600,accel,decel,180*1);//5);//15);
						timelength = cntcmt;
						while(!(SW1!=0&SW2==0) & (cntcmt < timelength + 500) )//緑スイッチが押されるまで
						{
							run(1,600,600,600,accel,decel,180*1);//5);//15);
					
						}
						run(1,600,600,min_speed,accel,decel,180);
												
						wait(500);
						Motor_Disable();	//MotorEnableをHiに。
						
					
					
					//速度1500mm/s	
						Motor_Enable();	//MotorEnableをHiに。
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
												
						myprintf("V=1500mm/s\n\r");
						run(1,min_speed,1500,1500,accel,decel,180*3);//5);//15);
						timelength = cntcmt;
						while(!(SW1!=0&SW2==0) & (cntcmt < timelength + 500) )//緑スイッチが押されるまで
						{
							run(1,1500,1500,1500,accel,decel,180*1);//5);//15);
					
						}
						run(1,1500,1500,min_speed,accel,decel,180*3);
												
						wait(500);
						Motor_Disable();	//MotorEnableをHiに。
						
					//速度2000mm/s	
						Motor_Enable();	//MotorEnableをHiに。
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
												
						myprintf("V=2000mm/s\n\r");
						run(1,min_speed,2000,2000,accel,decel,180*4);//5);//15);
						timelength = cntcmt;
						while(!(SW1!=0&SW2==0) & (cntcmt < timelength + 500) )//緑スイッチが押されるまで
						{
							run(1,2000,2000,2000,accel,decel,180*1);//5);//15);
					
						}
						run(1,2000,2000,min_speed,accel,decel,180*4);
												
						wait(500);
						Motor_Disable();	//MotorEnableをHiに。
						
					//速度2500mm/s	
						Motor_Enable();	//MotorEnableをHiに。
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
						Flag_control_on=0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
												
						myprintf("V=2500mm/s\n\r");
						run(1,min_speed,2500,2500,accel,decel,1080*1);//5);//15);
						timelength = cntcmt;
						while(!(SW1!=0&SW2==0) & (cntcmt < timelength + 500) )//緑スイッチが押されるまで
						{
							run(1,2500,2500,2500,accel,decel,180*1);//5);//15);
					
						}
						run(1,2500,2500,min_speed,accel,decel,1080);
												
						wait(500);
						Motor_Disable();	//MotorEnableをHiに。
									
						wait(200);
						Flag_all_control=1;
				//	};
					
					break;
				case 4:
					Motor_Enable();	//MotorEnableをHiに。
						//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
						//wait(500);
							while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
												
						Flag_control_on=0;
						
						LED2 = 1;
						wait(200);
						LED2 = 0;
						wait(200);
						
				//	num_music = 2;//2:伝説のエアライドマシン
				//	Flag_bz_on=1;
					
					//First_run();
					//sla_RotRight(4*0.99);
				//	testslalom_R(1);
					
					
				
					RotRight(ROT180Ratio);//4*10 );
					
					//wait(800);
					
					Flag_bz_on=0;
					LEDsimasima(100);
					Motor_Disable();	//MotorEnableをHiに。
			
					break;
				case 5:
					Motor_Enable();	//MotorEnableをHiに。
						//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
						//wait(500);
						
						
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
												
						Flag_control_on=0;
						
						LED2 = 1;
						wait(200);
						LED2 = 0;
						wait(200);						
						Flag_control_on=0;
						
					
				//	num_music = 2;//2:伝説のエアライドマシン
				//	Flag_bz_on=1;
					
					//First_run();
					//sla_RotRight(4*0.99);
					//testslalom_L(1);
					
					
			
					RotLeft(ROT180Ratio);//4*10 );
					
				//	wait(800);
					
					Flag_bz_on=0;
					LEDsimasima(100);
					Motor_Disable();	//MotorEnableをHiに。
			
					break;
				case 6:
					Motor_Enable();	//MotorEnableをHiに。
						//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
						//wait(500);
						
						
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
												
						Flag_control_on=1;//0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
					
				//16区間の調整
					Flag_all_control=0;
					run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*5);//7);//15);//5);//15);
					wait(200);
					LEDFlash(4,200,3);
				//	RotRight(2);
				//	wait(200);
				//	LEDFlash(4,200,3);
				//	run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*15);//5);//15);
					
				
				/*								
					Flag_control_on=0;
					
					run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
					run(1,Def_target_speed,Def_target_speed,Def_target_speed,accel,decel,180);
					run(1,Def_target_speed,Def_target_speed,min_speed,accel,decel,90);
					wait(500);
					run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,270);
					wait(250);
					run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90);
					wait(500);
					run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
					run(1,Def_target_speed,Def_target_speed,Def_target_speed,accel,decel,180);
					run(1,Def_target_speed,Def_target_speed,min_speed,accel,decel,90);
					wait(500);
					run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,270);
					wait(250);
					run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90);
					wait(500);
				*/	
					LEDsimasima(200);
					Motor_Disable();
					
					
					break;
				case 7:
					Motor_Enable();	//MotorEnableをHiに。
						//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
						//wait(500);
						
						
						while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
						{
						//	numRot = Switch_LED(1);
						//	wait(100);
						}
												
						Flag_control_on=1;//0;
						
						LED2 = 1;
						wait(500);
						LED2 = 0;
						wait(200);
					
				//16区間の調整
					Flag_all_control=0;
					run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*15);//7);//15);//5);//15);
					wait(200);
					LEDFlash(4,200,3);
				//	RotRight(2);
				//	wait(200);
				//	LEDFlash(4,200,3);
				//	run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*15);//5);//15);
					
				
				/*								
					Flag_control_on=0;
					
					run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
					run(1,Def_target_speed,Def_target_speed,Def_target_speed,accel,decel,180);
					run(1,Def_target_speed,Def_target_speed,min_speed,accel,decel,90);
					wait(500);
					run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,270);
					wait(250);
					run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90);
					wait(500);
					run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
					run(1,Def_target_speed,Def_target_speed,Def_target_speed,accel,decel,180);
					run(1,Def_target_speed,Def_target_speed,min_speed,accel,decel,90);
					wait(500);
					run(3,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,270);
					wait(250);
					run(2,min_ang_speed,Def_target_ang_speed,min_ang_speed,ang_accel,ang_decel,90);
					wait(500);
				*/	
					LEDsimasima(200);
					Motor_Disable();
					
				/*
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);						
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
						
				RotRight(1);
					
					wait(800);
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);						
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
						
				RotLeft(1);
					
					wait(800);
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);						
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
						
				RotRight(2);
					
					wait(800);
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);						
					Flag_control_on=0;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
						
				RotLeft(2);
					
					wait(800);
					
					Flag_bz_on=0;
					LEDsimasima(200);
					Motor_Disable();	//MotorEnableをHiに。
			*/
					break;
				case 8:
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//	サーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキット	//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					Motor_Riset();
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					
					KP = 1;//1;//0.2;
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=1;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(500);						
					Flag_all_control=1;
					
					Acc90mm();
					
					//一周目
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//2周目
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//3周目
					run(1,Def_target_speed,1400,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
				/*	run(1,Def_target_speed,1000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
				*/	
				
					//3週途中で止まる
					run(1,Def_target_speed,500,min_speed,accel,decel,180*3);
					
					KnightRider(4,200);
					
					Motor_Disable();	//MotorEnableをHiに。
					wait(200);
					
					KP = defKP;
				
					break;
				case 9:
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//	サーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキット	//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					
					KP = 1;//1;//0.2;
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=1;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(500);						
					Flag_all_control=1;
					
					Acc90mm();
					
					
					//一周目
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
				
					//2周目
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//3周目
					run(1,Def_target_speed,1600,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
				/*	run(1,Def_target_speed,1100,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1100,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1100,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
				*/
					//3週途中で止まる
					run(1,Def_target_speed,500,min_speed,accel,decel,180*3);
					
					KnightRider(4,200);
					
					Motor_Disable();	//MotorEnableをHiに。
					wait(200);
					
					KP = defKP;
					break;
					
				case 10:
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//	サーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキット	//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					
					KP = 1;//1;//0.2;
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=1;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(500);						
					Flag_all_control=1;
					
					Acc90mm();
					
					
					//一周目
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//2周目
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//3周目
					run(1,Def_target_speed,1800,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
				/*	run(1,Def_target_speed,900,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,900,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,900,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
				*/
					//3週途中で止まる
					run(1,Def_target_speed,500,min_speed,accel,decel,180*3);
					
					KnightRider(4,200);
					
					Motor_Disable();	//MotorEnableをHiに。
					wait(200);
					
					KP = defKP;
				
					break;
					
				case 11:
				//	accel = 3500;//5000;
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//	サーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキットサーキット	//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					Motor_Enable();	//MotorEnableをHiに。
					//Enableにしてから待つ(下でスイッチ推すまでずっと励磁)
					//wait(500);
					
					KP = 1;//1;//0.2;
					
					while(!(SW1!=0&SW2==0))//緑スイッチが押されるまで
					{
					//	numRot = Switch_LED(1);
					//	wait(100);
					}
											
					Flag_control_on=1;
					
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(500);						
					Flag_all_control=1;
					
					Acc90mm();
					
					
					//一周目
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//2周目
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					
					//3周目
					run(1,Def_target_speed,2000,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
				/*	run(1,Def_target_speed,700,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
					run(1,Def_target_speed,700,Def_target_speed,accel,decel,180*14);//6);
					sla_RotRight(1);
					run(1,Def_target_speed,700,Def_target_speed,accel,decel,180*14);//8);
					sla_RotRight(1);
				*/
					//3週途中で止まる
					run(1,Def_target_speed,500,min_speed,accel,decel,180*3);
					
					KnightRider(4,200);
					
					Motor_Disable();	//MotorEnableをHiに。
					wait(200);
					
					KP = defKP;
				
					break;
					
				case 12:
				//	accel = 3000;//6000;
					target_speed = 1700;
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					Motor_Enable();	//MotorEnableをHiに。
					wait(1);
			
					//Forward(16);
					i_sar=0;
						
						while(i_sar<4){
				/*			Forward(15);
							wait(300);
							RotRight(1);
							wait(300);
							Forward(7);
							wait(300);
							RotRight(1);
							wait(300);
							Forward(15);
							wait(300);
							RotRight(1);
							wait(300);
							Forward(7);
							wait(300);
							RotRight(1);
							wait(300);
										
				*/	
							i_sar++;	
						}
			
					wait(500);
					Motor_Disable();	//MotorEnableをHiに。
			
				
				
					break;
					
				case 13:
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					Motor_Enable();	//MotorEnableをHiに。
					wait(1);
			
					//First_run();
					//sla_RotRight(4*0.99);
					RotRight(4);
					
					wait(500);
					Motor_Disable();	//MotorEnableをHiに。
			
				
				
					break;
					
					
				case 14:
					LED2 = 1;
					wait(500);
					LED2 = 0;
					wait(200);
					Motor_Enable();	//MotorEnableをHiに。
					wait(1);
			
					//First_run();
					//sla_RotRight(4*0.99);
					RotLeft(4);
					
					wait(500);
					Motor_Disable();	//MotorEnableをHiに。
			
				
				
					break;
					
				case 15:
					
				/*	
					num_music = 0;
					wait(300);
					while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
						num_music = Switch_LED(num_music);
						num_notes = 1;
					}
					
					LEDFlash(num_music, 200, 3);					
					KnightRider(1,300);
					while(!(SW1!=0&SW2==0)){//緑スイッチが押されるまで
						
						//myprintf(" %d %d %d %d %d \n\r",size_HanayamataOP,sizeof(HanayamataOP),sizeof(HanayamataOP->freq),sizeof(HanayamataOP->beat),sizeof(HanayamataOP->Tempo_music) );//"| %3d %3d | %3d %3d | %7.2f |\n\r",SEN_L,SEN_FL,SEN_FR,SEN_R,control);	
						Flag_bz_on=1;
											
					}
					Flag_bz_on=0;
				*/	
				/*	Flag_control_on = 0;//制御OFF
					LED2 = 1;
					wait(300);
					LED2 = 0;
					wait(300);
					Motor_Enable();	//MotorEnableをHiに。
					wait(1);
					
					
					LED4=1;
					//run(1,min_speed,Def_target_speed,min_speed,accel,decel,180*3);
					
					Acc90mm();
				//	Add180mm();
				//	Add180mm();
					Reduce90mm();
					Acc90mm();
					Reduce90mm();
					Acc90mm();
					Reduce90mm();
					
					//run(1,50,500,50,accel,decel,180*3);
					//testRun();
					//Acc90mm();
					LED4=0;
					//LED2=1;
					//	run(1,50,8000,50,1000,1000,180*15);//加速度、減速度をaccelでなく、値(マジックナンバー)にすると、CMTと計算が合わない
					//LED2=0;
				*/	/*	run(1,min_speed,Def_target_speed,Def_target_speed,accel,decel,90);
						LED3=1;
						run(1,500,800,500,accel,decel,180);
						run(1,500,800,min_speed,accel,decel,90);
						wait(500);
						run(2,50,1000,50,ang_accel,ang_decel,270);
						wait(250);
						run(3,50,1000,50,ang_accel,ang_decel,90);
						wait(500);
						run(1,min_speed,800,500,accel,decel,90);
						run(1,500,800,500,accel,decel,180);
						run(1,500,800,min_speed,accel,decel,90);
						wait(500);
						run(3,50,1000,50,ang_accel,ang_decel,270);
						wait(250);
						run(2,50,1000,50,ang_accel,ang_decel,90);
					*/	wait(500);
					
					
					
				
				
					break;
				
				default:
					LEDFlash(mode_main,100,5);
					LEDsimasima(1000);
					
					break;
					
					
				
			}
			wait(200);		
		
			
			
		}
		
	}
	
}