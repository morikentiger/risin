/* testgame.h */
#include <stdlib.h>
#include "iodefine.h"
#include "mydefine.h"
#include "myprintf.h"
#include "printf_uOLED.h"
#include "uOLED.h"

typedef struct {
	char x;
	char y;
	char vx;
	char vxmax;
	char vy;
	char vymax;
	char ax;
	char ay;
	char life;
	char jump;
	char jumponce;
	char ground;
	char image_w;
	char image_h;
	char bounds_w;
	char bounds_h;
		
}CharaData;


//プロトタイプ宣言
void jicharaInit();
void tekiInit();
void JitamaMove();
void TekiMove();
char IsAtari(CharaData, CharaData);
char AtariHantei();

//グローバル変数
extern CharaData jikidata;
extern CharaData jitama[3];
extern char trigger;
extern CharaData tekidata[3];
extern t_colour colour;