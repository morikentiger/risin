/* mazeSercher.h */



struct State {
	int 	Wall[16][16];	//区間ごとの壁情報
	int		X;				//区間座標X
	int		Y;				//区間座標Y
	int		Psi;			//区間姿勢ψ
};


int X_goal;
int Y_goal;



volatile int Mode_Motion=100;	//動作を選択する(0:なし、1:前後、2:回転

volatile int Flag_wall_end = 0;	//ケツ(end)が壁に付いているとき1。スタートとUターン後、1になる

volatile int Flag_motion_LHM = 0;	//左手法での行動選択フラグ



void lefthandslalom(void)
{
	
	int cntLHM = 1;
	int time_run = 200;//200;

	int flag_seat = 1;		//ケツ当てをする場合は1になる。最初はケツ当てから始まるので1にする。
	//Flag_motion_LHM = 0;
	struct State Mypos ={255, 0, 0, North};
	
	X_goal = GORL_X;
	Y_goal = GORL_Y;
	
	Flag_wall_end = 1;
	//First_run();//始めの一歩（要調整、ケツをくっつけた状態から始めるので、90mmではない）
	//run_start();
	Flag_wall_end = 0;
	
	Mypos.X = 0;
	Mypos.Y = 1;
	wait(time_run);
	
	while(!(Mypos.X = X_goal & Mypos.Y == Y_goal))
	{//cntLHM != 4*5){//Coordinate != Goal){
		
	/*	if(flag_seat == 1)
		{
			run_start();
			flag_seat = 0;
		}
		else
		{
			
			Acc90mm();
			
		}
	*/
	Acc90mm();
		
		while(1)
		{//直進を続ける
			if(refSEN_L < TH_L){//左の壁がない→左に90度ターンして、180mm前進
				Flag_motion_LHM = 1;
				sla_RotLeft(1*1.01);
				/*if(refSEN_R > TH_R){//左に曲がる際、右壁があったらケツ当てをする
					flag_seat = 1;	//
				}*/
				//break;
							
			}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;//このとき等速から、減速に遷移しない。(Mode_motionが1のまま、つまり、等速のまま）
				Add180mm();//180mm追加で走る
			
			}else if(refSEN_R < TH_R){//右の壁がない→右に90度ターンして、180mm前進
				Flag_motion_LHM = 3;
				sla_RotRight(1*0.99);
				//flag_seat = 1;
				//break;
						
			}else{//その他（壁に囲まれた→右に180度ターンして、180mm前進
				Flag_motion_LHM = 4;
				flag_seat = 1;
				break;
				
			}			
		}
		if(Flag_motion_LHM==1){
						
		}else if(Flag_motion_LHM==3){
			
		}else if(Flag_motion_LHM==4){
			Reduce90mm();
			wait(time_run);
			RotRight(2);//Uターン
			wait(time_run);
			if(flag_seat == 1){
				Backward_tune();
				wait(time_run);
			}
		}
		
				
		cntLHM++;
		
	}	
	
	KnightRider(1200);
	
}//lefthandslalom



void lefthandNonstop(void)
{
	
	int cntLHM = 1;
	int time_run = 200;//200;

//	int flag_seat = 1;		//ケツ当てをする場合は1になる。最初はケツ当てから始まるので1にする。
	//Flag_motion_LHM = 0;
	struct State Mypos ={255, 0, 0, North};
	
	X_goal = GORL_X;
	Y_goal = GORL_Y;
	
	Flag_wall_end = 1;
	//First_run();//始めの一歩（要調整、ケツをくっつけた状態から始めるので、90mmではない）
	//run_start();
	Flag_wall_end = 0;
	
	Mypos.X = 0;
	Mypos.Y = 1;
	wait(time_run);

	while(!(Mypos.X = X_goal & Mypos.Y == Y_goal))
	{//cntLHM != 4*5){//Coordinate != Goal){
		
	/*	if(flag_seat == 1){
			LED4=1;
			run_start();
			LED3=1;
			flag_seat = 0;
		}else{
			
			Acc90mm();
			
		}
	*/	Acc90mm();
		
		
		while(1){//直進を続ける
		//	LED0=1;
			//区間の切れ目なのでセンサ値取得する
			refSEN_FR = SEN_FR;
			refSEN_R  = SEN_R;
			refSEN_L  = SEN_L;
			refSEN_FL = SEN_FL;
					
			if(refSEN_L < TH_L){//左の壁がない→左に90度ターンして、180mm前進
				Flag_motion_LHM = 1;
				//【予定】slaLeft();//さらに↓のbreakを取る
			/*	if(refSEN_R > TH_R){//左に曲がる際、右壁があったらケツ当てをする
					flag_seat = 1;	//
				}
			*/	break;
							
			}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進(等速)
				Flag_motion_LHM = 2;
				Add180mm();//180mm追加で走る
			
			}else if(refSEN_R < TH_R){//右の壁がない→右に90度ターンして、180mm前進
				Flag_motion_LHM = 3;
				//【予定】slaRight();//さらに↓のbreakを取る
			//	flag_seat = 1;
				break;
						
			}else{//その他（壁に囲まれた→右に180度ターンして、180mm前進
				Flag_motion_LHM = 4;
			//	flag_seat = 1;
				break;
				
			}			
		}//whiel(1)直進を続ける
		LED2=1;
		Reduce90mm();
		wait(200);
		LED1=1;
		if(Flag_motion_LHM==1){
			RotLeft(1);	//左旋回
			wait(time_run);
		/*	if(flag_seat == 1){
				Backward_tune();
				wait(time_run);
			}
		*/
		}else if(Flag_motion_LHM==3){
			RotRight(1);//右旋回
			wait(time_run);
		/*	if(flag_seat == 1){
				Backward_tune();
				wait(time_run);
			}
		*/
		}else if(Flag_motion_LHM==4){
			RotRight(2);//Uターン
			wait(time_run);
		/*	if(flag_seat == 1){
				Backward_tune();
				wait(time_run);
			}
		*/
		}
		
				
		cntLHM++;
		
	}	
	
	KnightRider(1200);
	
}//lefthandNonstop





void lefthandmethod(void)
{
	
	int cntLHM = 1;
	int time_run = 200;//200;
	
	struct State Mypos ={255, 0, 0, North};
	
	X_goal = GORL_X;
	Y_goal = GORL_Y;


	
	
	First_run();//始めの一歩（要調整、ケツをくっつけた状態から始めるので、90mmではない）
	Mypos.X = 0;
	Mypos.Y = 1;
	wait(time_run);
	
	while(!(Mypos.X = X_goal & Mypos.Y == Y_goal)){//cntLHM != 4*5){//Coordinate != Goal){
		
			
		
		if(refSEN_L < TH_L){//左の壁がない→左に90度ターンして、180mm前進
			RotLeft(1);
			wait(time_run);
			Forward(1);
			wait(time_run);
			
		}else if(refSEN_FL < TH_FL | SEN_FR < TH_FR){//前壁がない→180mm前進
			Forward(1);	
			wait(time_run);
			
		}else if(refSEN_R < TH_R){//右の壁がない→右に90度ターンして、180mm前進
			RotRight(1);
			wait(time_run);
			Forward(1);
			wait(time_run);
			
		}else{//その他（壁に囲まれた→右に180度ターンして、180mm前進
			RotRight(2);
			wait(time_run);
			Backward_tune();
			wait(time_run);
			First_run();
			wait(time_run);
			
		}
		
		cntLHM++;
		
	}	
	
	KnightRider(1200);
	
}//lefthandmethod