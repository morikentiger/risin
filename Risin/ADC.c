/* ADC.c */



//A/D変換初期設定
void init_adc(void){
	// スタンバイ解除 
	STB.CR4.BIT._AD0 = 0;
	STB.CR4.BIT._AD1 = 0;
	// ADIイネーブル 
	AD0.ADCSR.BIT.ADIE = 0;
	AD1.ADCSR.BIT.ADIE = 0;
	// トリガイネーブル 
	AD0.ADCSR.BIT.TRGE = 0;
	AD1.ADCSR.BIT.TRGE = 0;
	// ADF コントロール 
	AD0.ADCSR.BIT.CONADF = 0;
	AD1.ADCSR.BIT.CONADF = 0;
	// ステートコントロール 
	AD0.ADCSR.BIT.STC = 0; 
	AD1.ADCSR.BIT.STC = 0; 
	// A/D変換時間;Pφ/4 ⇒動かなかったら早くしよう
	AD0.ADCSR.BIT.CKSL = 0; 
	AD1.ADCSR.BIT.CKSL = 0; 
	// シングルモード=>4チャンネルスキャン、2チャンネルスキャン 
	AD0.ADCSR.BIT.ADM = 1;	//4ch
	AD1.ADCSR.BIT.ADM = 3;	//2ch
	// 1サイクルスキャン 
	AD0.ADCSR.BIT.ADCS = 0;
	AD1.ADCSR.BIT.ADCS = 0;
	// チャネルセレクト CH0 AN0 
	AD0.ADCSR.BIT.CH = 3;	//011 ch0は、AN0,1,2,3
	AD1.ADCSR.BIT.CH = 1;	//001 ch1は、AN4,5
}


void ADC(void){
	
	//ch0のA/D変換(AN0,1,2,3)
	AD0.ADCR.BIT.ADST = 0;
	AD0.ADCSR.BIT.CH = 3;	//011 ch0は、AN0,1,2,3
	AD0.ADCR.BIT.ADST = 1;
	while(AD0.ADCSR.BIT.ADF == 0);
	SEN0 = AD0.ADDR0 >> 6;
	SEN1 = AD0.ADDR1 >> 6;
	SEN2 = AD0.ADDR2 >> 6;
	SEN3 = AD0.ADDR3 >> 6;
	AD0.ADCSR.BIT.ADF = 0;
//	AD0.ADCR.BIT.ADST = 0;
	
	//ch1のA/D変換(AN4,5)
	AD1.ADCR.BIT.ADST = 0;
	AD1.ADCSR.BIT.CH = 1;	//001 ch1は、AN4,5
	AD1.ADCR.BIT.ADST = 1;
	while(AD1.ADCSR.BIT.ADF == 0);
	SEN4 = AD1.ADDR4 >> 6;
	SEN5 = AD1.ADDR5 >> 6;
	AD1.ADCSR.BIT.ADF = 0;
//	AD1.ADCR.BIT.ADST = 0;
	
	
	
}

